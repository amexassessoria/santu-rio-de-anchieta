<?php 

    Controller::carregaModel('Colaborador/Colaborador'); 

    class ColaboradorController
    {      
        
        public function Salvar()
        {               
			$sMensagem = "";
			$bRetorno = false;
			
            $sCPF = $_POST['Colaborador_chr_CPF'];
            Colaborador::setaFiltro(" AND Colaborador_chr_CPF = '".$sCPF."'");
            $iTotalRegistros = Colaborador::count();

            if ($iTotalRegistros == 0) {
				
				$bCPFValido = Util::validaCPF($_POST['Colaborador_chr_CPF']);
				
				if ($bCPFValido) {
					
					$bEmailValido = Util::validaemail($_POST['Colaborador_vch_Email']);
					
					if ($bEmailValido) {

						$oColaborador = new Colaborador();
				
						$oColaborador->Colaborador_vch_Nome           = utf8_decode($_POST['Colaborador_vch_Nome']);
						$oColaborador->Colaborador_vch_Endereco       = utf8_decode($_POST['Colaborador_vch_Endereco']);
						$oColaborador->Colaborador_lng_Numero         = utf8_decode($_POST['Colaborador_lng_Numero']);
						$oColaborador->Colaborador_vch_Bairro         = utf8_decode($_POST['Colaborador_vch_Bairro']);
						$oColaborador->Colaborador_vch_Cidade         = utf8_decode($_POST['Colaborador_vch_Cidade']);
						$oColaborador->Colaborador_chr_Estado         = $_POST['Colaborador_chr_Estado'];
						$oColaborador->Colaborador_vch_Telefone       = $_POST['Colaborador_vch_Telefone'];
						$oColaborador->Colaborador_vch_Email          = $_POST['Colaborador_vch_Email'];
						$oColaborador->Colaborador_chr_CPF            = $_POST['Colaborador_chr_CPF'];
						$oColaborador->Colaborador_chr_Sexo           = $_POST['Colaborador_chr_Sexo'];
						$oColaborador->Colaborador_dat_DataNascimento = date('Y-m-d', strtotime($_POST['Colaborador_dat_DataNascimento']));
						$oColaborador->Colaborador_chr_CEP            = $_POST['Colaborador_chr_CEP'];
						$oColaborador->Colaborador_vch_Complemento    = utf8_decode($_POST['Colaborador_vch_Complemento']);
						$oColaborador->Logradouro_lng_Codigo          = $_POST['Logradouro_lng_Codigo'];
						$oColaborador->Colaborador_dat_DataCadastro   = date("Y-m-d H:i:s");
						$mRetorno = $oColaborador->salvar($oColaborador);

						if ($mRetorno) {
							$bRetorno = true;
							$sMensagem = "Seu cadastro foi efetuado com sucesso, aguarde nosso contato.";
						} else {
							$bRetorno = false;
							$sMensagem = "Erro ao efetuar seu cadastro. Tente novamente";
						}
					} else {
						$bRetorno = false;
						$sMensagem = "Seu e-mail é inválido. Tente novamente";
					}
				} else {
					$bRetorno = false;
					$sMensagem = "Seu CPF é inválido. Tente novamente";
				}
            } else {
                $bRetorno = false;
				$sMensagem = "Seu CPF já está cadastrado na campanha.";
            }
			
			$arrRertorno = array(
				"retorno" => $bRetorno,
				"mensagem" => $sMensagem
			);
			
			echo json_encode($arrRertorno);
        }

        public function Listar()
        {
            echo Colaborador::selectAll();
        }
        
    }
?>