<?php 

    Controller::carregaModel('Noticia/Noticia'); 

    class NoticiaController
    {      
        
        public function Listar()
        {
            $iPagina = (isset($_GET['p']) ? $_GET['p'] : 0);
            $iLimite = 10;
            $iInicio = 0;
            
            if ($iPagina > 1) {
                $iInicio = $iLimite * $iPagina;
            } 
            
            /*Noticia::setaFiltro(" AND Artigo_chr_Carrossel = 'S' ");*/
            Noticia::setaLimite($iLimite,$iInicio);
            
            echo Noticia::selectAll();
            
        }
        
        public function Detalhar()
        {            
            $iCodigo = $_GET['id'];
            echo Noticia::selectPorCodigo($iCodigo);
        }
        
        public function SantoDoDia()
        {            
            Noticia::setaFiltro(" AND Tipo_lng_Codigo = 5 ");
            Noticia::setaFiltro(" AND ( Artigo_chr_Dia = ".date('d')." AND Artigo_chr_Mes = ".date('m')." )");
            echo Noticia::select();
        }
        
    }
?>