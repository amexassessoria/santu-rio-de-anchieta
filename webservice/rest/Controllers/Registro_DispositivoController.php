<?php 

    Controller::carregaModel('Registro_Dispositivo/Registro_Dispositivo'); 

    class Registro_DispositivoController
    {      
        
        public function Salvar()
        {
            $sID = $_POST['id'];
            Registro_Dispositivo::setaFiltro(" AND Registro_Dispositivo_vch_ID = '".$sID."'");
            $iTotalRegistros = Registro_Dispositivo::count();

            $oRegistro_Dispositivo = new Registro_Dispositivo();
            $oRegistro_Dispositivo->Registro_Dispositivo_vch_ID = $sID;
            $oRegistro_Dispositivo->Registro_Dispositivo_dat_Data = date("Y-m-d H:i:s");

            if ($iTotalRegistros == 0) {
                $oRegistro_Dispositivo->Registro_Dispositivo_vch_Plataforma = $_POST['plataforma'];
                $mRetorno = Registro_Dispositivo::salvar($oRegistro_Dispositivo);

                if ($mRetorno) {
                    echo true;
                } else {
                    echo false;
                }

            } else {
                $mRetorno = Registro_Dispositivo::alterar($oRegistro_Dispositivo);
                
                if ($mRetorno) {
                    echo true;
                } else {
                    echo false;
                }
            }
        }

        public function Listar()
        {
            echo Registro_Dispositivo::selectAll();
        }
        
    }
?>