<?php

    class Informativo
    {
    
        public $Informativo_lng_Codigo;
        public $Informativo_vch_Titulo; 
        public $Informativo_vch_Url;
        public $Informativo_chr_Mes;
        public $Informativo_chr_Ano;
        public $Informativo_lng_Numero;
        public $Informativo_txt_Descricao;
        public $Informativo_vch_EmbedCode;
        public $Informativo_vch_IssuuLink;
        public $Informativo_vch_Capa;

        /* M�todos m�gicos GET e SET */
        public function __get($property) 
        {
            if (property_exists($this, $property)) 
            {
              return $this->$property;
            }
        }

        public function __set($property, $value) 
        {
            if (property_exists($this, $property)) 
            {
              $this->$property = $value;
            }
            return $this;
        }
        
        /* Queries Dinamicas */
        private static $sOrdem  = 'Artigo_lng_Codigo';
        private static $iLimite = 0;
        private static $iInicio = 0;
        private static $mArrCampos = '';
        
        public static function setaFiltro($sArgCampo)
        {
            self::$mArrCampos['CONDICAO'][] = $sArgCampo;
        }

        public static function setaJoin($sArgJoin)
        {
            self::$mArrJoin['JOIN'][] = $sArgJoin;
        }

        public static function setaOrdem($sArgOrdem)
        {
            self::$sOrdem = $sArgOrdem;
        }

        public static final function setaLimite($iArgLimite, $iArgInicio = 0)
        {
            self::$iLimite = $iArgLimite;
            self::$iInicio = $iArgInicio;
        }
        
        public static function selectUltimo( )
        {
            
            $oConexao = DB::Conectar();

            $mResultado = $oConexao->prepare("SELECT *
                               FROM Informativo
                               ORDER BY Informativo_lng_Codigo DESC
                               LIMIT 1"
                            );  

            $mResultado->execute();

            $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
   
            if (!empty($mArrDados))
            { 

                $arr = array(
                    'Informativo_lng_Codigo'    => $mArrDados[0]['Informativo_lng_Codigo'],
                    'Informativo_vch_Titulo'    => utf8_encode($mArrDados[0]['Informativo_vch_Titulo']),
                    'Informativo_vch_Url'       => 'http://'.$_SERVER['HTTP_HOST'].'/informativos/'.$mArrDados[0]['Informativo_vch_Url'],
                    'Informativo_chr_Mes'       => $mArrDados[0]['Informativo_chr_Mes'],
                    'Informativo_chr_Ano'       => $mArrDados[0]['Informativo_chr_Ano'],
                    'Informativo_lng_Numero'    => $mArrDados[0]['Informativo_lng_Numero'],
                    'Informativo_txt_Descricao' => utf8_encode($mArrDados[0]['Informativo_txt_Descricao']),
                    'Informativo_vch_EmbedCode' => $mArrDados[0]['Informativo_vch_EmbedCode'],
                    'Informativo_vch_IssuuLink' => $mArrDados[0]['Informativo_vch_IssuuLink'],
                    'Informativo_vch_Capa'      => $mArrDados[0]['Informativo_vch_Capa']
                );
                    
                return json_encode($arr);
            } 
            else
            {
                return null;
            }
        }
    }
?>