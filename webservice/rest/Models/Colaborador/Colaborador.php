<?php

    class Colaborador
    {
    
        public $Colaborador_lng_Codigo;
        public $Colaborador_vch_Nome; 
        public $Colaborador_vch_Endereco;
        public $Colaborador_lng_Numero;
        public $Colaborador_vch_Bairro;
        public $Colaborador_vch_Cidade;
        public $Colaborador_chr_Estado;
        public $Colaborador_vch_Telefone;
        public $Colaborador_vch_Email;
        public $Colaborador_chr_CPF;
        public $Colaborador_dat_DataNascimento;
        public $Colaborador_chr_CEP;
        public $Colaborador_chr_Sexo;
        public $Logradouro_lng_Codigo;
        public $Colaborador_vch_Complemento;
        public $Colaborador_chr_Importado_Frs;
        public $Colaborador_dat_DataCadastro;

        /* M�todos m�gicos GET e SET */
        public function __get($property) 
        {
            if (property_exists($this, $property)) 
            {
              return $this->$property;
            }
        }

        public function __set($property, $value) 
        {
            if (property_exists($this, $property)) 
            {
              $this->$property = $value;
            }
            return $this;
        }
        
        /* Queries Dinamicas */
        private static $sOrdem  = 'Colaborador_lng_Codigo';
        private static $iLimite = 0;
        private static $iInicio = 0;
        private static $mArrCampos = '';
        
        public static function setaFiltro($sArgCampo)
        {
            self::$mArrCampos['CONDICAO'][] = $sArgCampo;
        }

        public static function setaJoin($sArgJoin)
        {
            self::$mArrJoin['JOIN'][] = $sArgJoin;
        }

        public static function setaOrdem($sArgOrdem)
        {
            self::$sOrdem = $sArgOrdem;
        }

        public static final function setaLimite($iArgLimite, $iArgInicio = 0)
        {
            self::$iLimite = $iArgLimite;
            self::$iInicio = $iArgInicio;
        }
        
        public static final function count( )
        {
            $oConexao = DB::Conectar();

            $sFiltros = '';

            // Define os Filtros
            if (isset(self::$mArrCampos['CONDICAO']))
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                
                self::$mArrCampos = "";
                
            }

            $mResultado = $oConexao->prepare("SELECT
                               count(*)
                               FROM Colaborador
                               WHERE 1 = 1 ".$sFiltros."                       
                             ");  
            
            $mResultado->execute();
            //echo $mResultado->queryString;
            
            if ($mResultado)
            {
                $iTotal = $mResultado->fetchColumn();
            }
            else
            {
                $iTotal = null;
            }
            

            return $iTotal;     
        }

        public static function salvar( $oColaborador )
        {
			
            try 
            {
                $oConexao = db::conectar();

                $sSql = $oConexao->prepare("INSERT INTO Colaborador (
                        Colaborador_vch_Nome,
                        Colaborador_vch_Email,
                        Colaborador_vch_Endereco,
                        Colaborador_vch_Bairro,
                        Colaborador_vch_Cidade,
                        Colaborador_chr_Estado,
                        Colaborador_vch_Telefone,
                        Colaborador_lng_Numero,
                        Colaborador_chr_CPF,
                        Colaborador_dat_DataNascimento,
                        Colaborador_vch_Complemento,
                        Logradouro_lng_Codigo,
                        Colaborador_chr_Sexo,
                        Colaborador_chr_CEP,
                        Colaborador_dat_DataCadastro
                        ) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )"); 

                $sSql->bindValue(1, ($oColaborador->Colaborador_vch_Nome));
                $sSql->bindValue(2, ($oColaborador->Colaborador_vch_Email));
                $sSql->bindValue(3, ($oColaborador->Colaborador_vch_Endereco));
                $sSql->bindValue(4, ($oColaborador->Colaborador_vch_Bairro));
                $sSql->bindValue(5, ($oColaborador->Colaborador_vch_Cidade));
                $sSql->bindValue(6, ($oColaborador->Colaborador_chr_Estado));
                $sSql->bindValue(7, ($oColaborador->Colaborador_vch_Telefone));
                $sSql->bindValue(8, ($oColaborador->Colaborador_lng_Numero));
                $sSql->bindValue(9, ($oColaborador->Colaborador_chr_CPF));
                $sSql->bindValue(10,($oColaborador->Colaborador_dat_DataNascimento));
                $sSql->bindValue(11,($oColaborador->Colaborador_vch_Complemento));
                $sSql->bindValue(12,($oColaborador->Logradouro_lng_Codigo));
                $sSql->bindValue(13,($oColaborador->Colaborador_chr_Sexo));
                $sSql->bindValue(14,($oColaborador->Colaborador_chr_CEP));
                $sSql->bindValue(15,($oColaborador->Colaborador_dat_DataCadastro));
                
                return $sSql->execute();

            } 
            catch (Exception $e) 
            {
                return false;
            }
        }
    }
?>