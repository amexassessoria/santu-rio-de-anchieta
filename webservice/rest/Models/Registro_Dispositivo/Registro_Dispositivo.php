<?php

    class Registro_Dispositivo
    {
    
        public $Registro_Dispositivo_lng_Codigo;
        public $Registro_Dispositivo_vch_ID; 
        public $Registro_Dispositivo_dat_Data;
        public $Registro_Dispositivo_vch_Plataforma;

        /* M�todos m�gicos GET e SET */
        public function __get($property) 
        {
            if (property_exists($this, $property)) 
            {
              return $this->$property;
            }
        }

        public function __set($property, $value) 
        {
            if (property_exists($this, $property)) 
            {
              $this->$property = $value;
            }
            return $this;
        }
        
        /* Queries Dinamicas */
        private static $sOrdem  = 'Registro_Dispositivo_lng_Codigo';
        private static $iLimite = 0;
        private static $iInicio = 0;
        private static $mArrCampos = '';
        
        public static function setaFiltro($sArgCampo)
        {
            self::$mArrCampos['CONDICAO'][] = $sArgCampo;
        }

        public static function setaJoin($sArgJoin)
        {
            self::$mArrJoin['JOIN'][] = $sArgJoin;
        }

        public static function setaOrdem($sArgOrdem)
        {
            self::$sOrdem = $sArgOrdem;
        }

        public static final function setaLimite($iArgLimite, $iArgInicio = 0)
        {
            self::$iLimite = $iArgLimite;
            self::$iInicio = $iArgInicio;
        }
        
        public static function selectPorCodigo( $iCodigo )
        {
            $oConexao = DB::Conectar();

            $mResultado = $oConexao->prepare("SELECT *
                               FROM Registro_Dispositivo
                               WHERE Registro_Dispositivo_lng_Codigo = ".$iCodigo
                            );  

            $mResultado->execute();

            $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
            
            if (!empty($mArrDados))
            { 
                    
                $arr = array(
                    'Registro_Dispositivo_lng_Codigo' => $mArrDados[0]['Registro_Dispositivo_lng_Codigo'],
                    'Registro_Dispositivo_vch_ID' => utf8_encode($mArrDados[0]['Registro_Dispositivo_vch_ID'])
                );
                
                return json_encode($arr);
                
            } else {
                return NULL; 
            }
        }
        
        public static function selectAll( )
        {
            $oConexao = DB::Conectar();

            $sFiltros = '';
            
            /* Define o Limite */
            if (self::$iLimite > 0)
            {
                $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

                //Limpa Filtro
                self::$iInicio = 0;
                self::$iLimite = 0;
            }

            // Define os Filtros
            if (isset(self::$mArrCampos['CONDICAO']))
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }

            $mResultado = $oConexao->prepare("SELECT *
                                FROM Registro_Dispositivo
                                WHERE 1 = 1 ".$sFiltros." 
                                 ORDER BY ".self::$sOrdem."
                                ".$sLimite."
                             ");  

            $mResultado->execute();

            $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);

            /* Instancia o Objeto */
            $arrObj = array();
            
            if (!empty($mArrDados))
            { 
                for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
                {
                    
                    $arr = array(
                        'Registro_Dispositivo_lng_Codigo' => $mArrDados[$a]['Registro_Dispositivo_lng_Codigo'],
                        'Registro_Dispositivo_vch_ID' => utf8_encode($mArrDados[$a]['Registro_Dispositivo_vch_ID'])
                    );

                    $arrObj[] = $arr;
                }
                
                return json_encode($arrObj);
                
            } else {
                return NULL; 
            }
        }
        
        public static final function count( )
        {
            $oConexao = DB::Conectar();

            $sFiltros = '';

            // Define os Filtros
            if (isset(self::$mArrCampos['CONDICAO']))
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                
                self::$mArrCampos = "";
                
            }

            $mResultado = $oConexao->prepare("SELECT
                               count(*)
                               FROM Registro_Dispositivo
                               WHERE 1 = 1 ".$sFiltros."                       
                             ");  
            
            $mResultado->execute();
            //echo $mResultado->queryString;
            
            if ($mResultado)
            {
                $iTotal = $mResultado->fetchColumn();
            }
            else
            {
                $iTotal = null;
            }
            

            return $iTotal;     
        }

        public static function salvar( $oRegistro_Dispositivo )
        {
            try 
            {
                $oConexao = db::conectar();

                $sSql = $oConexao->prepare("INSERT INTO Registro_Dispositivo (
                            Registro_Dispositivo_vch_ID,
                            Registro_Dispositivo_dat_Data,
                            Registro_Dispositivo_vch_Plataforma
                            ) VALUES ( ?,?,? )"); 

                $sSql->bindValue(1, ($oRegistro_Dispositivo->Registro_Dispositivo_vch_ID));
				$sSql->bindValue(2, ($oRegistro_Dispositivo->Registro_Dispositivo_dat_Data));
                $sSql->bindValue(3, ($oRegistro_Dispositivo->Registro_Dispositivo_vch_Plataforma));
                
                return $sSql->execute();

            } 
            catch (Exception $e) 
            {
                return false;
            }
        }

        public static function alterar( $oRegistro_Dispositivo )
        {
            try 
            {
                $oConexao = db::conectar();

                $sSql = $oConexao->prepare("UPDATE Registro_Dispositivo SET
                            Registro_Dispositivo_dat_Data = ?
                            WHERE Registro_Dispositivo_vch_ID  = ?"); 

                $sSql->bindValue(1, ($oRegistro_Dispositivo->Registro_Dispositivo_dat_Data));
                $sSql->bindValue(2, ($oRegistro_Dispositivo->Registro_Dispositivo_vch_ID));
                
                return $sSql->execute();

            } 
            catch (Exception $e) 
            {
                return false;
            }
        }
    }
?>