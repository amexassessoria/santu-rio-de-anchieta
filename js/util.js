function salvaDados(form)
{
    $.ajax({
        type: "POST",
        url: form.attr('action'),
        data: form.serialize()
    });

}

/*$(".number").keydown(function(event) 
 {
 if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39))
 {
 return;
 }
 else 
 {
 if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) 
 {
 event.preventDefault(); 
 }   
 }
 });*/

function clearForm(idForm) {
    $('#' + idForm).find(':input').each(function() {
        switch (this.type) {
            case 'password':
            case 'hidden':
            case 'select-multiple':
            case 'select-one':
            case 'text':
            case 'textarea':
                $(this).val('');
                break;
            case 'checkbox':
            case 'radio':
                this.checked = false;
        }
    });

}

function resetForm(id) {
    $('#' + id).each(function() {
        this.reset();
    });
}

function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test(email)) {
        return false;
    } else {
        return true;
    }
}

function validaCpf(str){
    str = str.replace('.','');
    str = str.replace('.','');
    str = str.replace('-','');
 
    cpf = str;
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;
    if (cpf.length < 11)
        return false;
    for (i = 0; i < cpf.length - 1; i++)
        if (cpf.charAt(i) != cpf.charAt(i + 1)){
            digitos_iguais = 0;
            break;
        }
    if (!digitos_iguais){
        numeros = cpf.substring(0,9);
        digitos = cpf.substring(9);
        soma = 0;
        for (i = 10; i > 1; i--)
            soma += numeros.charAt(10 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
        numeros = cpf.substring(0,10);
        soma = 0;
        for (i = 11; i > 1; i--)
            soma += numeros.charAt(11 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;
        return true;
    }
    else
        return false;
}

function retiraAcentos(palavra) { 
    com_acento = '����������������������������������������������'; 
    sem_acento = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC'; 
    nova=''; 
    for(i=0;i<palavra.length;i++) { 
        if (com_acento.search(palavra.substr(i,1))>=0) { 
            nova+=sem_acento.substr(com_acento.search(palavra.substr(i,1)),1); 
        } 
        else { 
            nova+=palavra.substr(i,1); 
        } 
    } 
    return nova; 
}

function converteEstado(sEstado) { 
    
    sEstado = retiraAcentos(sEstado);
    var sEstadoSigla = 'sp';
    
    switch(sEstado) {
        case 'Acre':
            sEstadoSigla = 'ac';
            break;
        case 'Alagoas':
            sEstadoSigla = 'al';
            break;
        case 'Amapa':
            sEstadoSigla = 'ap';
            break;
        case 'Amazonas':
            sEstadoSigla = 'am';
            break;
        case 'Bahia':
            sEstadoSigla = 'ba';
            break;
        case 'Ceara':
            sEstadoSigla = 'ce';
            break;    
        case 'Distrito Federal':
            sEstadoSigla = 'df';
            break;
        case 'Espirito Santo':
            sEstadoSigla = 'es';
            break;
        case 'Goias':
            sEstadoSigla = 'go';
            break;
        case 'Maranhao':
            sEstadoSigla = 'ma';
            break;
        case 'Mato Grosso do Sul':
            sEstadoSigla = 'ms';
            break;
        case 'Mato Grosso':
            sEstadoSigla = 'mt';
            break;
        case 'Minas Gerais':
            sEstadoSigla = 'mg';
            break;
        case 'Para':
            sEstadoSigla = 'pa';
            break;
        case 'Paraiba':
            sEstadoSigla = 'pb';
            break;
        case 'Parana':
            sEstadoSigla = 'pr';
            break;
        case 'Pernambuco':
            sEstadoSigla = 'pe';
            break;
        case 'Piaui':
            sEstadoSigla = 'pi';
            break;
        case 'Rio de Janeiro':
            sEstadoSigla = 'rj';
            break;
        case 'Rio Grande do Norte':
            sEstadoSigla = 'rn';
            break;
        case 'Rio Grande do Sul':
            sEstadoSigla = 'rs';
            break;
        case 'Rondonia':
            sEstadoSigla = 'ro';
            break;
        case 'Roraima':
            sEstadoSigla = 'rr';
            break;
        case 'Santa Catarina':
            sEstadoSigla = 'sc';
            break;
        case 'Sao Paulo':
            sEstadoSigla = 'sp';
            break;    
        case 'Sergipe':
            sEstadoSigla = 'se';
            break;    
        case 'Tocantins':
            sEstadoSigla = 'to';
            break;
    }
    
    return sEstadoSigla;
}