<?php

class Artigo
{
    
    /* Atributos */
    private $iCodigo;
    private $sTitulo;
    private $sConteudo;
    private $sUrlImagem;
    private $sDataCadastro;
    private $sDataAlteracao;
    private $sAgenda;
    private $sDia;
    private $sMes;
    private $sMesExtenso;
    private $sAno;
    private $sLink;
    private $sDestaque;
    private $sResumo;
    private $sOcultarMiniatura;
    private $sCarrossel;
    private $sAtivo;
    private $sCredito;
    private $sFonteDescricao;
    private $sFonteUrl;
    
    private $iCodigoTipo;
    private $iCodigoUsuario;
    
    private $oArtigoTipo;
    private $oUsuario;
    private $oAlbum;
    
    private $arrObjArtigoTag;
    
    /* M�todos m�gicos GET e SET */
    public function __get($property) 
    {
        if (property_exists($this, $property)) 
        {
          return $this->$property;
        }
    }
    
    public function __set($property, $value) 
    {
        if (property_exists($this, $property)) 
        {
          $this->$property = $value;
        }
        return $this;
    }

}

?>