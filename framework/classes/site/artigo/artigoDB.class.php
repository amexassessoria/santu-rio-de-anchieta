<?php

class ArtigoDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Artigo.Artigo_lng_Codigo';
    private static $iLimite = 0;
    private static $iInicio = 0;
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function setaLimite($iArgLimite, $iArgInicio = 0)
    {
        self::$iLimite = $iArgLimite;
        self::$iInicio = $iArgInicio;
    }
    
    public static final function pesquisaArtigoLista( $bTags = false )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sJoin = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }

        // Define os Joins
        if (isset(self::$mArrJoin['JOIN']))
        {
            if ( self::$mArrJoin['JOIN'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrJoin['JOIN']); $a < $iCount; ++$a)
                {
                    $sJoin .= (self::$mArrJoin['JOIN'][$a]);
                }
            }
            //Limpa Filtros
            self::$mArrJoin['JOIN'] = '';
        }
        
        /* Define o Limite */
        if (self::$iLimite > 0)
        {
            $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

            //Limpa Filtro
            self::$iInicio = 0;
            self::$iLimite = 0;
        }
             
        $mResultado = $oConexao->prepare("SELECT 
                           (CASE WHEN ((Artigo_dat_Agenda IS NOT NULL) AND (Artigo_dat_Agenda != '0000-00-00 00:00:00')) THEN Artigo_dat_Agenda ELSE Artigo_dat_Cadastro END) AS Artigo_dat_Cadastro,
                           Artigo_vch_Titulo,
                           Artigo.Artigo_lng_Codigo,
                           Artigo_vch_Link,
                           Artigo_vch_UrlImagem,
                           Artigo_vch_Resumo,
                           Artigo_chr_Ativo,
                           Artigo_txt_Conteudo
                           FROM Artigo
                           ".$sJoin."
                           WHERE 1 = 1 ".$sFiltros." 
                           AND ( Artigo_dat_Agenda <= NOW() OR Artigo_dat_Agenda IS NULL) 
                           AND ( Artigo_chr_Ativo = 'S' OR Artigo_chr_Ativo IS NULL )
                           ORDER BY Artigo_dat_Cadastro DESC
                           ".$sLimite."
                         "); 
       
        //echo $mResultado->queryString;
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjArtigo = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oArtigo  = new Artigo;
                        
                $sUrlImagem = ($mArrDados[$a]['Artigo_vch_UrlImagem'] == '') ? 'images/default.jpg' : $mArrDados[$a]['Artigo_vch_UrlImagem'];
                
                $oArtigo->iCodigo       = $mArrDados[$a]['Artigo_lng_Codigo'];;
                $oArtigo->sTitulo       = $mArrDados[$a]['Artigo_vch_Titulo'];
                $oArtigo->sUrlImagem    = $sUrlImagem;
                $oArtigo->sAtivo        = $mArrDados[$a]['Artigo_chr_Ativo'];
                $oArtigo->sConteudo     = $mArrDados[$a]['Artigo_txt_Conteudo'];
                $oArtigo->sDataCadastro = $mArrDados[$a]['Artigo_dat_Cadastro'];
                $oArtigo->sResumo       = $mArrDados[$a]['Artigo_vch_Resumo'];
                $oArtigo->sLink         = 'artigo/'.$mArrDados[$a]['Artigo_vch_Link'].'.html';

                if ($bTags) {
                    ArtigoTagDB::setaFiltro(' AND Artigo_lng_Codigo = '.$oArtigo->iCodigo);
                    $oArtigo->arrObjArtigoTag = ArtigoTagDB::pesquisaArtigoTagLista();
                }
                
                $arrObjArtigo->append($oArtigo);
            }
        }
        return $arrObjArtigo;
    }
	
	public static final function pesquisaArtigoListaMaisVistos( )
    {
        $oConexao = db::conectar();

		$sLimite  = '';
		
		/* Define o Limite */
        if (self::$iLimite > 0)
        {
            $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

            //Limpa Filtro
            self::$iInicio = 0;
            self::$iLimite = 0;
        }
             
        $mResultado = $oConexao->prepare("SELECT 
                           (CASE WHEN ((Artigo_dat_Agenda IS NOT NULL) AND (Artigo_dat_Agenda != '0000-00-00 00:00:00')) THEN Artigo_dat_Agenda ELSE Artigo_dat_Cadastro END) AS Artigo_dat_Cadastro,
                           Artigo_vch_Titulo,
                           Artigo.Artigo_lng_Codigo,
                           Artigo_vch_Link,
                           Artigo_vch_UrlImagem,
                           Artigo_vch_Resumo,
                           Artigo_chr_Ativo,
                           Artigo_txt_Conteudo
                           FROM Artigo
                           WHERE Tipo_lng_Codigo = 1
						   AND Artigo_dat_Cadastro BETWEEN NOW() - INTERVAL 30 DAY AND NOW() 
                           AND ( Artigo_dat_Agenda <= NOW() OR Artigo_dat_Agenda IS NULL) 
                           AND ( Artigo_chr_Ativo = 'S' OR Artigo_chr_Ativo IS NULL )
                           ORDER BY Artigo_lng_TotalDeVisitas DESC
                           ".$sLimite."
                         "); 
       
        //echo $mResultado->queryString;
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjArtigo = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oArtigo  = new Artigo;
                        
                $sUrlImagem = ($mArrDados[$a]['Artigo_vch_UrlImagem'] == '') ? 'images/default.jpg' : $mArrDados[$a]['Artigo_vch_UrlImagem'];
                
                $oArtigo->iCodigo       = $mArrDados[$a]['Artigo_lng_Codigo'];;
                $oArtigo->sTitulo       = $mArrDados[$a]['Artigo_vch_Titulo'];
                $oArtigo->sUrlImagem    = $sUrlImagem;
                $oArtigo->sAtivo        = $mArrDados[$a]['Artigo_chr_Ativo'];
                $oArtigo->sConteudo     = $mArrDados[$a]['Artigo_txt_Conteudo'];
                $oArtigo->sDataCadastro = $mArrDados[$a]['Artigo_dat_Cadastro'];
                $oArtigo->sResumo       = $mArrDados[$a]['Artigo_vch_Resumo'];
                $oArtigo->sLink         = 'artigo/'.$mArrDados[$a]['Artigo_vch_Link'].'.html';
                
                $arrObjArtigo->append($oArtigo);
            }
        }
        return $arrObjArtigo;
    }
    
    public static final function pesquisaArtigoListaPainelDeControle( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
        
        /* Define o Limite */
        if (self::$iLimite > 0)
        {
            $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

            //Limpa Filtro
            self::$iInicio = 0;
            self::$iLimite = 0;
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Artigo_lng_Codigo,
                           Artigo_vch_Titulo,
                           Artigo_vch_Link,
                           Artigo_dat_Cadastro,
                           Usuario_lng_Codigo,
                           Tipo_lng_Codigo,
                           Artigo_dat_Agenda
                           FROM Artigo
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                           ".$sLimite."
                         "); 
        
        $mResultado->execute(); 
        //echo $mResultado->queryString;
        
        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjArtigo = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oArtigo  = new Artigo;
                
                UsuarioDB::setaFiltro(' AND Usuario_lng_Codigo = '.$mArrDados[$a]['Usuario_lng_Codigo']);
                $oUsuario = UsuarioDB::pesquisaUsuario( true );
                
                ArtigoTipoDB::setaFiltro(' AND Tipo_lng_Codigo = '.$mArrDados[$a]['Tipo_lng_Codigo']);
                $oArtigoTipo = ArtigoTipoDB::pesquisaArtigoTipo();
               
                //$oArtigo->iCodigoTipo   = $mArrDados[$a]['Tipo_lng_Codigo'];
                $oArtigo->iCodigo       = $mArrDados[$a]['Artigo_lng_Codigo'];
                $oArtigo->sAgenda       = $mArrDados[$a]['Artigo_dat_Agenda'];
                $oArtigo->sTitulo       = $mArrDados[$a]['Artigo_vch_Titulo'];
                $oArtigo->sDataCadastro = $mArrDados[$a]['Artigo_dat_Cadastro'];
                $oArtigo->sLink         = 'artigo/'.$mArrDados[$a]['Artigo_vch_Link'].'.html';
                
                $oArtigo->oArtigoTipo = $oArtigoTipo;
                $oArtigo->oUsuario    = $oUsuario;
    
                $arrObjArtigo->append($oArtigo);
            }
        }
        return $arrObjArtigo;
    }
    
    public static final function pesquisaArtigo( )
    {
        
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Artigo_lng_Codigo,
                           Artigo_vch_Titulo,
                           Artigo_txt_Conteudo,
                           Artigo_vch_UrlImagem,
                           (CASE WHEN ((Artigo_dat_Agenda IS NOT NULL) AND (Artigo_dat_Agenda != '0000-00-00 00:00:00')) THEN Artigo_dat_Agenda ELSE Artigo_dat_Cadastro END) AS Artigo_dat_Cadastro,
                           Artigo_chr_Destaque,
                           Artigo_vch_Link,
                           Tipo_lng_Codigo,
                           Artigo_chr_Dia,
                           Artigo_chr_Mes,
                           Artigo_vch_Resumo,
                           Artigo_chr_Ano,
                           Artigo_chr_OcultarMiniatura,
                           Artigo_chr_Carrossel,
                           Artigo_chr_Ativo,
                           Album_lng_Codigo,
                           Artigo_vch_Credito,
                           Artigo_vch_FonteDescricao,
                           Artigo_vch_FonteUrl,
                           Artigo_dat_Agenda
                           FROM Artigo
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        //echo $mResultado->queryString;

         /* Instancia o Objeto */
        $oArtigo = new Artigo;
        
        if (is_array($mArrDados))
        { 
           //$sUrlImagem = ($mArrDados['Artigo_vch_UrlImagem'] == '') ? 'images/default.png' : $mArrDados['Artigo_vch_UrlImagem'];
           $oAlbum = new Album();
           
           ArtigoTipoDB::setaFiltro(' AND Tipo_lng_Codigo = '.$mArrDados['Tipo_lng_Codigo']);
           $oArtigoTipo = ArtigoTipoDB::pesquisaArtigoTipo();

           if ( $mArrDados['Album_lng_Codigo'] !== NULL) {
               AlbumDB::setaFiltro(' AND Album_lng_Codigo = '.$mArrDados['Album_lng_Codigo']);
               $oAlbum = AlbumDB::pesquisaAlbum();
           }

           if (strpos($mArrDados['Artigo_vch_UrlImagem'], 'http') !== false) {
                // N�o faz nada
            } else {
                $mArrDados['Artigo_vch_UrlImagem'] = PATH_WWW.$mArrDados['Artigo_vch_UrlImagem'];
            }

           //$oAlbum->iCodigo = $mArrDados['Album_lng_Codigo'];
                   
           $oArtigo->iCodigo         = $mArrDados['Artigo_lng_Codigo'];
           $oArtigo->sTitulo         = $mArrDados['Artigo_vch_Titulo'];
           $oArtigo->sConteudo       = $mArrDados['Artigo_txt_Conteudo'];
           $oArtigo->sUrlImagem      = $mArrDados['Artigo_vch_UrlImagem'];
           $oArtigo->sLink           = 'artigo/'.$mArrDados['Artigo_vch_Link'].'.html';
           $oArtigo->sDataCadastro   = $mArrDados['Artigo_dat_Cadastro'];
           $oArtigo->sDestaque       = $mArrDados['Artigo_chr_Destaque'];
           $oArtigo->sDia            = $mArrDados['Artigo_chr_Dia'];
           $oArtigo->sMes            = $mArrDados['Artigo_chr_Mes'];
           $oArtigo->oAlbum          = $oAlbum;
           $oArtigo->sMesExtenso     = Util::retornaMesExtenso($mArrDados['Artigo_chr_Mes']);
           $oArtigo->sAno            = $mArrDados['Artigo_chr_Ano'];
           $oArtigo->sResumo         = $mArrDados['Artigo_vch_Resumo'];
           $oArtigo->sOcultarMiniatura = $mArrDados['Artigo_chr_OcultarMiniatura'];
           $oArtigo->sCarrossel      = $mArrDados['Artigo_chr_Carrossel'];
           $oArtigo->sAtivo          = $mArrDados['Artigo_chr_Ativo'];
           $oArtigo->sCredito        = $mArrDados['Artigo_vch_Credito'];
           $oArtigo->sFonteDescricao = $mArrDados['Artigo_vch_FonteDescricao'];
           $oArtigo->sFonteUrl       = $mArrDados['Artigo_vch_FonteUrl'];
           $oArtigo->sAgenda         = $mArrDados['Artigo_dat_Agenda'];
           //$oArtigo->iCodigoTipo   = $mArrDados['Tipo_lng_Codigo'];
           
           $oArtigo->oArtigoTipo   = $oArtigoTipo;
           
           
           ArtigoTagDB::setaFiltro(' AND Artigo_lng_Codigo = '.$oArtigo->iCodigo);
           $oArtigo->arrObjArtigoTag = ArtigoTagDB::pesquisaArtigoTagLista();

        }
        return $oArtigo;     
    }
    
    public static final function pesquisaArtigoDoDia( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Artigo_lng_Codigo,
                           Artigo_vch_Titulo,
                           Artigo_vch_UrlImagem,
                           Artigo_vch_Link,
                           Artigo_chr_Dia,
                           Artigo_chr_Mes,
                           Artigo_chr_Ano
                           FROM Artigo
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        //echo $mResultado->queryString;
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);

         /* Instancia o Objeto */
        $oArtigo = new Artigo;
        
        if (is_array($mArrDados))
        { 
           $oArtigo->iCodigo       = $mArrDados['Artigo_lng_Codigo'];
           $oArtigo->sTitulo       = $mArrDados['Artigo_vch_Titulo'];
           $oArtigo->sUrlImagem    = $mArrDados['Artigo_vch_UrlImagem'];
           $oArtigo->sLink         = 'artigo/'.$mArrDados['Artigo_vch_Link'].'.html';
           $oArtigo->sDia          = $mArrDados['Artigo_chr_Dia'];
           $oArtigo->sMes          = $mArrDados['Artigo_chr_Mes'];
           $oArtigo->sAno            = $mArrDados['Artigo_chr_Ano'];
        }
        return $oArtigo;     
    }
    
    public static final function salvaArtigo( $oArtigo )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Artigo (
                    Artigo_vch_Titulo,
                    Artigo_txt_Conteudo,
                    Artigo_dat_Cadastro,
                    Artigo_chr_Destaque,
                    Artigo_vch_Link,
                    Artigo_vch_UrlImagem,
                    Tipo_lng_Codigo,
                    Usuario_lng_Codigo,
                    Artigo_chr_Dia,
                    Artigo_chr_Mes,
                    Artigo_chr_Ano,
                    Artigo_vch_Resumo,
                    Album_lng_Codigo,
                    Artigo_chr_OcultarMiniatura,
                    Artigo_chr_Carrossel,
                    Artigo_chr_Ativo,
                    Artigo_vch_Credito,
                    Artigo_vch_FonteDescricao,
                    Artigo_vch_FonteUrl,
                    Artigo_dat_Agenda
                    ) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )"); 
        
        //$sLink = Util::formataUrl($oArtigo->sTitulo);

        $sSql->bindParam(1, ($oArtigo->sTitulo));
        $sSql->bindParam(2, ($oArtigo->sConteudo));
        $sSql->bindParam(3, ($oArtigo->sDataCadastro));
        $sSql->bindParam(4, ($oArtigo->sDestaque));
        $sSql->bindParam(5, ($oArtigo->sLink));
        $sSql->bindParam(6, ($oArtigo->sUrlImagem));
        $sSql->bindParam(7, ($oArtigo->oArtigoTipo->iCodigo));
        $sSql->bindParam(8, ($oArtigo->iCodigoUsuario));
        $sSql->bindParam(9, ($oArtigo->sDia));
        $sSql->bindParam(10,($oArtigo->sMes));
        $sSql->bindParam(11,($oArtigo->sAno));
        $sSql->bindParam(12,($oArtigo->sResumo));
        $sSql->bindParam(13,($oArtigo->oAlbum->iCodigo));
        $sSql->bindParam(14,($oArtigo->sOcultarMiniatura));
        $sSql->bindParam(15,($oArtigo->sCarrossel));
        $sSql->bindParam(16,($oArtigo->sAtivo));
        $sSql->bindParam(17,($oArtigo->sCredito));
        $sSql->bindParam(18,($oArtigo->sFonteDescricao));
        $sSql->bindParam(19,($oArtigo->sFonteUrl));
        $sSql->bindParam(20,($oArtigo->sAgenda));
        $sSql->execute();
        
        //$sSql->debugDumpParams(); 
        //print ($sSql->queryString);
        return $oConexao->lastInsertId(); 
    }
    
    public static final function alteraArtigo( $oArtigo )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Artigo SET
                    Artigo_vch_Titulo    = :titulo,
                    Artigo_txt_Conteudo  = :conteudo,
                    Artigo_dat_Alteracao = :dataAlteracao,
                    Artigo_chr_Destaque  = :destaque,
                    Artigo_vch_UrlImagem = :urlImagem,
                    Artigo_chr_Dia       = :dia,
                    Artigo_chr_Mes       = :mes,
                    Artigo_chr_Ano       = :ano,
                    Artigo_vch_Resumo    = :resumo,
                    Album_lng_Codigo     = :album,
                    Artigo_chr_OcultarMiniatura  = :ocultarMiniatura,
                    Artigo_chr_Carrossel = :carrossel,
                    Artigo_chr_Ativo     = :ativo,
                    Artigo_vch_Credito   = :credito,
                    Artigo_vch_FonteDescricao = :fonteDescricao,
                    Artigo_vch_FonteUrl       = :fonteUrl,
                    Artigo_dat_Agenda         = :agenda
                    WHERE Artigo_lng_Codigo   = :codigo " );
        
        $sLink = Util::formataUrl($oArtigo->sTitulo);
 
        $sSql->bindParam(':titulo',       ($oArtigo->sTitulo));
        $sSql->bindParam(':conteudo',     ($oArtigo->sConteudo));
        $sSql->bindParam(':dataAlteracao',($oArtigo->sDataAlteracao));
        $sSql->bindParam(':codigo',       ($oArtigo->iCodigo));
        $sSql->bindParam(':destaque',     ($oArtigo->sDestaque));
        $sSql->bindParam(':urlImagem',    ($oArtigo->sUrlImagem));
        $sSql->bindParam(':dia',          ($oArtigo->sDia));
        $sSql->bindParam(':mes',          ($oArtigo->sMes));
        $sSql->bindParam(':ano',          ($oArtigo->sAno));
        $sSql->bindParam(':resumo',       ($oArtigo->sResumo));
        $sSql->bindParam(':album',        ($oArtigo->oAlbum->iCodigo));
        $sSql->bindParam(':ocultarMiniatura',($oArtigo->sOcultarMiniatura));
        $sSql->bindParam(':carrossel',    ($oArtigo->sCarrossel));
        $sSql->bindParam(':ativo',        ($oArtigo->sAtivo));
        $sSql->bindParam(':credito',      ($oArtigo->sCredito));
        //$sSql->bindParam(':link',          ($sLink));
        $sSql->bindParam(':fonteDescricao',($oArtigo->sFonteDescricao));
        $sSql->bindParam(':fonteUrl',      ($oArtigo->sFonteUrl));
        $sSql->bindParam(':agenda',        ($oArtigo->sAgenda));
        
        $sSql->execute();
    }
    
    public static final function excluiArtigo( $iCodigo  )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Artigo 
                    WHERE Artigo_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
    
    public static final function contaVisita( $oArtigo )
    {
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Artigo SET
                    Artigo_lng_TotalDeVisitas = Artigo_lng_TotalDeVisitas + 1
                    WHERE Artigo_lng_Codigo = :codigo " );

        $sSql->bindParam(':codigo',       ($oArtigo->iCodigo));
        
        $sSql->execute();
        
    }
    
    public static final function contaArtigo( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sJoin    = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            //self::$mArrCampos['CONDICAO'] = '';
        }
        
        // Define os Joins
        if (isset(self::$mArrJoin['JOIN']))
        {
            if ( self::$mArrJoin['JOIN'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrJoin['JOIN']); $a < $iCount; ++$a)
                {
                    $sJoin .= (self::$mArrJoin['JOIN'][$a]);
                }
            }
            //Limpa Filtros
            //self::$mArrJoin['JOIN'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           count(*)
                           FROM Artigo
                           ".$sJoin."
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        //echo $mResultado->queryString;
        $mResultado->execute();
        
        $iTotal = $mResultado->fetchColumn();
        
        return $iTotal;     
    }
    
   
}

?>