<?php

class RelatorioDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Relatorio_lng_Codigo';
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function pesquisaRelatorioLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Relatorio_lng_Codigo,
                           Relatorio_dat_Data
                           FROM Relatorio
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjRelatorio = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oRelatorio  = new Relatorio;
                
                $oRelatorio->iCodigo            = $mArrDados[$a]['Relatorio_lng_Codigo'];
                $oRelatorio->sData              = $mArrDados[$a]['Relatorio_dat_Data'];
                
                $arrObjRelatorio->append($oRelatorio);
            }
        }
        return $arrObjRelatorio;
    }
    public static final function pesquisaRelatorio(  )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
        
             
        $mResultado = $oConexao->prepare("SELECT
                           Relatorio_lng_Codigo,
                           Relatorio_dat_Data
                           FROM Relatorio
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        //echo $mResultado->queryString;
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);

         /* Instancia o Objeto */
        $oRelatorio = new Relatorio;
        
        if (is_array($mArrDados))
        { 
           $oRelatorio->iCodigo    = $mArrDados['Relatorio_lng_Codigo'];
           $oRelatorio->sData = $mArrDados['Relatorio_dat_Data'];
           
           RelatorioPedidoDB::setaFiltro(" AND Relatorio_Pedido_chr_Ativo = 'S'");
           RelatorioPedidoDB::setaFiltro(" AND Relatorio_lng_Codigo = ".$oRelatorio->iCodigo);
           $oRelatorio->arrObjRelatorioPedido = RelatorioPedidoDB::pesquisaRelatorioPedidoLista();

        }
        
        return $oRelatorio;     
    }
    
    public static final function alteraRelatorio( $oRelatorio )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Relatorio SET
                    Relatorio_dat_Data    = :descricao
                    WHERE Relatorio_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oRelatorio->iCodigo));
        $sSql->bindParam(':descricao', ($oRelatorio->sData));
        
        $sSql->execute();
    }
    
    public static final function salvaRelatorio( $oRelatorio )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Relatorio (
                    Relatorio_dat_Data
                    ) VALUES ( ? )"); 
        
        $sSql->bindParam(1, ($oRelatorio->sData));
        $sSql->execute();
        
        return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiRelatorio( $iCodigo  )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Relatorio 
                    WHERE Relatorio_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
 
    
 
}

?>