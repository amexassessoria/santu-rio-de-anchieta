<?php

class ArtigoTagDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Artigo_lng_Codigo';
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function pesquisaArtigoTagLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Artigo_lng_Codigo,
                           Art_tag.Tag_lng_Codigo,
                           Tag_vch_Descricao
                           FROM Art_tag
                           INNER JOIN Tag ON ( Tag.Tag_lng_Codigo = Art_tag.Tag_lng_Codigo )
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                         ");  
        
        $mResultado->execute();
        //echo $mResultado->queryString;

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjArtigoTag = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oArtigoTag  = new ArtigoTag;
                
                $oArtigoTag->iCodigoArtigo = $mArrDados[$a]['Artigo_lng_Codigo'];
                $oArtigoTag->iCodigoTag    = $mArrDados[$a]['Tag_lng_Codigo'];
                $oArtigoTag->sDescricaoTag = $mArrDados[$a]['Tag_vch_Descricao'];
                
                $arrObjArtigoTag->append($oArtigoTag);
                
            }
        }
        
        return $arrObjArtigoTag;
    }
    
    public static final function pesquisaArtigoTag( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Artigo_lng_Codigo,
                           Tag_lng_Codigo
                           FROM Art_tag
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oArtigoTag = new ArtigoTag;
        
        if (is_array($mArrDados))
        { 
           $oArtigoTag->iCodigoTag    = $mArrDados['Artigo_lng_Codigo'];
           $oArtigoTag->iCodigoArtigo = $mArrDados['Tag_lng_Codigo'];
        }
        
        return $oArtigoTag;     
    }
    

    
    public static final function salvaArtigoTag( $iCodigoArtigo, $iCodigoTag )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Art_tag (
                    Artigo_lng_Codigo,
                    Tag_lng_Codigo
                    ) VALUES ( ?,? )"); 
        
        $sSql->bindParam(1, $iCodigoArtigo);
        $sSql->bindParam(2, $iCodigoTag);
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiArtigoTag( $iCodigoArtigo )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Art_tag 
                    WHERE Artigo_lng_Codigo = :codigoArtigo ");
       
        $sSql->bindParam(':codigoArtigo',$iCodigoArtigo, PDO::PARAM_INT);   
        $sSql->execute();
    }

}
?>