<?php

class PopupDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Popup_lng_Codigo';
    private static $iLimite = 0;
    private static $iLimiteArtigo = 0;
    private static $iInicio = 0;
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function setaLimite($iArgLimite, $iArgInicio = 0)
    {
        self::$iLimite = $iArgLimite;
        self::$iInicio = $iArgInicio;
    }
    
    public static final function setaLimiteArtigo($iArgLimiteArtigo)
    {
        self::$iLimiteArtigo = $iArgLimiteArtigo;
    }
    
    public static final function pesquisaPopupListaCentralDeControle( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
        
        /* Define o Limite */
        if (self::$iLimite > 0)
        {
            $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

            //Limpa Filtro
            self::$iInicio = 0;
            self::$iLimite = 0;
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Popup_lng_Codigo,
                           Popup_vch_Descricao,
                           Popup_vch_ImagemUrl,
                           Popup_vch_Link,
                           Popup_dat_Entrada,
                           Popup_dat_Expiracao,
                           Popup_bit_LinkExterno
                           FROM Popup
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                           ".$sLimite."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjPopup = new ArrayObject();

        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oPopup  = new Popup;
                
                $oPopup->iCodigo      = $mArrDados[$a]['Popup_lng_Codigo'];
                $oPopup->sDescricao   = $mArrDados[$a]['Popup_vch_Descricao'];
                $oPopup->sImagemUrl   = 'images/banner/'.$mArrDados[$a]['Popup_vch_ImagemUrl'];
                $oPopup->sLink        = $mArrDados[$a]['Popup_vch_Link'];
                $oPopup->sEntrada     = $mArrDados[$a]['Popup_dat_Entrada'];
                $oPopup->sExpiracao   = $mArrDados[$a]['Popup_dat_Expiracao'];
                $oPopup->bLinkExterno = $mArrDados[$a]['Popup_bit_LinkExterno'];
                
                $arrObjPopup->append($oPopup);
            }
        }
        return $arrObjPopup;
    }
    
    public static final function pesquisaPopup( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Popup_lng_Codigo,
                           Popup_vch_Descricao,
                           Popup_vch_ImagemUrl,
                           Popup_vch_Link,
                           Popup_dat_Entrada,
                           Popup_dat_Expiracao,
                           Popup_bit_LinkExterno
                           FROM Popup
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oPopup = new Popup;
        
        if (is_array($mArrDados))
        { 
            $oPopup->iCodigo      = $mArrDados['Popup_lng_Codigo'];
            $oPopup->sDescricao   = $mArrDados['Popup_vch_Descricao'];
            $oPopup->sImagemUrl   = $mArrDados['Popup_vch_ImagemUrl'];
            $oPopup->sLink        = $mArrDados['Popup_vch_Link'];
            $oPopup->sEntrada     = $mArrDados['Popup_dat_Entrada'];
            $oPopup->sExpiracao   = $mArrDados['Popup_dat_Expiracao'];
            $oPopup->bLinkExterno = $mArrDados['Popup_bit_LinkExterno'];
        }
        
        return $oPopup;     
    }

    public static final function pesquisaPopupAtivo( )
    {
        $oConexao = db::conectar();
             
        $mResultado = $oConexao->prepare("SELECT
                           Popup_lng_Codigo,
                           Popup_vch_Descricao,
                           Popup_vch_ImagemUrl,
                           Popup_vch_Link,
                           Popup_dat_Entrada,
                           Popup_dat_Expiracao,
                           Popup_bit_LinkExterno
                           FROM Popup
                           WHERE (now() BETWEEN Popup_dat_Entrada AND Popup_dat_Expiracao)
                           ORDER BY Popup_lng_Codigo DESC
                           LIMIT 1                  
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        
        if (is_array($mArrDados))
        { 
            $oPopup = new Popup;

            $oPopup->iCodigo      = $mArrDados['Popup_lng_Codigo'];
            $oPopup->sDescricao   = $mArrDados['Popup_vch_Descricao'];
            $oPopup->sImagemUrl   = $mArrDados['Popup_vch_ImagemUrl'];
            $oPopup->sLink        = $mArrDados['Popup_vch_Link'];
            $oPopup->sEntrada     = $mArrDados['Popup_dat_Entrada'];
            $oPopup->sExpiracao   = $mArrDados['Popup_dat_Expiracao'];
            $oPopup->bLinkExterno = $mArrDados['Popup_bit_LinkExterno'];

            return $oPopup;     
        } else {
            return false;
        }
        
        
    }
    
    public static final function alteraPopup( $oPopup )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Popup SET

                    Popup_vch_Descricao = :descricao,
                    Popup_vch_ImagemUrl = :url,
                    Popup_vch_Link = :link,
                    Popup_dat_Entrada = :entrada,
                    Popup_dat_Expiracao = :expiracao,
                    Popup_bit_LinkExterno = :externo
                    WHERE Popup_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oPopup->iCodigo));
        $sSql->bindParam(':descricao', ($oPopup->sDescricao));
        $sSql->bindParam(':url',       ($oPopup->sImagemUrl));
        $sSql->bindParam(':link',      ($oPopup->sLink));
        $sSql->bindParam(':entrada',   ($oPopup->sEntrada));
        $sSql->bindParam(':expiracao', ($oPopup->sExpiracao));
        $sSql->bindParam(':externo',   ($oPopup->bLinkExterno));
        
        $sSql->execute();
    }
    
    public static final function salvaPopup( $oPopup )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Popup (
                    Popup_vch_Descricao,
                    Popup_vch_ImagemUrl,
                    Popup_vch_Link,
                    Popup_dat_Entrada,
                    Popup_dat_Expiracao,
                    Popup_bit_LinkExterno,
                    Popup_dat_Cadastro,
                    Usuario_lng_Codigo
                    ) VALUES ( ?,?,?,?,?,?,?,?)"); 
        
        $sSql->bindParam(1, ($oPopup->sDescricao));
        $sSql->bindParam(2, ($oPopup->sImagemUrl));
        $sSql->bindParam(3, ($oPopup->sLink));
        $sSql->bindParam(4, ($oPopup->sEntrada));
        $sSql->bindParam(5, ($oPopup->sExpiracao));
        $sSql->bindParam(6, ($oPopup->bLinkExterno));
        $sSql->bindParam(7, ($oPopup->sCadastro));
        $sSql->bindParam(8, ($oPopup->iCodigoUsuario));
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiPopup( $iCodigo  )
    {
        PopupDB::setaFiltro(' AND Popup_lng_Codigo = '.$_GET['codigo']);
        $oPopup = PopupDB::pesquisaPopup();
    
        $file = "images/banner/" . $oPopup->sImagemUrl;
    
        if ( file_exists( $file ) )
        {
            @unlink( $file );
        }
    
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Popup 
                    WHERE Popup_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
 
    
 
}

?>