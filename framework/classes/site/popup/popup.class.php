<?php

class Popup
{
    
    /* Atributos */
    private $iCodigo;
    private $sDescricao;
    private $sImagemUrl;
    private $sLink;
    private $bLinkExterno;
    private $sExpiracao;
    private $sCadastro;
    private $sEntrada;

    private $iCodigoUsuario;
    
    /* M�todos m�gicos GET e SET */
    public function __get($property) 
    {
        if (property_exists($this, $property)) 
        {
          return $this->$property;
        }
    }
    
    public function __set($property, $value) 
    {
        if (property_exists($this, $property)) 
        {
          $this->$property = $value;
        }
        return $this;
    }

}

?>