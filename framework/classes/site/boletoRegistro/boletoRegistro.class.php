<?php

class BoletoRegistro
{
    
    /* Atributos */
    private $iCodigo;
    private $iCodigoColaborador;
    private $iCodigoCampanha;
    private $sEmissao;
    private $sVencimento;
    private $fValor;
    private $iSequenciaEmissao;
    private $sNossoNumero;
    private $sNumeroDocumento;
    private $sLinhaDigitavel;
    
    /* M�todos m�gicos GET e SET */
    public function __get($property) 
    {
        if (property_exists($this, $property)) 
        {
          return $this->$property;
        }
    }
    
    public function __set($property, $value) 
    {
        if (property_exists($this, $property)) 
        {
          $this->$property = $value;
        }
        return $this;
    }

}

?>