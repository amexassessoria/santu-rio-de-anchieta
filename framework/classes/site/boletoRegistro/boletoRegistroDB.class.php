<?php

class BoletoRegistroDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Boleto_Registro_lng_Codigo';
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    
    public static final function salvaBoletoRegistro( $oBoletoRegistro )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Boleto_Registro (
                           Colaborador_lng_Codigo,
                           Campanha_lng_Codigo,
                           Boleto_Registro_dat_Emissao,
                           Boleto_Registro_dat_Vencimento,
                           Boleto_Registro_dec_Valor,
                           Boleto_Registro_lng_Sequencia_Emissao,
                           Boleto_Registro_vch_Nosso_Numero,
                           Boleto_Registro_vch_Numero_Documento,
                           Boleto_Registro_chr_Linha_Digitiavel
                    ) VALUES ( ?,?,?,?,?,?,?,?,? )"); 
        
        $sSql->bindParam(1,  ($oBoletoRegistro->iCodigoColaborador));
        $sSql->bindParam(2,  ($oBoletoRegistro->iCodigoCampanha));
        $sSql->bindParam(3,  ($oBoletoRegistro->sEmissao));
        $sSql->bindParam(4,  ($oBoletoRegistro->sVencimento));
        $sSql->bindParam(5,  ($oBoletoRegistro->fValor));
        $sSql->bindParam(6,  ($oBoletoRegistro->iSequenciaEmissao));
        $sSql->bindParam(7,  ($oBoletoRegistro->sNossoNumero));
        $sSql->bindParam(8,  ($oBoletoRegistro->sNumeroDocumento));
        $sSql->bindParam(9,  ($oBoletoRegistro->sLinhaDigitavel));
        
        $sSql->execute();
        //echo ($sSql->queryString);
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
        //var_dump($oBoletoRegistro);
    }
    
    
    public static final function incrementaSequencial( $iSequencial, $iCodigo )
    {
        $oConexao = db::conectar();
   
        $sSql=$oConexao->prepare("UPDATE Boleto SET
                    Boleto_lng_Sequencial = ".$iSequencial." WHERE Boleto_lng_Codigo = ".$iCodigo);
        $sSql->execute();
    }
    
 
}

?>