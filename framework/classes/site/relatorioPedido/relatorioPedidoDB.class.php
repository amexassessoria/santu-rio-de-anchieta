<?php

class RelatorioPedidoDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Relatorio_Pedido_lng_Codigo';
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function pesquisaRelatorioPedidoLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Relatorio_Pedido_lng_Codigo,
                           Pedido_lng_Codigo,
                           Relatorio_lng_Codigo,
                           Relatorio_Pedido_chr_Ativo
                           FROM Relatorio_Pedido
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                         ");  
        
        $mResultado->execute();
        //echo $mResultado->queryString;

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjRelatorioPedido = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oRelatorioPedido  = new RelatorioPedido;
                
                PedidoDB::setaFiltro(" AND Pedido_lng_Codigo = ".$mArrDados[$a]['Pedido_lng_Codigo']);
                $oPedido = PedidoDB::pesquisaPedido();
                
                $oRelatorioPedido->iCodigo = $mArrDados[$a]['Relatorio_Pedido_lng_Codigo'];
                $oRelatorioPedido->sAtivo  = $mArrDados[$a]['Relatorio_Pedido_chr_Ativo'];
                $oRelatorioPedido->oPedido = $oPedido;
                
                $arrObjRelatorioPedido->append($oRelatorioPedido);
            }
        }
        return $arrObjRelatorioPedido;
    }
    public static final function pesquisaRelatorioPedido( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           *
                           FROM Relatorio_Pedido
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oRelatorioPedido = new RelatorioPedido;
        
        if (is_array($mArrDados))
        { 
            /* Instancia o Objeto */
            $oRelatorioPedido  = new RelatorioPedido;

            PedidoDB::setaFiltro(" AND Pedido_lng_Codigo = ".$mArrDados[$a]['Pedido_lng_Codigo']);
            $oPedido = PedidoDB::pesquisaPedido();

            $oRelatorioPedido->iCodigo = $mArrDados['Relatorio_Pedido_lng_Codigo'];
            $oRelatorioPedido->sAtivo  = $mArrDados['Relatorio_Pedido_dat_Data'];
            $oRelatorioPedido->oPedido = $oPedido;
        }
        
        return $oRelatorioPedido;     
    }
    
    public static final function alteraRelatorioPedido( $oRelatorioPedido )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Relatorio_Pedido SET
                    Relatorio_Pedido_chr_Ativo    = :ativo
                    WHERE Relatorio_Pedido_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oRelatorioPedido->iCodigo));
        $sSql->bindParam(':ativo', ($oRelatorioPedido->sAtivo));
        
        $sSql->execute();
    }
    
    public static final function salvaRelatorioPedido( $oRelatorioPedido )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Relatorio_Pedido (
                    Relatorio_Pedido_chr_Ativo,
                    Pedido_lng_Codigo,
                    Relatorio_lng_Codigo
                    ) VALUES ( ?,?,? )"); 
        
        $sSql->bindParam(1, ($oRelatorioPedido->sAtivo));
        $sSql->bindParam(2, ($oRelatorioPedido->iPedido));
        $sSql->bindParam(3, ($oRelatorioPedido->iRelatorio));
        
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    } 
    
    public static final function excluiRelatorioPedido( $iCodigoRelatorio )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Relatorio_Pedido 
                    WHERE Relatorio_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigoRelatorio, PDO::PARAM_INT);   
        $sSql->execute();
    }
    
    public static final function excluiRelatorioPedidoPorPedido( $iCodigoPedido )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Relatorio_Pedido 
                    WHERE Pedido_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigoPedido, PDO::PARAM_INT);   
        $sSql->execute();
    }
}

?>