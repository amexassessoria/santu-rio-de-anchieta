<?php

class ColaboradorDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Colaborador_lng_Codigo';
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function pesquisaColaboradorLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Colaborador_lng_Codigo,
                           Colaborador_vch_Nome,
                           Colaborador_vch_Email,
                           Colaborador_vch_Endereco,
                           Colaborador_vch_Bairro,
                           Colaborador_vch_Cidade,
                           Colaborador_chr_Estado,
                           Colaborador_vch_Telefone,
                           Colaborador_lng_Numero,
                           Colaborador_chr_CPF,
                           Colaborador_dat_DataNascimento,
                           Colaborador_vch_Complemento,
                           Logradouro_lng_Codigo,
                           Colaborador_chr_Sexo,
                           Colaborador_chr_CEP,
                           Colaborador_dat_DataCadastro
                           FROM Colaborador
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjColaborador = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oColaborador  = new Colaborador;
                
                LogradouroDB::setaFiltro(' AND Logradouro_lng_Codigo = '.$mArrDados[$a]['Logradouro_lng_Codigo']);
                $oLogradoro = LogradouroDB::pesquisaLogradouro();
                
                $oColaborador->iCodigo         = $mArrDados[$a]['Colaborador_lng_Codigo'];
                $oColaborador->sCPF            = $mArrDados[$a]['Colaborador_chr_CPF'];
                $oColaborador->sNome           = $mArrDados[$a]['Colaborador_vch_Nome'];
                $oColaborador->sEndereco       = $mArrDados[$a]['Colaborador_vch_Endereco'];
                $oColaborador->sBairro         = $mArrDados[$a]['Colaborador_vch_Bairro'];
                $oColaborador->sCidade         = $mArrDados[$a]['Colaborador_vch_Cidade'];
                $oColaborador->sCEP            = $mArrDados[$a]['Colaborador_chr_CEP'];
                $oColaborador->sEstado         = $mArrDados[$a]['Colaborador_chr_Estado'];
                $oColaborador->iNumero         = $mArrDados[$a]['Colaborador_lng_Numero'];
                $oColaborador->sTelefone       = $mArrDados[$a]['Colaborador_vch_Telefone'];
                $oColaborador->sDataNascimento = $mArrDados[$a]['Colaborador_dat_DataNascimento'];
                $oColaborador->sSexo           = $mArrDados[$a]['Colaborador_chr_Sexo'];
                $oColaborador->sEmail          = $mArrDados[$a]['Colaborador_vch_Email'];
                $oColaborador->sDataCadastro   = $mArrDados[$a]['Colaborador_dat_DataCadastro'];
                $oColaborador->sComplemento    = $mArrDados[$a]['Colaborador_vch_Complemento'];
                $oColaborador->oLogradouro     = $oLogradoro;
                
                $arrObjColaborador->append($oColaborador);
            }
        }
        return $arrObjColaborador;
    }
    public static final function pesquisaColaborador( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Colaborador_lng_Codigo,
                           Colaborador_chr_CPF,
                           Colaborador_vch_Nome,
                           Colaborador_vch_Endereco,
                           Colaborador_lng_Numero,
                           Colaborador_vch_Bairro,
                           Colaborador_vch_Cidade,
                           Colaborador_chr_Estado,
                           Colaborador_chr_CEP
                           FROM Colaborador
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
         /* Instancia o Objeto */
        $oColaborador = new Colaborador;
        
        if (is_array($mArrDados))
        { 
           $oColaborador->iCodigo   = $mArrDados['Colaborador_lng_Codigo'];
           $oColaborador->sNome     = $mArrDados['Colaborador_vch_Nome'];
           $oColaborador->sCPF      = $mArrDados['Colaborador_chr_CPF'];
           $oColaborador->sEndereco = $mArrDados['Colaborador_vch_Endereco'];
           $oColaborador->sBairro   = $mArrDados['Colaborador_vch_Bairro'];
           $oColaborador->sCidade   = $mArrDados['Colaborador_vch_Cidade'];
           $oColaborador->sCEP      = $mArrDados['Colaborador_chr_CEP'];
           $oColaborador->sEstado   = $mArrDados['Colaborador_chr_Estado'];
           $oColaborador->iNumero   = $mArrDados['Colaborador_lng_Numero'];
        }
        
        return $oColaborador;     
    }
    
    public static final function alteraColaborador( $oColaborador )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Colaborador SET
                    Colaborador_chr_CPF    = :descricao
                    WHERE Colaborador_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oColaborador->iCodigo));
        $sSql->bindParam(':descricao', ($oColaborador->sCPF));
        
        $sSql->execute();
    }
    
    public static final function salvaColaborador( $oColaborador )
    { 
        try 
        {
            $oConexao = db::conectar();

            $sSql = $oConexao->prepare("INSERT INTO Colaborador (
                        Colaborador_vch_Nome,
                        Colaborador_vch_Email,
                        Colaborador_vch_Endereco,
                        Colaborador_vch_Bairro,
                        Colaborador_vch_Cidade,
                        Colaborador_chr_Estado,
                        Colaborador_vch_Telefone,
                        Colaborador_lng_Numero,
                        Colaborador_chr_CPF,
                        Colaborador_dat_DataNascimento,
                        Colaborador_vch_Complemento,
                        Logradouro_lng_Codigo,
                        Colaborador_chr_Sexo,
                        Colaborador_chr_CEP,
                        Colaborador_dat_DataCadastro
                        ) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )"); 

            $sSql->bindParam(1, ($oColaborador->sNome));
            $sSql->bindParam(2, ($oColaborador->sEmail));
            $sSql->bindParam(3, ($oColaborador->sEndereco));
            $sSql->bindParam(4, ($oColaborador->sBairro));
            $sSql->bindParam(5, ($oColaborador->sCidade));
            $sSql->bindParam(6, ($oColaborador->sEstado));
            $sSql->bindParam(7, ($oColaborador->sTelefone));
            $sSql->bindParam(8, ($oColaborador->iNumero));
            $sSql->bindParam(9, ($oColaborador->sCPF));
            $sSql->bindParam(10,($oColaborador->sDataNascimento));
            $sSql->bindParam(11,($oColaborador->sComplemento));
            $sSql->bindParam(12,($oColaborador->oLogradouro->iCodigo));
            $sSql->bindParam(13,($oColaborador->sSexo));
            $sSql->bindParam(14,($oColaborador->sCEP));
            $sSql->bindParam(15,(date('Y-m-d')));
            $sSql->execute();
            
            return $oConexao->lastInsertId(); 
        }
        catch (Exception $e) 
        {
            return false;
        }
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiColaborador( $iCodigo  )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Colaborador 
                    WHERE Colaborador_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
 
    
 
}

?>