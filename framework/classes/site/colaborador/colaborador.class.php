<?php

class Colaborador
{
    
    /* Atributos */
    
    private $oLogradouro;
    
    private $iCodigo;
    private $sNome;
    private $sEmail;
    private $sEndereco;
    private $sBairro;
    private $sCidade;
    private $sCEP;
    private $sEstado;
    private $sTelefone;
    private $iNumero;
    private $sCPF;
    private $sDataNascimento;
    private $sSexo;
    private $sDataCadastro;
    private $sComplemento;
    
    
    
    /* M�todos m�gicos GET e SET */
    public function __get($property) 
    {
        if (property_exists($this, $property)) 
        {
          return $this->$property;
        }
    }
    
    public function __set($property, $value) 
    {
        if (property_exists($this, $property)) 
        {
          $this->$property = $value;
        }
        return $this;
    }

}

?>