<?php

class ParametroDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = '';
    private static $iLimite = 0;
    private static $iInicio = 0;
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function setaLimite($iArgLimite, $iArgInicio = 0)
    {
        self::$iLimite = $iArgLimite;
        self::$iInicio = $iArgInicio;
    }
    
    public static final function pesquisaParametro( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Parametro_txt_RespostaTestemunho,
                           Parametro_txt_RespostaPedidoOracao
                           FROM Parametro
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);

         /* Instancia o Objeto */
        $oParametro = new Parametro;
        
        if (is_array($mArrDados))
        { 
           $oParametro->sRespostaTestemunho   = $mArrDados['Parametro_txt_RespostaTestemunho'];
           $oParametro->sRespostaPedidoOracao = $mArrDados['Parametro_txt_RespostaPedidoOracao'];
        }
        
        return $oParametro;     
    }
    
    public static final function alteraParametro( $oParametro )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Parametro SET
                    Parametro_txt_RespostaTestemunho    = :respostaTestemunho,
                    Parametro_txt_RespostaPedidoOracao  = :respostaPedidoOracao " );
 
        $sSql->bindParam(':respostaTestemunho',  ($oParametro->sRespostaTestemunho));
        $sSql->bindParam(':respostaPedidoOracao',($oParametro->sRespostaPedidoOracao));
        
        $sSql->execute();
    }
    
 
}

?>