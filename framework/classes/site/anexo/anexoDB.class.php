<?php

class AnexoDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Anexo_lng_Codigo DESC';
    private static $iLimite = 0;
    private static $iInicio = 0;
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function setaLimite($iArgLimite, $iArgInicio = 0)
    {
        self::$iLimite = $iArgLimite;
        self::$iInicio = $iArgInicio;
    }
    
    public static final function pesquisaAnexoLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
        
        /* Define o Limite */
        if (self::$iLimite > 0)
        {
            $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

            //Limpa Filtro
            self::$iInicio = 0;
            self::$iLimite = 0;
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Anexo_lng_Codigo,
                           Anexo_vch_Titulo,
                           Anexo_vch_Caminho,
                           Anexo_dat_Cadastro
                           FROM Anexo
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                           ".$sLimite."
                         ");  
        
        $mResultado->execute();
        //echo $mResultado->queryString;

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjAnexo = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oAnexo  = new Anexo;
                
                $oAnexo->iCodigo    = $mArrDados[$a]['Anexo_lng_Codigo'];
                $oAnexo->sTitulo    = $mArrDados[$a]['Anexo_vch_Titulo'];
                $oAnexo->sCaminho   = str_replace('%2F', '/', urlencode($mArrDados[$a]['Anexo_vch_Caminho']));
                $oAnexo->sCadastro  = $mArrDados[$a]['Anexo_dat_Cadastro'];
                
                $arrObjAnexo->append($oAnexo);
            }
        }
        return $arrObjAnexo;
    }
    public static final function pesquisaAnexo( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Anexo_lng_Codigo,
                           Anexo_vch_Titulo,
                           Anexo_vch_Caminho,
                           Anexo_dat_Cadastro
                           FROM Anexo
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oAnexo = new Anexo;
        
        if (is_array($mArrDados))
        { 
           $oAnexo->iCodigo    = $mArrDados['Anexo_lng_Codigo'];
           $oAnexo->sTitulo    = $mArrDados['Anexo_vch_Titulo'];
           $oAnexo->sCaminho   = $mArrDados['Anexo_vch_Caminho'];
           $oAnexo->sCadastro  = $mArrDados['Anexo_dat_Cadastro'];
        }
        
        return $oAnexo;     
    }
    
    public static final function alteraAnexo( $oAnexo )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Anexo SET
                    Anexo_vch_Titulo       = :titulo,
                    Anexo_vch_Caminho      = :caminho
                    WHERE Anexo_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oAnexo->iCodigo));
        $sSql->bindParam(':titulo',    ($oAnexo->sTitulo));
        $sSql->bindParam(':caminho',   ($oAnexo->sCaminho));
        
        $sSql->execute();
    }
    
    public static final function salvaAnexo( $oAnexo )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Anexo (
                    Anexo_vch_Titulo,
                    Anexo_vch_Caminho,
                    Anexo_dat_Cadastro
                    ) VALUES ( ?,?,? )"); 
        
        $sSql->bindParam(1, ($oAnexo->sTitulo));
        $sSql->bindParam(2, ($oAnexo->sCaminho));
        $sSql->bindParam(3, ($oAnexo->sCadastro));
        
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiAnexo( $iCodigo  )
    {
        AnexoDB::setaFiltro(' AND Anexo_lng_Codigo = '.$iCodigo);
        $oAnexo = AnexoDB::pesquisaAnexo();
    
        $file = "informativos/" . $oAnexo->sCaminho;
    
        if ( file_exists( $file ) )
        {
            @unlink( $file );
        }
        
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Anexo 
                    WHERE Anexo_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
    public static final function contaAnexo( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sJoin    = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            //self::$mArrCampos['CONDICAO'] = '';
        }
        
        // Define os Joins
        if (isset(self::$mArrJoin['JOIN']))
        {
            if ( self::$mArrJoin['JOIN'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrJoin['JOIN']); $a < $iCount; ++$a)
                {
                    $sJoin .= (self::$mArrJoin['JOIN'][$a]);
                }
            }
            //Limpa Filtros
            //self::$mArrJoin['JOIN'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           count(*)
                           FROM Anexo
                           ".$sJoin."
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        //echo $mResultado->queryString;
        $mResultado->execute();
        
        $iTotal = $mResultado->fetchColumn();
        
        return $iTotal;     
    }
    
 
}

?>