<?php

class InformativoDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Informativo_lng_Codigo';
    private static $iLimite = 0;
    private static $iInicio = 0;
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function setaLimite($iArgLimite, $iArgInicio = 0)
    {
        self::$iLimite = $iArgLimite;
        self::$iInicio = $iArgInicio;
    }
    
    public static final function pesquisaInformativoLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
        
        /* Define o Limite */
        if (self::$iLimite > 0)
        {
            $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

            //Limpa Filtro
            self::$iInicio = 0;
            self::$iLimite = 0;
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Informativo_lng_Codigo,
                           Informativo_vch_Titulo,
                           Informativo_vch_Url,
                           Informativo_chr_Mes,
                           Informativo_chr_Ano,
                           Informativo_lng_Numero,
                           Informativo_txt_Descricao,
                           Informativo_vch_EmbedCode,
                           Informativo_vch_IssuuLink
                           FROM Informativo
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                           ".$sLimite."
                         ");  
        
        $mResultado->execute();
        //echo $mResultado->queryString;

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjInformativo = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oInformativo  = new Informativo;
                
                $oInformativo->iCodigo    = $mArrDados[$a]['Informativo_lng_Codigo'];
                $oInformativo->sTitulo    = $mArrDados[$a]['Informativo_vch_Titulo'];
                $oInformativo->sUrl       = $mArrDados[$a]['Informativo_vch_Url'];
                $oInformativo->sMes       = $mArrDados[$a]['Informativo_chr_Mes'];
                $oInformativo->sAno       = $mArrDados[$a]['Informativo_chr_Ano'];
                $oInformativo->iNumero    = $mArrDados[$a]['Informativo_lng_Numero'];
                $oInformativo->sDescricao = $mArrDados[$a]['Informativo_txt_Descricao'];
                $oInformativo->sEmbedCode = $mArrDados[$a]['Informativo_vch_EmbedCode'];
                $oInformativo->sIssuuLink = $mArrDados[$a]['Informativo_vch_IssuuLink'];
                
                $arrObjInformativo->append($oInformativo);
            }
        }
        return $arrObjInformativo;
    }
    public static final function pesquisaInformativo( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Informativo_lng_Codigo,
                           Informativo_vch_Titulo,
                           Informativo_vch_Url,
                           Informativo_chr_Mes,
                           Informativo_chr_Ano,
                           Informativo_lng_Numero,
                           Informativo_txt_Descricao,
                           Informativo_vch_EmbedCode,
                           Informativo_vch_IssuuLink
                           FROM Informativo
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oInformativo = new Informativo;
        
        if (is_array($mArrDados))
        { 
           $oInformativo->iCodigo    = $mArrDados['Informativo_lng_Codigo'];
           $oInformativo->sTitulo    = $mArrDados['Informativo_vch_Titulo'];
           $oInformativo->sUrl       = $mArrDados['Informativo_vch_Url'];
           $oInformativo->sMes       = $mArrDados['Informativo_chr_Mes'];
           $oInformativo->sAno       = $mArrDados['Informativo_chr_Ano'];
           $oInformativo->iNumero    = $mArrDados['Informativo_lng_Numero'];
           $oInformativo->sDescricao = $mArrDados['Informativo_txt_Descricao'];
           $oInformativo->sEmbedCode = $mArrDados['Informativo_vch_EmbedCode'];
           $oInformativo->sIssuuLink = $mArrDados['Informativo_vch_IssuuLink'];
        }
        
        return $oInformativo;     
    }
    
    public static final function alteraInformativo( $oInformativo )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Informativo SET
                    Informativo_vch_Titulo       = :titulo,
                    Informativo_vch_Url          = :url,
                    Informativo_chr_Mes          = :mes,
                    Informativo_chr_Ano          = :ano,
                    Informativo_lng_Numero       = :numero,
                    Informativo_txt_Descricao    = :descricao,
                    Informativo_vch_EmbedCode    = :embedCode,
                    Informativo_vch_IssuuLink    = :issuuLink
                    WHERE Informativo_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oInformativo->iCodigo));
        $sSql->bindParam(':titulo',    ($oInformativo->sTitulo));
        $sSql->bindParam(':mes',       ($oInformativo->sMes));
        $sSql->bindParam(':ano',       ($oInformativo->sAno));
        $sSql->bindParam(':url',       ($oInformativo->sUrl));
        $sSql->bindParam(':numero',    ($oInformativo->iNumero));
        $sSql->bindParam(':descricao', ($oInformativo->sDescricao));
        $sSql->bindParam(':embedCode', ($oInformativo->sEmbedCode));
        $sSql->bindParam(':issuuLink', ($oInformativo->sIssuuLink));
        
        $sSql->execute();
    }
    
    public static final function salvaInformativo( $oInformativo )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Informativo (
                    Informativo_vch_Titulo,
                    Informativo_vch_Url,
                    Informativo_chr_Mes,
                    Informativo_chr_Ano,
                    Informativo_lng_Numero,
                    Informativo_txt_Descricao,
                    Informativo_vch_EmbedCode,
                    Informativo_vch_IssuuLink
                    ) VALUES ( ?,?,?,?,?,?,?,? )"); 
        
        $sSql->bindParam(1, ($oInformativo->sTitulo));
        $sSql->bindParam(2, ($oInformativo->sUrl));
        $sSql->bindParam(3, ($oInformativo->sMes));
        $sSql->bindParam(4, ($oInformativo->sAno));
        $sSql->bindParam(5, ($oInformativo->iNumero));
        $sSql->bindParam(6, ($oInformativo->sDescricao));
        $sSql->bindParam(7, ($oInformativo->sEmbedCode));
        $sSql->bindParam(8, ($oInformativo->sIssuuLink));
        
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiInformativo( $iCodigo  )
    {
        InformativoDB::setaFiltro(' AND Informativo_lng_Codigo = '.$iCodigo);
        $oInformativo = InformativoDB::pesquisaInformativo();
    
        $file = "informativos/" . $oInformativo->sUrl;
    
        if ( file_exists( $file ) )
        {
            @unlink( $file );
        }
        
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Informativo 
                    WHERE Informativo_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
	public static final function contaInformativo( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sJoin    = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            //self::$mArrCampos['CONDICAO'] = '';
        }
        
        // Define os Joins
        if (isset(self::$mArrJoin['JOIN']))
        {
            if ( self::$mArrJoin['JOIN'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrJoin['JOIN']); $a < $iCount; ++$a)
                {
                    $sJoin .= (self::$mArrJoin['JOIN'][$a]);
                }
            }
            //Limpa Filtros
            //self::$mArrJoin['JOIN'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           count(*)
                           FROM Informativo
                           ".$sJoin."
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        //echo $mResultado->queryString;
        $mResultado->execute();
        
        $iTotal = $mResultado->fetchColumn();
        
        return $iTotal;     
    }
    
 
}

?>