<?php

class Pedido
{
    
    /* Atributos */
    private $iCodigo;
    private $sNome;
    private $sEmail;
    private $sCidade;
    private $sEstado;
    private $sIntencao;
    private $sExibirIntencao;
    private $sData;
    private $sTipo;
    private $iDias;
    private $sAprovado;
    private $sRespondido;
    private $sImagemUrl;
    
    /* M�todos m�gicos GET e SET */
    public function __get($property) 
    {
        if (property_exists($this, $property)) 
        {
          return $this->$property;
        }
    }
    
    public function __set($property, $value) 
    {
        if (property_exists($this, $property)) 
        {
          $this->$property = $value;
        }
        return $this;
    }

}

?>