<?php

class PedidoDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem     = 'Pedido_lng_Codigo DESC';
    private static $iLimite    = '';
    private static $iInicio    = '';
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaLimite($iArgLimite, $iArgInicio = 0)
    {
        self::$iLimite = $iArgLimite;
        self::$iInicio = $iArgInicio;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function pesquisaPedidoLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
        
        if ( self::$iLimite <> '')
        {
            $sLimite = " LIMIT ".self::$iInicio." , ".self::$iLimite;

            //Limpa Filtro
            self::$iInicio = '';
            self::$iLimite = '';
        }
        
        $mResultado = $oConexao->prepare("SELECT
                           Pedido_lng_Codigo,
                           Pedido_vch_Nome,
                           Pedido_vch_Email,
                           Pedido_vch_Cidade,
                           Pedido_chr_Estado,
                           Pedido_txt_Intencao,
                           Pedido_chr_ExibirIntencao,
                           Pedido_dat_Data,
                           Pedido_chr_Tipo,
                           Pedido_vch_UrlImagem
                           FROM Pedido
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                           ".$sLimite."
                         ");  
        
        //echo $mResultado->queryString;
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjArtigo = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oPedido  = new Pedido;
                
                $oPedido->iCodigo         = $mArrDados[$a]['Pedido_lng_Codigo'];
                $oPedido->sNome           = $mArrDados[$a]['Pedido_vch_Nome'];
                $oPedido->sEmail          = $mArrDados[$a]['Pedido_vch_Email'];
                $oPedido->sCidade         = $mArrDados[$a]['Pedido_vch_Cidade'];
                $oPedido->sEstado         = $mArrDados[$a]['Pedido_chr_Estado'];
                $oPedido->sIntencao       = $mArrDados[$a]['Pedido_txt_Intencao'];
                $oPedido->sExibirIntencao = $mArrDados[$a]['Pedido_chr_ExibirIntencao'];
                $oPedido->sData           = $mArrDados[$a]['Pedido_dat_Data'];
                $oPedido->sTipo           = $mArrDados[$a]['Pedido_chr_Tipo'];
                $oPedido->sImagemUrl      = $mArrDados[$a]['Pedido_vch_UrlImagem'];
                $oPedido->iDias           = Util::retornaDiferencaDeDias($mArrDados[$a]['Pedido_dat_Data'], date( 'Y-m-d' ));
                
                $arrObjArtigo->append($oPedido);
            }
        }
        return $arrObjArtigo;
    }
    public static final function pesquisaPedido( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Pedido_lng_Codigo,
                           Pedido_vch_Nome,
                           Pedido_vch_Email,
                           Pedido_chr_Estado,
                           Pedido_vch_Cidade,
                           Pedido_txt_Intencao,
                           Pedido_chr_ExibirIntencao,
                           Pedido_chr_Tipo,
                           Pedido_dat_Data,
                           Pedido_vch_UrlImagem
                           FROM Pedido
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $oPedido = new Pedido;
        
        if (is_array($mArrDados))
        { 
           $oPedido->iCodigo         = $mArrDados['Pedido_lng_Codigo'];
           $oPedido->sNome           = $mArrDados['Pedido_vch_Nome'];
           $oPedido->sEmail          = $mArrDados['Pedido_vch_Email'];
           $oPedido->sEstado         = $mArrDados['Pedido_chr_Estado'];
           $oPedido->sCidade         = $mArrDados['Pedido_vch_Cidade'];
           $oPedido->sIntencao       = $mArrDados['Pedido_txt_Intencao'];
           $oPedido->sExibirIntencao = $mArrDados['Pedido_chr_ExibirIntencao'];
           $oPedido->sData           = $mArrDados['Pedido_dat_Data'];
           $oPedido->sTipo           = $mArrDados['Pedido_chr_Tipo'];
           $oPedido->sImagemUrl      = $mArrDados['Pedido_vch_UrlImagem'];
        }
        
        return $oPedido;     
    }
    
    public static final function contaPedido( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            //self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           count(*)
                           FROM Pedido
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $iTotal = $mResultado->fetchColumn();
        
        return $iTotal;     
    }
    
    public static final function salvaPedido( $oPedido )
    {
        try 
        {
            $oConexao = db::conectar();

            $sSql = $oConexao->prepare("INSERT INTO Pedido (
                        Pedido_vch_Nome,
                        Pedido_vch_Email,
                        Pedido_chr_Estado,
                        Pedido_vch_Cidade,
                        Pedido_txt_Intencao,
                        Pedido_chr_ExibirIntencao,
                        Pedido_dat_Data,
                        Pedido_chr_Tipo,
                        Pedido_chr_Aprovado,
                        Pedido_chr_Respondido,
                        Pedido_vch_UrlImagem
                        ) VALUES ( ?,?,?,?,?,?,?,?,?,?,? )"); 

            $sSql->bindParam(1, ($oPedido->sNome));
            $sSql->bindParam(2, ($oPedido->sEmail));
            $sSql->bindParam(3, ($oPedido->sEstado));
            $sSql->bindParam(4, ($oPedido->sCidade));
            $sSql->bindParam(5, ($oPedido->sIntencao));
            $sSql->bindParam(6, ($oPedido->sExibirIntencao));
            $sSql->bindParam(7, ($oPedido->sData));
            $sSql->bindParam(8, ($oPedido->sTipo));
            $sSql->bindParam(9, ($oPedido->sAprovado));
            $sSql->bindParam(10,($oPedido->sRespondido));
            $sSql->bindParam(11,($oPedido->sImagemUrl));
            $sSql->execute();

            return $oConexao->lastInsertId(); 
        } 
        catch (Exception $e) 
        {
            return false;
        }
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiPedido( $iCodigo  )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Pedido 
                    WHERE Pedido_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
    
    public static final function aprovar( $iCodigo )
    {
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Pedido SET
                    Pedido_chr_Aprovado = 'S'
                    WHERE Pedido_lng_Codigo = :codigo " );

        $sSql->bindParam(':codigo',       ($iCodigo));
        
        $sSql->execute();
    }
    
    public static final function responder( $iCodigo )
    {
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Pedido SET
                    Pedido_chr_Respondido = 'S'
                    WHERE Pedido_lng_Codigo = :codigo " );

        $sSql->bindParam(':codigo',       ($iCodigo));
        
        $sSql->execute();
    }
 
}

?>