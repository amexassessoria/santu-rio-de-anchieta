<?php

class SubmenuDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Submenu_lng_Posicao';
    private static $iLimite = 0;
    private static $iInicio = 0;
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function setaLimite($iArgLimite, $iArgInicio = 0)
    {
        self::$iLimite = $iArgLimite;
        self::$iInicio = $iArgInicio;
    }
    
    public static final function pesquisaSubmenuLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
        
        /* Define o Limite */
        if (self::$iLimite > 0)
        {
            $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

            //Limpa Filtro
            self::$iInicio = 0;
            self::$iLimite = 0;
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Submenu_lng_Codigo,
                           Submenu_vch_Descricao,
                           Submenu_vch_Link,
                           Submenu_chr_Ativo,
                           Menu_lng_Codigo
                           FROM Submenu
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                           ".$sLimite."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjSubmenu = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oSubmenu  = new Submenu;
                
                $oSubmenu->iCodigo     = $mArrDados[$a]['Submenu_lng_Codigo'];
                $oSubmenu->sDescricao  = $mArrDados[$a]['Submenu_vch_Descricao'];
                $oSubmenu->sLink       = $mArrDados[$a]['Submenu_vch_Link'];
                $oSubmenu->sAtivo      = $mArrDados[$a]['Submenu_chr_Ativo'];
                $oSubmenu->iCodigoMenu = $mArrDados[$a]['Menu_lng_Codigo'];
                
                $arrObjSubmenu->append($oSubmenu);
            }
        }
        return $arrObjSubmenu;
    }
    public static final function pesquisaSubmenu( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Submenu_lng_Codigo,
                           Submenu_vch_Descricao,
                           Submenu_vch_Link,
                           Submenu_chr_Ativo,
                           Menu_lng_Codigo
                           FROM Submenu
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oSubmenu = new Submenu;
        
        if (is_array($mArrDados))
        { 
           $oSubmenu->iCodigo     = $mArrDados['Submenu_lng_Codigo'];
           $oSubmenu->sDescricao  = $mArrDados['Submenu_vch_Descricao'];
           $oSubmenu->sLink       = $mArrDados['Submenu_vch_Link'];
           $oSubmenu->sAtivo      = $mArrDados['Submenu_chr_Ativo'];
           $oSubmenu->iCodigoMenu = $mArrDados['Menu_lng_Codigo'];
        }
        
        return $oSubmenu;     
    }
    
    public static final function alteraSubmenu( $oSubmenu )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Submenu SET
                    Submenu_vch_Descricao    = :descricao,
                    Submenu_vch_Link         = :url,
                    Submenu_chr_Ativo        = :ativo
                    WHERE Submenu_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oSubmenu->iCodigo));
        $sSql->bindParam(':descricao', ($oSubmenu->sDescricao));
        $sSql->bindParam(':url',       ($oSubmenu->sLink));
        $sSql->bindParam(':ativo',     ($oSubmenu->sAtivo));
        
        $sSql->execute();
    }
    
    public static final function alteraOrdemSubmenu( $oSubmenu )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Submenu SET
                    Submenu_lng_Posicao      = :posicao
                    WHERE Submenu_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oSubmenu->iCodigo));
        $sSql->bindParam(':posicao',   ($oSubmenu->iPosicao));
        
        $sSql->execute();
    }
    
    public static final function salvaSubmenu( $oSubmenu )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Submenu (
                    Submenu_vch_Descricao,
                    Submenu_vch_Link,
                    Submenu_chr_Ativo,
                    Menu_lng_Codigo
                    ) VALUES ( ?,?,?,? )"); 
        
        $sSql->bindParam(1, ($oSubmenu->sDescricao));
        $sSql->bindParam(2, ($oSubmenu->sLink));
        $sSql->bindParam(3, ($oSubmenu->sAtivo));
        $sSql->bindParam(4, ($oSubmenu->iCodigoMenu));
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiSubmenu( $iCodigo  )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Submenu 
                    WHERE Submenu_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
 
    
 
}

?>