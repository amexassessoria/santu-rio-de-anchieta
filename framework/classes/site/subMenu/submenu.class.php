<?php

class Submenu
{
    
    /* Atributos */
    private $iCodigo;
    private $sDescricao;
    private $sLink;
    private $iPosicao;
    private $sAtivo;
    
    private $iCodigoMenu;
    
    /* M�todos m�gicos GET e SET */
    public function __get($property) 
    {
        if (property_exists($this, $property)) 
        {
          return $this->$property;
        }
    }
    
    public function __set($property, $value) 
    {
        if (property_exists($this, $property)) 
        {
          $this->$property = $value;
        }
        return $this;
    }

}

?>