<?php

class AlbumDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem     = 'Album_lng_Codigo';
    private static $iLimite = 0;
    private static $iInicio = 0;
    
    public static function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function setaLimite($iArgLimite, $iArgInicio = 0)
    {
        self::$iLimite = $iArgLimite;
        self::$iInicio = $iArgInicio;
    }
    
    public static function pesquisaAlbumLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
        
        /* Define o Limite */
        if (self::$iLimite > 0)
        {
            $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

            //Limpa Filtro
            self::$iInicio = 0;
            self::$iLimite = 0;
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Album.Album_lng_Codigo,
                           Album_vch_Titulo,
                           Album_chr_Visivel,
                           Album_Tipo_lng_Codigo,
                           Foto_vch_Url
                           FROM Album
                           LEFT JOIN Foto ON ( Album.Foto_lng_Codigo = Foto.Foto_lng_Codigo )
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                           ".$sLimite."
                         ");  
        
        //echo $mResultado->queryString;
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjAlbum = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oAlbum  = new Album;
                
                $oAlbum->iCodigo  = $mArrDados[$a]['Album_lng_Codigo'];
                $oAlbum->sTitulo  = $mArrDados[$a]['Album_vch_Titulo'];
                $oAlbum->sVisivel = $mArrDados[$a]['Album_chr_Visivel'];
                
                $oAlbum->sUrlCapa = $mArrDados[$a]['Foto_vch_Url'];
               
                if ( $oAlbum->sUrlCapa == null)
                {
                    FotoDB::setaFiltro(' AND Album_lng_Codigo = '.$mArrDados[$a]['Album_lng_Codigo']);
                    $oFoto = FotoDB::pesquisaFoto();
                    $oAlbum->sUrlCapa = $oFoto->sUrl;
                }
               
                $arrObjAlbum->append($oAlbum);
            }
        } 
        return $arrObjAlbum;
    }
    
    public static function pesquisaAlbum( $tipo = false )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Album_lng_Codigo,
                           Album_vch_Titulo,
                           Album_chr_Visivel,
                           Album_vch_Credito,
                           Album_Tipo_lng_Codigo
                           FROM Album
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oAlbum = new Album;
      
        if (is_array($mArrDados))
        {          
           $oAlbum->iCodigo  = $mArrDados['Album_lng_Codigo'];
           $oAlbum->sTitulo  = $mArrDados['Album_vch_Titulo'];
           $oAlbum->sVisivel = $mArrDados['Album_chr_Visivel'];
           $oAlbum->sCredito = $mArrDados['Album_vch_Credito'];
           
           if ($tipo == true)
           {
               AlbumTipoDB::setaFiltro(" AND Album_Tipo_lng_Codigo = ".$mArrDados['Album_Tipo_lng_Codigo']);
               $oAlbumTipo = AlbumTipoDB::pesquisaAlbumTipo();
               $oAlbum->oAlbumTipo = $oAlbumTipo;
           }
           
           FotoDB::setaFiltro(" AND Album_lng_Codigo = $oAlbum->iCodigo");
           $arrObjFoto = FotoDB::pesquisaFotoLista();
           
           $oAlbum->arrObjFoto = $arrObjFoto;
        }
        
        return $oAlbum;     
    }
    
    public static final function alteraAlbum( $oAlbum )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Album SET
                    Album_vch_Titulo       = :titulo,
                    Album_chr_Visivel      = :visivel,
                    Album_vch_Credito      = :credito,
                    Album_Tipo_lng_Codigo  = :album_tipo
                    WHERE Album_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',   ($oAlbum->iCodigo));
        $sSql->bindParam(':titulo',   ($oAlbum->sTitulo));
        $sSql->bindParam(':visivel',  ($oAlbum->sVisivel));
        $sSql->bindParam(':credito',  ($oAlbum->sCredito));
        $sSql->bindParam(':album_tipo',  ($oAlbum->oAlbumTipo->iCodigo));
        
        $sSql->execute();
    }
    
    public static final function salvaAlbum( $oAlbum )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Album (
                    Album_vch_Titulo,
                    Album_chr_Visivel,
                    Album_vch_Credito,
                    Album_Tipo_lng_Codigo
                    ) VALUES ( ?,?,?,? )"); 
        
        $sSql->bindParam(1, ($oAlbum->sTitulo));
        $sSql->bindParam(2, ($oAlbum->sVisivel));
        $sSql->bindParam(3, ($oAlbum->sCredito));
        $sSql->bindParam(4, ($oAlbum->oAlbumTipo->iCodigo));
        $sSql->execute();
        
        return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
        
    public static final function alteraCapa( $oAlbum )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Album SET
                    Foto_lng_Codigo      = :albumCodigo
                    WHERE Album_lng_Codigo = :fotoCodigo " );
        //var_dump($sSql);
        $sSql->bindParam(':albumCodigo', ($oAlbum->iCodigo));
        $sSql->bindParam(':fotoCodigo',  ($oAlbum->iFotoCodigo));
        
        $sSql->execute();
    }
    
    public static final function excluiAlbum( $iCodigo  )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Album 
                    WHERE Album_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
    
    public static final function contaAlbum( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sJoin    = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            //self::$mArrCampos['CONDICAO'] = '';
        }
        
        // Define os Joins
        if (isset(self::$mArrJoin['JOIN']))
        {
            if ( self::$mArrJoin['JOIN'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrJoin['JOIN']); $a < $iCount; ++$a)
                {
                    $sJoin .= (self::$mArrJoin['JOIN'][$a]);
                }
            }
            //Limpa Filtros
            //self::$mArrJoin['JOIN'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           count(*)
                           FROM Album
                           ".$sJoin."
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        //echo $mResultado->queryString;
        $mResultado->execute();
        
        $iTotal = $mResultado->fetchColumn();
        
        return $iTotal;     
    }
    
 
}

?>