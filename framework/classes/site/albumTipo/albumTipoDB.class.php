<?php

class AlbumTipoDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Album_Tipo_lng_Codigo';
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function pesquisaAlbumTipoLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Album_Tipo_lng_Codigo,
                           Album_Tipo_vch_Descricao
                           FROM Album_Tipo
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjAlbum_Tipo = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oAlbumTipo  = new AlbumTipo;
                
                $oAlbumTipo->iCodigo    = $mArrDados[$a]['Album_Tipo_lng_Codigo'];
                $oAlbumTipo->sDescricao = $mArrDados[$a]['Album_Tipo_vch_Descricao'];
                
                $arrObjAlbum_Tipo->append($oAlbumTipo);
            }
        }
        return $arrObjAlbum_Tipo;
    }
    public static final function pesquisaAlbumTipo( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Album_Tipo_lng_Codigo,
                           Album_Tipo_vch_Descricao
                           FROM Album_Tipo
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oAlbumTipo = new AlbumTipo;
        
        if (is_array($mArrDados))
        { 
           $oAlbumTipo->iCodigo    = $mArrDados['Album_Tipo_lng_Codigo'];
           $oAlbumTipo->sDescricao = $mArrDados['Album_Tipo_vch_Descricao'];
        }
        
        return $oAlbumTipo;     
    }
    
    public static final function alteraAlbumTipo( $oAlbumTipo )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Album_Tipo SET
                    Album_Tipo_vch_Descricao    = :descricao
                    WHERE Album_Tipo_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oAlbumTipo->iCodigo));
        $sSql->bindParam(':descricao', ($oAlbumTipo->sDescricao));
        
        $sSql->execute();
    }
    
    public static final function salvaAlbumTipo( $oAlbumTipo )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Album_Tipo (
                    Album_Tipo_vch_Descricao
                    ) VALUES ( ? )"); 
        
        $sSql->bindParam(1, ($oAlbumTipo->sDescricao));
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiAlbumTipo( $iCodigo  )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Album_Tipo 
                    WHERE Album_Tipo_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
 
    
 
}

?>