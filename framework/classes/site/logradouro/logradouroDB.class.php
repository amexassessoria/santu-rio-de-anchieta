<?php

class LogradouroDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Logradouro_lng_Codigo';
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function pesquisaLogradouroLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Logradouro_lng_Codigo,
                           Logradouro_vch_Descricao
                           FROM Logradouro
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                         ");  
        
        $mResultado->execute();
        //echo($mResultado->queryString);
                
        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjLogradouro = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oLogradouro  = new Logradouro;
                
                $oLogradouro->iCodigo    = $mArrDados[$a]['Logradouro_lng_Codigo'];
                $oLogradouro->sDescricao = $mArrDados[$a]['Logradouro_vch_Descricao'];
                
                $arrObjLogradouro->append($oLogradouro);
            }
        }
        return $arrObjLogradouro;
    }
    public static final function pesquisaLogradouro( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Logradouro_lng_Codigo,
                           Logradouro_vch_Descricao
                           FROM Logradouro
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oLogradouro = new Logradouro;
        
        if (is_array($mArrDados))
        { 
           $oLogradouro->iCodigo    = $mArrDados['Logradouro_lng_Codigo'];
           $oLogradouro->sDescricao = $mArrDados['Logradouro_vch_Descricao'];
        }
        
        return $oLogradouro;     
    }
    
    public static final function alteraLogradouro( $oLogradouro )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Logradouro SET
                    Logradouro_vch_Descricao    = :descricao
                    WHERE Logradouro_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oLogradouro->iCodigo));
        $sSql->bindParam(':descricao', ($oLogradouro->sDescricao));
        
        $sSql->execute();
    }
    
    public static final function salvaLogradouro( $oLogradouro )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Logradouro (
                    Logradouro_vch_Descricao
                    ) VALUES ( ? )"); 
        
        $sSql->bindParam(1, ($oLogradouro->sDescricao));
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiLogradouro( $iCodigo  )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Logradouro 
                    WHERE Logradouro_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
 
    
 
}

?>