<?php

class Menu
{
    
    /* Atributos */
    private $iCodigo;
    private $iCodigoPai;
    private $sDescricao;
    private $sLink;
    private $iPosicao;
    private $sAtivo;
    
    private $arrObjMenu;
    
    /* M�todos m�gicos GET e SET */
    public function __get($property) 
    {
        if (property_exists($this, $property)) 
        {
          return $this->$property;
        }
    }
    
    public function __set($property, $value) 
    {
        if (property_exists($this, $property)) 
        {
          $this->$property = $value;
        }
        return $this;
    }

}

?>