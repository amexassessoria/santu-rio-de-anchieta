<?php

class MenuDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Menu_lng_Posicao,Menu_lng_Codigo';
    private static $iLimite = 0;
    private static $iInicio = 0;
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function setaLimite($iArgLimite, $iArgInicio = 0)
    {
        self::$iLimite = $iArgLimite;
        self::$iInicio = $iArgInicio;
    }
    
    public static final function pesquisaMenuLista( $bSubMenu = false )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
        
        /* Define o Limite */
        if (self::$iLimite > 0)
        {
            $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

            //Limpa Filtro
            self::$iInicio = 0;
            self::$iLimite = 0;
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Menu_lng_Codigo,
                           Menu_vch_Descricao,
                           Menu_vch_Link,
                           Menu_chr_Ativo,
                           Menu_lng_CodigoPai
                           FROM Menu
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                           ".$sLimite."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjMenu = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oMenu  = new Menu;
                
                if ( $bSubMenu == true )
                {
                    /* if ( $mArrDados[$a]['Menu_lng_CodigoPai'] <> NULL )
                    { */
                        MenuDB::setaFiltro(" AND Menu_chr_Ativo = 'S' AND Menu_lng_CodigoPai = ".$mArrDados[$a]['Menu_lng_Codigo']);
                        $oMenu->arrObjMenu = MenuDB::pesquisaMenuLista( $bSubMenu );
                    /* } */
                }
                
                $oMenu->iCodigo    = $mArrDados[$a]['Menu_lng_Codigo'];
                $oMenu->iCodigoPai = $mArrDados[$a]['Menu_lng_CodigoPai'];
                $oMenu->sDescricao = $mArrDados[$a]['Menu_vch_Descricao'];
                $oMenu->sLink      = $mArrDados[$a]['Menu_vch_Link'];
                $oMenu->sAtivo     = $mArrDados[$a]['Menu_chr_Ativo'];
                
                $arrObjMenu->append($oMenu);
            }
        }
        return $arrObjMenu;
    }
    
    public static final function pesquisaMenu( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Menu_lng_Codigo,
                           Menu_vch_Descricao,
                           Menu_vch_Link,
                           Menu_chr_Ativo,
                           Menu_lng_CodigoPai
                           FROM Menu
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        //echo $mResultado->queryString;
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oMenu = new Menu;
        
        if (is_array($mArrDados))
        { 
           $oMenu->iCodigo    = $mArrDados['Menu_lng_Codigo'];
           $oMenu->sDescricao = $mArrDados['Menu_vch_Descricao'];
           $oMenu->sLink      = $mArrDados['Menu_vch_Link'];
           $oMenu->sAtivo     = $mArrDados['Menu_chr_Ativo'];
           $oMenu->iCodigoPai = $mArrDados['Menu_lng_CodigoPai'];
        }
        
        return $oMenu;     
    }
    
    public static final function alteraMenu( $oMenu )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Menu SET
                    Menu_vch_Descricao    = :descricao,
                    Menu_vch_Link         = :url,
                    Menu_chr_Ativo        = :ativo,
                    Menu_lng_CodigoPai    = :codigoPai
                    WHERE Menu_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oMenu->iCodigo));
        $sSql->bindParam(':descricao', ($oMenu->sDescricao));
        $sSql->bindParam(':url',       ($oMenu->sLink));
        $sSql->bindParam(':ativo',     ($oMenu->sAtivo));
        $sSql->bindParam(':codigoPai', ($oMenu->iCodigoPai));
        
        $sSql->execute();
    }
    
    public static final function alteraOrdemMenu( $oMenu )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Menu SET
                    Menu_lng_Posicao      = :posicao
                    WHERE Menu_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oMenu->iCodigo));
        $sSql->bindParam(':posicao',   ($oMenu->iPosicao));
        
        $sSql->execute();
    }
    
    public static final function salvaMenu( $oMenu )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Menu (
                    Menu_vch_Descricao,
                    Menu_vch_Link,
                    Menu_chr_Ativo,
                    Menu_lng_CodigoPai
                    ) VALUES ( ?,?,?,? )"); 
        
        $sSql->bindParam(1, ($oMenu->sDescricao));
        $sSql->bindParam(2, ($oMenu->sLink));
        $sSql->bindParam(3, ($oMenu->sAtivo));
        $sSql->bindParam(4, ($oMenu->iCodigoPai));
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiMenu( $iCodigo )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Menu 
                    WHERE Menu_lng_Codigo = :codigo");

        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();

    }
    
    public static final function verificaSubmenu( $iCodigoPai )
    {
        $oConexao = db::conectar();
             
        $mResultado = $oConexao->prepare("SELECT
                           Menu_lng_Codigo
                           FROM Menu
                           WHERE Menu_lng_CodigoPai = ".$iCodigoPai."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oMenu = new Menu;
        
        if (is_array($mArrDados))
        { 
           return true;
        }
        
        return false;     
    }
    
 
}

?>