<?php

class ArtigoTipoDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Tipo_lng_Codigo';
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function pesquisaArtigoTipoLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Tipo_lng_Codigo,
                           Tipo_vch_Descricao
                           FROM Art_tipo
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjArtigo = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oArtigoTipo  = new ArtigoTipo;
                
                $oArtigoTipo->iCodigo    = $mArrDados[$a]['Tipo_lng_Codigo'];
                $oArtigoTipo->sDescricao = $mArrDados[$a]['Tipo_vch_Descricao'];
                
                $arrObjArtigo->append($oArtigoTipo);
            }
        }
        return $arrObjArtigo;
    }
    public static final function pesquisaArtigoTipo( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Tipo_lng_Codigo,
                           Tipo_vch_Descricao,
                           Tipo_vch_DescricaoPlural
                           FROM Art_tipo
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        //echo $mResultado->queryString;
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oArtigoTipo = new ArtigoTIpo;
        
        if (is_array($mArrDados))
        { 
           $oArtigoTipo->iCodigo    = $mArrDados['Tipo_lng_Codigo'];
           $oArtigoTipo->sDescricao = $mArrDados['Tipo_vch_Descricao'];
           $oArtigoTipo->sDescricaoPlural = $mArrDados['Tipo_vch_DescricaoPlural'];
        }
       // var_dump($oArtigoTipo);
        return $oArtigoTipo;     
    }
    
 
}

?>