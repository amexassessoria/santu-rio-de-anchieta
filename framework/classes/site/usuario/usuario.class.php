<?php

class Usuario
{
    
    /* Atributos */
    private $iCodigo;
    private $sNome;
    private $sLogin;
    private $sSenha;
    private $sEmail;
    private $sAtivo;
    private $sDataCadastro;
    
    private $sAdicionarArtigo;
    private $sEditarArtigo;
    private $sExcluirArtigo;
    private $sCadastrarUsuario;
    private $sCadastrarFoto;
    private $sCadastrarTag;
    private $sCadastrarVideo;
    private $sCadastrarAudio;
    private $sCadastrarMenu;
    private $sCadastrarInformativo;
    private $sCadastrarCarrossel;
    private $sVisualizarRelatorios;
    private $sAdministrarEspiritualidade;
    private $sCadastrarConfiguracoes;
    
    
    /* M�todos m�gicos GET e SET */
    public function __get($property) 
    {
        if (property_exists($this, $property)) 
        {
          return $this->$property;
        }
    }
    
    public function __set($property, $value) 
    {
        if (property_exists($this, $property)) 
        {
          $this->$property = $value;
        }
        return $this;
    }

}

?>