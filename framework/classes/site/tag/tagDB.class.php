<?php

class TagDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Tag_lng_Codigo';
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function pesquisaTagLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Tag_lng_Codigo,
                           Tag_vch_Descricao
                           FROM Tag
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjTag = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oTag  = new Tag;
                
                $oTag->iCodigo    = $mArrDados[$a]['Tag_lng_Codigo'];
                $oTag->sDescricao = $mArrDados[$a]['Tag_vch_Descricao'];
                
                $arrObjTag->append($oTag);
            }
        }
        return $arrObjTag;
    }
    public static final function pesquisaTag( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Tag_lng_Codigo,
                           Tag_vch_Descricao
                           FROM Tag
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oTag = new Tag;
        
        if (is_array($mArrDados))
        { 
           $oTag->iCodigo    = $mArrDados['Tag_lng_Codigo'];
           $oTag->sDescricao = $mArrDados['Tag_vch_Descricao'];
        }
        
        return $oTag;     
    }
    
    public static final function alteraTag( $oTag )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Tag SET
                    Tag_vch_Descricao    = :descricao
                    WHERE Tag_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oTag->iCodigo));
        $sSql->bindParam(':descricao', ($oTag->sDescricao));
        
        $sSql->execute();
    }
    
    public static final function salvaTag( $oTag )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Tag (
                    Tag_vch_Descricao
                    ) VALUES ( ? )"); 
        
        $sSql->bindParam(1, ($oTag->sDescricao));
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiTag( $iCodigo  )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Tag 
                    WHERE Tag_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
 
    
 
}

?>