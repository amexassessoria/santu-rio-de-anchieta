<?php

class AudioTipoDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Audio_Tipo_lng_Codigo';
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function pesquisaAudioTipoLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Audio_Tipo_lng_Codigo,
                           Audio_Tipo_vch_Descricao
                           FROM Audio_Tipo
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjAudio_Tipo = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oAudioTipo  = new AudioTipo;
                
                $oAudioTipo->iCodigo    = $mArrDados[$a]['Audio_Tipo_lng_Codigo'];
                $oAudioTipo->sDescricao = $mArrDados[$a]['Audio_Tipo_vch_Descricao'];
                
                $arrObjAudio_Tipo->append($oAudioTipo);
            }
        }
        return $arrObjAudio_Tipo;
    }
    public static final function pesquisaAudioTipo( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Audio_Tipo_lng_Codigo,
                           Audio_Tipo_vch_Descricao
                           FROM Audio_Tipo
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oAudioTipo = new AudioTipo;
        
        if (is_array($mArrDados))
        { 
           $oAudioTipo->iCodigo    = $mArrDados['Audio_Tipo_lng_Codigo'];
           $oAudioTipo->sDescricao = $mArrDados['Audio_Tipo_vch_Descricao'];
        }
        
        return $oAudioTipo;     
    }
    
    public static final function alteraAudioTipo( $oAudioTipo )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Audio_Tipo SET
                    Audio_Tipo_vch_Descricao    = :descricao
                    WHERE Audio_Tipo_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oAudioTipo->iCodigo));
        $sSql->bindParam(':descricao', ($oAudioTipo->sDescricao));
        
        $sSql->execute();
    }
    
    public static final function salvaAudioTipo( $oAudioTipo )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Audio_Tipo (
                    Audio_Tipo_vch_Descricao
                    ) VALUES ( ? )"); 
        
        $sSql->bindParam(1, ($oAudioTipo->sDescricao));
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiAudioTipo( $iCodigo  )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Audio_Tipo 
                    WHERE Audio_Tipo_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
 
    
 
}

?>