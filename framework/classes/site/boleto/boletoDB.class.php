<?php

class BoletoDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Banco_lng_Id';
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function pesquisaBoletoLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Banco_lng_Id,
                           Boleto_lng_Codigo,
                           Boleto_chr_Convenio,
                           Boleto_chr_Carteira,
                           Boleto_vch_Codigo_Cedente,
                           Boleto_vch_Nome_Cedente,
                           Boleto_chr_CNPJ_Cedente,
                           Boleto_vch_Endereco_Cedente,
                           Boleto_vch_Bairro_Cedente,
                           Boleto_chr_CEP_Cedente,
                           Boleto_chr_Numero_Cedente,
                           Boleto_vch_Cidade_Cedente,
                           Boleto_chr_Estado_Cedente,
                           Boleto_chr_Agencia_Numero,
                           Boleto_chr_Agencia_Digito,
                           Boleto_chr_Conta_Numero,
                           Boleto_chr_Conta_Digito,
                           Boleto_chr_Operacao,
                           Boleto_vch_Logo_Grande,
                           Boleto_vch_Logo_Pequeno,
                           Boleto_vch_Local_Pagamento1,
                           Boleto_vch_Local_Pagamento2,
                           Boleto_vch_Instrucao1,
                           Boleto_vch_Instrucao2,
                           Boleto_vch_Instrucao3,
                           Boleto_vch_Instrucao4,
                           Boleto_vch_Instrucao5,
                           Boleto_vch_Instrucao6,
                           Boleto_vch_Instrucao7,
                           Boleto_vch_Instrucao8,
                           Boleto_vch_Instrucao9,
                           Boleto_chr_Ativo,
                           Boleto_dat_Data_Cadastro,
                           Boleto_dat_Data_Modificacao,
                           Boleto_lng_Sequencial
                           FROM Boleto
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjBoleto = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oBoleto  = new Boleto;
                
                BancoDB::setaFiltro(' AND Banco_lng_Id = '.$mArrDados[$a]['Banco_lng_Id']);
                $oBanco = BancoDB::pesquisaBanco();
                
                $oBoleto->oBanco           = $oBanco;
                
                $oBoleto->iIdBanco         = $mArrDados[$a]['Banco_lng_Id'];
                $oBoleto->iCodigo          = $mArrDados[$a]['Boleto_lng_Codigo'];
                $oBoleto->sConvenio        = $mArrDados[$a]['Boleto_chr_Convenio'];
                $oBoleto->sCarteira        = $mArrDados[$a]['Boleto_chr_Carteira'];
                $oBoleto->sCodigoCedente   = $mArrDados[$a]['Boleto_vch_Codigo_Cedente'];
                $oBoleto->sNomeCedente     = $mArrDados[$a]['Boleto_vch_Nome_Cedente'];
                $oBoleto->sCNPJCedente     = $mArrDados[$a]['Boleto_chr_CNPJ_Cedente'];
                $oBoleto->sEnderecoCedente = $mArrDados[$a]['Boleto_vch_Endereco_Cedente'];
                $oBoleto->sBairroCedente   = $mArrDados[$a]['Boleto_vch_Bairro_Cedente'];
                $oBoleto->sCEPCedente      = $mArrDados[$a]['Boleto_chr_CEP_Cedente'];
                $oBoleto->sNumeroCedente   = $mArrDados[$a]['Boleto_chr_Numero_Cedente'];
                $oBoleto->sCidadeCedente   = $mArrDados[$a]['Boleto_vch_Cidade_Cedente'];
                $oBoleto->sEstadoCedente   = $mArrDados[$a]['Boleto_chr_Estado_Cedente'];
                $oBoleto->sAgenciaNumero   = $mArrDados[$a]['Boleto_chr_Agencia_Numero'];
                $oBoleto->sAgenciaDigito   = $mArrDados[$a]['Boleto_chr_Agencia_Digito'];
                $oBoleto->sContaNumero     = $mArrDados[$a]['Boleto_chr_Conta_Numero'];
                $oBoleto->sContaDigito     = $mArrDados[$a]['Boleto_chr_Conta_Digito'];
                $oBoleto->sOperacao        = $mArrDados[$a]['Boleto_chr_Operacao'];
                $oBoleto->sLogoGrande      = $mArrDados[$a]['Boleto_vch_Logo_Grande'];
                $oBoleto->sLogoPequeno     = $mArrDados[$a]['Boleto_vch_Logo_Pequeno'];
                $oBoleto->sLocalPagamento1 = $mArrDados[$a]['Boleto_vch_Local_Pagamento1'];
                $oBoleto->sLocalPagamento2 = $mArrDados[$a]['Boleto_vch_Local_Pagamento2'];
                $oBoleto->sInstrucao1      = $mArrDados[$a]['Boleto_vch_Instrucao1'];
                $oBoleto->sInstrucao2      = $mArrDados[$a]['Boleto_vch_Instrucao2'];
                $oBoleto->sInstrucao3      = $mArrDados[$a]['Boleto_vch_Instrucao3'];
                $oBoleto->sInstrucao4      = $mArrDados[$a]['Boleto_vch_Instrucao4'];
                $oBoleto->sInstrucao5      = $mArrDados[$a]['Boleto_vch_Instrucao5'];
                $oBoleto->sInstrucao6      = $mArrDados[$a]['Boleto_vch_Instrucao6'];
                $oBoleto->sInstrucao7      = $mArrDados[$a]['Boleto_vch_Instrucao7'];
                $oBoleto->sInstrucao8      = $mArrDados[$a]['Boleto_vch_Instrucao8'];
                $oBoleto->sInstrucao9      = $mArrDados[$a]['Boleto_vch_Instrucao9'];
                $oBoleto->sAtivo           = $mArrDados[$a]['Boleto_chr_Ativo'];
                $oBoleto->sDataCadastro    = $mArrDados[$a]['Boleto_dat_Data_Cadastro'];
                $oBoleto->sDataModificacao = $mArrDados[$a]['Boleto_dat_Data_Modificacao'];
                $oBoleto->iSequencial      = $mArrDados[$a]['Boleto_lng_Sequencial'];
                
                $arrObjBoleto->append($oBoleto);
            }
        }
        return $arrObjBoleto;
    }
    public static final function pesquisaBoleto( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Boleto_lng_Codigo,
                           Boleto.Banco_lng_Id,
                           Banco_vch_Codigo,
                           Boleto_chr_Convenio,
                           Boleto_chr_Carteira,
                           Boleto_vch_Codigo_Cedente,
                           Boleto_vch_Nome_Cedente,
                           Boleto_chr_CNPJ_Cedente,
                           Boleto_vch_Endereco_Cedente,
                           Boleto_vch_Bairro_Cedente,
                           Boleto_chr_CEP_Cedente,
                           Boleto_chr_Numero_Cedente,
                           Boleto_vch_Cidade_Cedente,
                           Boleto_chr_Estado_Cedente,
                           Boleto_chr_Agencia_Numero,
                           Boleto_chr_Agencia_Digito,
                           Boleto_chr_Conta_Numero,
                           Boleto_chr_Conta_Digito,
                           Boleto_chr_Operacao,
                           Boleto_vch_Logo_Grande,
                           Boleto_vch_Logo_Pequeno,
                           Boleto_vch_Local_Pagamento1,
                           Boleto_vch_Local_Pagamento2,
                           Boleto_vch_Instrucao1,
                           Boleto_vch_Instrucao2,
                           Boleto_vch_Instrucao3,
                           Boleto_vch_Instrucao4,
                           Boleto_vch_Instrucao5,
                           Boleto_vch_Instrucao6,
                           Boleto_vch_Instrucao7,
                           Boleto_vch_Instrucao8,
                           Boleto_vch_Instrucao9,
                           Boleto_chr_Ativo,
                           Boleto_dat_Data_Cadastro,
                           Boleto_dat_Data_Modificacao,
                           Boleto_lng_Sequencial
                           FROM Boleto
                           INNER JOIN Banco on ( Boleto.Banco_lng_Id = Banco.Banco_lng_Id )
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        //echo($mResultado->queryString);
         /* Instancia o Objeto */
        $oBoleto = new Boleto;
        
        if (is_array($mArrDados))
        { 
           $oBoleto->iIdBanco         = $mArrDados['Banco_lng_Id'];
           $oBoleto->sCodigoBanco     = $mArrDados['Banco_vch_Codigo'];
           
           $oBoleto->iCodigo          = $mArrDados['Boleto_lng_Codigo'];
           $oBoleto->sConvenio        = $mArrDados['Boleto_chr_Convenio'];
           $oBoleto->sCarteira        = $mArrDados['Boleto_chr_Carteira'];
           $oBoleto->sCodigoCedente   = $mArrDados['Boleto_vch_Codigo_Cedente'];
           $oBoleto->sNomeCedente     = $mArrDados['Boleto_vch_Nome_Cedente'];
           $oBoleto->sCNPJCedente     = $mArrDados['Boleto_chr_CNPJ_Cedente'];
           $oBoleto->sEnderecoCedente = $mArrDados['Boleto_vch_Endereco_Cedente'];
           $oBoleto->sBairroCedente   = $mArrDados['Boleto_vch_Bairro_Cedente'];
           $oBoleto->sCEPCedente      = $mArrDados['Boleto_chr_CEP_Cedente'];
           $oBoleto->sNumeroCedente   = $mArrDados['Boleto_chr_Numero_Cedente'];
           $oBoleto->sCidadeCedente   = $mArrDados['Boleto_vch_Cidade_Cedente'];
           $oBoleto->sEstadoCedente   = $mArrDados['Boleto_chr_Estado_Cedente'];
           $oBoleto->sAgenciaNumero   = $mArrDados['Boleto_chr_Agencia_Numero'];
           $oBoleto->sAgenciaDigito   = $mArrDados['Boleto_chr_Agencia_Digito'];
           $oBoleto->sContaNumero     = $mArrDados['Boleto_chr_Conta_Numero'];
           $oBoleto->sContaDigito     = $mArrDados['Boleto_chr_Conta_Digito'];
           $oBoleto->sOperacao        = $mArrDados['Boleto_chr_Operacao'];
           $oBoleto->sLogoGrande      = $mArrDados['Boleto_vch_Logo_Grande'];
           $oBoleto->sLogoPequeno     = $mArrDados['Boleto_vch_Logo_Pequeno'];
           $oBoleto->sLocalPagamento1 = $mArrDados['Boleto_vch_Local_Pagamento1'];
           $oBoleto->sLocalPagamento2 = $mArrDados['Boleto_vch_Local_Pagamento2'];
           $oBoleto->sInstrucao1      = $mArrDados['Boleto_vch_Instrucao1'];
           $oBoleto->sInstrucao2      = $mArrDados['Boleto_vch_Instrucao2'];
           $oBoleto->sInstrucao3      = $mArrDados['Boleto_vch_Instrucao3'];
           $oBoleto->sInstrucao4      = $mArrDados['Boleto_vch_Instrucao4'];
           $oBoleto->sInstrucao5      = $mArrDados['Boleto_vch_Instrucao5'];
           $oBoleto->sInstrucao6      = $mArrDados['Boleto_vch_Instrucao6'];
           $oBoleto->sInstrucao7      = $mArrDados['Boleto_vch_Instrucao7'];
           $oBoleto->sInstrucao8      = $mArrDados['Boleto_vch_Instrucao8'];
           $oBoleto->sInstrucao9      = $mArrDados['Boleto_vch_Instrucao9'];
           $oBoleto->sAtivo           = $mArrDados['Boleto_chr_Ativo'];
           $oBoleto->sDataCadastro    = $mArrDados['Boleto_dat_Data_Cadastro'];
           $oBoleto->sDataModificacao = $mArrDados['Boleto_dat_Data_Modificacao'];
           $oBoleto->iSequencial      = $mArrDados['Boleto_lng_Sequencial'];
        }
        //var_dump($oBoleto);
        return $oBoleto;     
    }
    
    public static final function alteraBoleto( $oBoleto )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Boleto SET
                    Banco_lng_Id                = :idBanco,
                    Boleto_chr_Convenio         = :convenio,
                    Boleto_chr_Carteira         = :carteira,
                    Boleto_vch_Codigo_Cedente   = :codigoCedente,
                    Boleto_vch_Nome_Cedente     = :nomeCedente,
                    Boleto_chr_CNPJ_Cedente     = :CNPJCedente,
                    Boleto_vch_Endereco_Cedente = :enderecoCedente,
                    Boleto_vch_Bairro_Cedente   = :bairroCedente,
                    Boleto_chr_CEP_Cedente      = :CEPCedente,
                    Boleto_chr_Numero_Cedente   = :numeroCedente,
                    Boleto_vch_Cidade_Cedente   = :cidadeCedente,
                    Boleto_chr_Estado_Cedente   = :estadoCedente,
                    Boleto_chr_Agencia_Numero   = :agenciaNumero,
                    Boleto_chr_Agencia_Digito   = :agenciaDigito,
                    Boleto_chr_Conta_Numero     = :contaNumero,
                    Boleto_chr_Conta_Digito     = :contaDigito,
                    Boleto_chr_Operacao         = :operacao,
                    Boleto_vch_Logo_Grande      = :logoGrande,
                    Boleto_vch_Logo_Pequeno     = :logoPequeno,
                    Boleto_vch_Local_Pagamento1 = :pagamento1,
                    Boleto_vch_Local_Pagamento2 = :pagamento2,
                    Boleto_vch_Instrucao1       = :intrucao1,
                    Boleto_vch_Instrucao2       = :intrucao2,
                    Boleto_vch_Instrucao3       = :intrucao3,
                    Boleto_vch_Instrucao4       = :intrucao4,
                    Boleto_vch_Instrucao5       = :intrucao5,
                    Boleto_vch_Instrucao6       = :intrucao6,
                    Boleto_vch_Instrucao7       = :intrucao7,
                    Boleto_vch_Instrucao8       = :intrucao8,
                    Boleto_vch_Instrucao9       = :intrucao9,
                    Boleto_chr_Ativo            = :ativo,
                    Boleto_dat_Data_Modificacao = :dataModificacao
                    WHERE Boleto_lng_Codigo     = :codigo " );
 
        $sSql->bindParam(':convenio',       ($oBoleto->sConvenio));
        $sSql->bindParam(':carteira',       ($oBoleto->sCarteira));
        $sSql->bindParam(':codigoCedente',  ($oBoleto->sCodigoCedente));
        $sSql->bindParam(':nomeCedente',    ($oBoleto->sNomeCedente));
        $sSql->bindParam(':CNPJCedente',    ($oBoleto->sCNPJCedente));
        $sSql->bindParam(':enderecoCedente',($oBoleto->sEnderecoCedente));
        $sSql->bindParam(':bairroCedente',  ($oBoleto->sBairroCedente));
        $sSql->bindParam(':CEPCedente',     ($oBoleto->sCEPCedente));
        $sSql->bindParam(':numeroCedente',  ($oBoleto->sNumeroCedente));
        $sSql->bindParam(':cidadeCedente',  ($oBoleto->sCidadeCedente));
        $sSql->bindParam(':estadoCedente',  ($oBoleto->sEstadoCedente));
        $sSql->bindParam(':agenciaNumero',  ($oBoleto->sAgenciaNumero));
        $sSql->bindParam(':agenciaDigito',  ($oBoleto->sAgenciaDigito));
        $sSql->bindParam(':contaNumero',    ($oBoleto->sContaNumero));
        $sSql->bindParam(':contaDigito',    ($oBoleto->sContaDigito));
        $sSql->bindParam(':operacao',       ($oBoleto->sOperacao));
        $sSql->bindParam(':logoGrande',     ($oBoleto->sLogoGrande));
        $sSql->bindParam(':logoPequeno',    ($oBoleto->sLogoPequeno));
        $sSql->bindParam(':pagamento1',     ($oBoleto->sLocalPagamento1));
        $sSql->bindParam(':pagamento2',     ($oBoleto->sLocalPagamento2));
        $sSql->bindParam(':intrucao1',      ($oBoleto->sInstrucao1));
        $sSql->bindParam(':intrucao2',      ($oBoleto->sInstrucao2));
        $sSql->bindParam(':intrucao3',      ($oBoleto->sInstrucao3));
        $sSql->bindParam(':intrucao4',      ($oBoleto->sInstrucao4));
        $sSql->bindParam(':intrucao5',      ($oBoleto->sInstrucao5));
        $sSql->bindParam(':intrucao6',      ($oBoleto->sInstrucao6));
        $sSql->bindParam(':intrucao7',      ($oBoleto->sInstrucao7));
        $sSql->bindParam(':intrucao8',      ($oBoleto->sInstrucao8));
        $sSql->bindParam(':intrucao9',      ($oBoleto->sInstrucao9));
        $sSql->bindParam(':ativo',          ($oBoleto->sAtivo));
        $sSql->bindParam(':dataModificacao',($oBoleto->sDataModificacao));
        $sSql->bindParam(':idBanco',        ($oBoleto->iIdBanco));
        $sSql->bindParam(':codigo',         ($oBoleto->iCodigo));
        
        $sSql->execute();
        //echo ($sSql->queryString);
    }
    
    public static final function salvaBoleto( $oBoleto )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Boleto (
                           Banco_lng_Id,
                           Boleto_chr_Convenio,
                           Boleto_chr_Carteira,
                           Boleto_vch_Codigo_Cedente,
                           Boleto_vch_Nome_Cedente,
                           Boleto_chr_CNPJ_Cedente,
                           Boleto_vch_Endereco_Cedente,
                           Boleto_vch_Bairro_Cedente,
                           Boleto_chr_CEP_Cedente,
                           Boleto_chr_Numero_Cedente,
                           Boleto_vch_Cidade_Cedente,
                           Boleto_chr_Estado_Cedente,
                           Boleto_chr_Agencia_Numero,
                           Boleto_chr_Agencia_Digito,
                           Boleto_chr_Conta_Numero,
                           Boleto_chr_Conta_Digito,
                           Boleto_chr_Operacao,
                           Boleto_vch_Local_Pagamento1,
                           Boleto_vch_Local_Pagamento2,
                           Boleto_vch_Instrucao1,
                           Boleto_vch_Instrucao2,
                           Boleto_vch_Instrucao3,
                           Boleto_vch_Instrucao4,
                           Boleto_vch_Instrucao5,
                           Boleto_vch_Instrucao6,
                           Boleto_vch_Instrucao7,
                           Boleto_vch_Instrucao8,
                           Boleto_vch_Instrucao9,
                           Boleto_dat_Data_Cadastro,
                           Boleto_chr_Ativo
                    ) VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,? )"); 
        
        $sSql->bindParam(1,  ($oBoleto->iIdBanco));
        $sSql->bindParam(2,  ($oBoleto->sConvenio));
        $sSql->bindParam(3,  ($oBoleto->sCarteira));
        $sSql->bindParam(4,  ($oBoleto->sCodigoCedente));
        $sSql->bindParam(5,  ($oBoleto->sNomeCedente));
        $sSql->bindParam(6,  ($oBoleto->sAgenciaNumero));
        $sSql->bindParam(7,  ($oBoleto->sAgenciaDigito));
        $sSql->bindParam(8,  ($oBoleto->sContaNumero));
        $sSql->bindParam(9,  ($oBoleto->sContaDigito));
        $sSql->bindParam(10, ($oBoleto->sOperacao));
        $sSql->bindParam(11, ($oBoleto->sLocalPagamento1));
        $sSql->bindParam(12, ($oBoleto->sLocalPagamento2));
        $sSql->bindParam(13, ($oBoleto->sInstrucao1));
        $sSql->bindParam(14, ($oBoleto->sInstrucao2));
        $sSql->bindParam(15, ($oBoleto->sInstrucao3));
        $sSql->bindParam(16, ($oBoleto->sInstrucao4));
        $sSql->bindParam(17, ($oBoleto->sInstrucao5));
        $sSql->bindParam(18, ($oBoleto->sInstrucao6));
        $sSql->bindParam(19, ($oBoleto->sInstrucao7));
        $sSql->bindParam(20, ($oBoleto->sInstrucao8));
        $sSql->bindParam(21, ($oBoleto->sInstrucao9));
        $sSql->bindParam(22, ($oBoleto->sDataCadastro));
        $sSql->bindParam(23, ($oBoleto->sCNPJCedente));
        $sSql->bindParam(24, ($oBoleto->sEnderecoCedente));
        $sSql->bindParam(25, ($oBoleto->sBairroCedente));
        $sSql->bindParam(26, ($oBoleto->sCEPCedente));
        $sSql->bindParam(27, ($oBoleto->sNumeroCedente));
        $sSql->bindParam(28, ($oBoleto->sCidadeCedente));
        $sSql->bindParam(29, ($oBoleto->sEstadoCedente));
        $sSql->bindParam(30, ($oBoleto->sAtivo));
        
        $sSql->execute();
        //echo ($sSql->queryString);
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
        //var_dump($oBoleto);
    }
    
    public static final function excluiBoleto( $iIdBanco  )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Boleto 
                    WHERE Boleto_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iIdBanco, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
    public static final function desativaConvenios( )
    {
        $oConexao = db::conectar();
   
        $sSql=$oConexao->prepare("UPDATE Boleto SET
                    Boleto_chr_Ativo = 'N'");
        $sSql->execute();
    }
    
    public static final function incrementaSequencial( $iSequencial, $iCodigo )
    {
        $oConexao = db::conectar();
   
        $sSql=$oConexao->prepare("UPDATE Boleto SET
                    Boleto_lng_Sequencial = ".$iSequencial." WHERE Boleto_lng_Codigo = ".$iCodigo);
        $sSql->execute();
    }
    
 
}

?>