<?php

class Boleto
{
    
    /* Atributos */
    
    private $oBanco;
    
    private $iIdBanco;
    private $sCodigoBanco;
    
    private $iCodigo;
    private $iSequencial;
    private $sConvenio;
    private $sCarteira;
    private $sCodigoCedente;
    private $sNomeCedente;
    private $sCNPJCedente;
    private $sEnderecoCedente;
    private $sBairroCedente;
    private $sCEPCedente;
    private $sNumeroCedente;
    private $sCidadeCedente;
    private $sEstadoCedente;
    private $sAgenciaNumero;
    private $sAgenciaDigito;
    private $sContaNumero;
    private $sContaDigito;
    private $sOperacao;
    private $sLogoGrande;
    private $sLogoPequeno;
    private $sLocalPagamento1;
    private $sLocalPagamento2;
    private $sInstrucao1;
    private $sInstrucao2;
    private $sInstrucao3;
    private $sInstrucao4;
    private $sInstrucao5;
    private $sInstrucao6;
    private $sInstrucao7;
    private $sInstrucao8;
    private $sInstrucao9;
    private $sAtivo;
    private $sDataCadastro;
    private $sDataModificacao;
    
    /* M�todos m�gicos GET e SET */
    public function __get($property) 
    {
        if (property_exists($this, $property)) 
        {
          return $this->$property;
        }
    }
    
    public function __set($property, $value) 
    {
        if (property_exists($this, $property)) 
        {
          $this->$property = $value;
        }
        return $this;
    }

}

?>