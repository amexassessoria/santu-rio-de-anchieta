<?php

class VideoDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Video_lng_Codigo';
    private static $iLimite = 0;
    private static $iInicio = 0;
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function setaLimite($iArgLimite, $iArgInicio = 0)
    {
        self::$iLimite = $iArgLimite;
        self::$iInicio = $iArgInicio;
    }
    
    public static final function pesquisaVideoLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
        
        /* Define o Limite */
        if (self::$iLimite > 0)
        {
            $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

            //Limpa Filtro
            self::$iInicio = 0;
            self::$iLimite = 0;
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Video_lng_Codigo,
                           Video_vch_Descricao,
                           Video_vch_Link
                           FROM Video
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                           ".$sLimite."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjVideo = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oVideo  = new Video;
                
                $sCodigoIdentificacao = '';
                
                if (preg_match('![?&]{1}v=([^&]+)!', $mArrDados[$a]['Video_vch_Link'] . '&', $m))
                {
                    $sCodigoIdentificacao = $m[1];
                }
                //parse_str( parse_url( $mArrDados[$a]['Video_vch_Link'], PHP_URL_QUERY ), $sCodigoIdentificacao );
                
                $oVideo->iCodigo    = $mArrDados[$a]['Video_lng_Codigo'];
                $oVideo->sDescricao = $mArrDados[$a]['Video_vch_Descricao'];
                $oVideo->sLink      = $mArrDados[$a]['Video_vch_Link'];
                $oVideo->sCodigoIdentificacao  = $sCodigoIdentificacao;
                
                $arrObjVideo->append($oVideo);
            }
        }
        return $arrObjVideo;
    }
    public static final function pesquisaVideo( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Video_lng_Codigo,
                           Video_vch_Descricao,
                           Video_vch_Link
                           FROM Video
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oVideo = new Video;
        
        if (is_array($mArrDados))
        { 
           $oVideo->iCodigo    = $mArrDados['Video_lng_Codigo'];
           $oVideo->sDescricao = $mArrDados['Video_vch_Descricao'];
           $oVideo->sLink      = $mArrDados['Video_vch_Link'];
        }
        
        return $oVideo;     
    }
    
    public static final function alteraVideo( $oVideo )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Video SET
                    Video_vch_Descricao    = :descricao,
                    Video_vch_Link         = :link
                    WHERE Video_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oVideo->iCodigo));
        $sSql->bindParam(':descricao', ($oVideo->sDescricao));
        $sSql->bindParam(':link',      ($oVideo->sLink));
        
        $sSql->execute();
    }
    
    public static final function salvaVideo( $oVideo )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Video (
                    Video_vch_Descricao,
                    Video_vch_Link
                    ) VALUES ( ?,? )"); 
        
        $sSql->bindParam(1, ($oVideo->sDescricao));
        $sSql->bindParam(2, ($oVideo->sLink));
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiVideo( $iCodigo  )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Video 
                    WHERE Video_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
 
    public static final function contaVideo( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sJoin    = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            //self::$mArrCampos['CONDICAO'] = '';
        }
        
        // Define os Joins
        if (isset(self::$mArrJoin['JOIN']))
        {
            if ( self::$mArrJoin['JOIN'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrJoin['JOIN']); $a < $iCount; ++$a)
                {
                    $sJoin .= (self::$mArrJoin['JOIN'][$a]);
                }
            }
            //Limpa Filtros
            //self::$mArrJoin['JOIN'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           count(*)
                           FROM Video
                           ".$sJoin."
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        //echo $mResultado->queryString;
        $mResultado->execute();
        
        $iTotal = $mResultado->fetchColumn();
        
        return $iTotal;     
    }
 
}

?>