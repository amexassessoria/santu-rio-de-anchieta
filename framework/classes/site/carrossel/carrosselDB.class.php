<?php

class CarrosselDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Carrossel_lng_Codigo';
    private static $iLimite = 0;
    private static $iLimiteArtigo = 0;
    private static $iInicio = 0;
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function setaLimite($iArgLimite, $iArgInicio = 0)
    {
        self::$iLimite = $iArgLimite;
        self::$iInicio = $iArgInicio;
    }
    
    public static final function setaLimiteArtigo($iArgLimiteArtigo)
    {
        self::$iLimiteArtigo = $iArgLimiteArtigo;
    }
    
    public static final function pesquisaCarrosselListaCentralDeControle( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
        
        /* Define o Limite */
        if (self::$iLimite > 0)
        {
            $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

            //Limpa Filtro
            self::$iInicio = 0;
            self::$iLimite = 0;
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Carrossel_lng_Codigo,
                           Carrossel_vch_Descricao,
                           Carrossel_vch_ImagemUrl,
                           Carrossel_vch_Link,
                           Carrossel_dat_Entrada,
                           Carrossel_dat_Expiracao,
                           Carrossel_bit_LinkExterno
                           FROM Carrossel
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                           ".$sLimite."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjCarrossel = new ArrayObject();

        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oCarrossel  = new Carrossel;
                
                $oCarrossel->iCodigo      = $mArrDados[$a]['Carrossel_lng_Codigo'];
                $oCarrossel->sDescricao   = $mArrDados[$a]['Carrossel_vch_Descricao'];
                $oCarrossel->sImagemUrl   = 'images/banner/'.$mArrDados[$a]['Carrossel_vch_ImagemUrl'];
                $oCarrossel->sLink        = $mArrDados[$a]['Carrossel_vch_Link'];
                $oCarrossel->sEntrada     = $mArrDados[$a]['Carrossel_dat_Entrada'];
                $oCarrossel->sExpiracao   = $mArrDados[$a]['Carrossel_dat_Expiracao'];
                $oCarrossel->bLinkExterno = $mArrDados[$a]['Carrossel_bit_LinkExterno'];
                
                $arrObjCarrossel->append($oCarrossel);
            }
        }
        return $arrObjCarrossel;
    }
    
    public static final function pesquisaCarrosselLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
        
        /* Define o Limite */
        if (self::$iLimite > 0)
        {
            $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

            //Limpa Filtro
            self::$iInicio = 0;
            self::$iLimite = 0;
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Carrossel_lng_Codigo,
                           Carrossel_vch_Descricao,
                           Carrossel_vch_ImagemUrl,
                           Carrossel_vch_Link,
                           Carrossel_dat_Entrada,
                           Carrossel_dat_Expiracao,
                           Carrossel_bit_LinkExterno
                           FROM Carrossel
                           WHERE (now() BETWEEN Carrossel_dat_Entrada AND Carrossel_dat_Expiracao)              
                           ORDER BY Carrossel_lng_Codigo DESC
                           ".$sLimite."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjCarrossel = new ArrayObject();

        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oCarrossel  = new Carrossel;

                $oCarrossel->iCodigo      = $mArrDados[$a]['Carrossel_lng_Codigo'];
                $oCarrossel->sTitulo   = $mArrDados[$a]['Carrossel_vch_Descricao'];
                $oCarrossel->sImagemUrl   = 'images/banner/'.$mArrDados[$a]['Carrossel_vch_ImagemUrl'];
                $oCarrossel->sLink        = $mArrDados[$a]['Carrossel_vch_Link'];
                $oCarrossel->bLinkExterno = $mArrDados[$a]['Carrossel_bit_LinkExterno'];
                
                $arrObjCarrossel->append($oCarrossel);
            }
        }

        $iTotalCarrossel = Config::QUANTIDADE_NOTICIAS_CARROSSEL - (count($mArrDados));

        if( $iTotalCarrossel > 0 )
        {
            ArtigoDB::setaLimite($iTotalCarrossel);

            ArtigoDB::setaFiltro(" AND Artigo_chr_Carrossel = 'S'");
            $mArrDadosArtigo = ArtigoDB::pesquisaArtigoLista();
            
            
            foreach ( $mArrDadosArtigo as $oArtigo )
            {
                /* Instancia o Objeto */
                $oCarrossel  = new Carrossel;

                $oCarrossel->iCodigo    = $oArtigo->iCodigo;
                $oCarrossel->sDescricao = $oArtigo->sResumo;
                $oCarrossel->sImagemUrl = $oArtigo->sUrlImagem;
                $oCarrossel->sLink      = $oArtigo->sLink;
                $oCarrossel->sAtivo     = $oArtigo->sAtivo;
                $oCarrossel->sTitulo    = $oArtigo->sTitulo;

                $arrObjCarrossel->append($oCarrossel);
            }
        }
        
        return $arrObjCarrossel;
    }
    public static final function pesquisaCarrossel( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Carrossel_lng_Codigo,
                           Carrossel_vch_Descricao,
                           Carrossel_vch_ImagemUrl,
                           Carrossel_vch_Link,
                           Carrossel_dat_Entrada,
                           Carrossel_dat_Expiracao,
                           Carrossel_bit_LinkExterno
                           FROM Carrossel
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oCarrossel = new Carrossel;
        
        if (is_array($mArrDados))
        { 
            $oCarrossel->iCodigo      = $mArrDados['Carrossel_lng_Codigo'];
            $oCarrossel->sDescricao   = $mArrDados['Carrossel_vch_Descricao'];
            $oCarrossel->sImagemUrl   = $mArrDados['Carrossel_vch_ImagemUrl'];
            $oCarrossel->sLink        = $mArrDados['Carrossel_vch_Link'];
            $oCarrossel->sEntrada     = $mArrDados['Carrossel_dat_Entrada'];
            $oCarrossel->sExpiracao   = $mArrDados['Carrossel_dat_Expiracao'];
            $oCarrossel->bLinkExterno = $mArrDados['Carrossel_bit_LinkExterno'];
        }
        
        return $oCarrossel;     
    }
    
    public static final function alteraCarrossel( $oCarrossel )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Carrossel SET

                    Carrossel_vch_Descricao = :descricao,
                    Carrossel_vch_ImagemUrl = :url,
                    Carrossel_vch_Link = :link,
                    Carrossel_dat_Entrada = :entrada,
                    Carrossel_dat_Expiracao = :expiracao,
                    Carrossel_bit_LinkExterno = :externo
                    WHERE Carrossel_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oCarrossel->iCodigo));
        $sSql->bindParam(':descricao', ($oCarrossel->sDescricao));
        $sSql->bindParam(':url',       ($oCarrossel->sImagemUrl));
        $sSql->bindParam(':link',      ($oCarrossel->sLink));
        $sSql->bindParam(':entrada',   ($oCarrossel->sEntrada));
        $sSql->bindParam(':expiracao', ($oCarrossel->sExpiracao));
        $sSql->bindParam(':externo',   ($oCarrossel->bLinkExterno));
        
        $sSql->execute();
    }
    
    public static final function salvaCarrossel( $oCarrossel )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Carrossel (
                    Carrossel_vch_Descricao,
                    Carrossel_vch_ImagemUrl,
                    Carrossel_vch_Link,
                    Carrossel_dat_Entrada,
                    Carrossel_dat_Expiracao,
                    Carrossel_bit_LinkExterno,
                    Carrossel_dat_Cadastro,
                    Usuario_lng_Codigo
                    ) VALUES ( ?,?,?,?,?,?,?,?)"); 
        
        $sSql->bindParam(1, ($oCarrossel->sDescricao));
        $sSql->bindParam(2, ($oCarrossel->sImagemUrl));
        $sSql->bindParam(3, ($oCarrossel->sLink));
        $sSql->bindParam(4, ($oCarrossel->sEntrada));
        $sSql->bindParam(5, ($oCarrossel->sExpiracao));
        $sSql->bindParam(6, ($oCarrossel->bLinkExterno));
        $sSql->bindParam(7, ($oCarrossel->sCadastro));
        $sSql->bindParam(8, ($oCarrossel->iCodigoUsuario));
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiCarrossel( $iCodigo  )
    {
        CarrosselDB::setaFiltro(' AND Carrossel_lng_Codigo = '.$_GET['codigo']);
        $oCarrossel = CarrosselDB::pesquisaCarrossel();
    
        $file = "images/banner/" . $oCarrossel->sImagemUrl;
    
        if ( file_exists( $file ) )
        {
            @unlink( $file );
        }
    
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Carrossel 
                    WHERE Carrossel_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
 
    
 
}

?>