<?php

class FotoDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem     = 'Foto_lng_Posicao';
    
    public static function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static function pesquisaFotoLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Foto_lng_Codigo,
                           Foto_vch_Caption,
                           Foto_vch_Url,
                           Foto_vch_Informacao
                           FROM Foto
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        //var_dump($mArrDados);
         /* Instancia o Objeto */
        $arrObjFoto = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oFoto  = new Foto;
                
                $oFoto->iCodigo     = $mArrDados[$a]['Foto_lng_Codigo'];
                $oFoto->sCaption    = $mArrDados[$a]['Foto_vch_Caption'];
                $oFoto->sUrl        = $mArrDados[$a]['Foto_vch_Url'];
                $oFoto->sInformacao = $mArrDados[$a]['Foto_vch_Informacao'];
               
                $arrObjFoto->append($oFoto);
            }
        } 
        return $arrObjFoto;
    }
    public static function pesquisaFoto( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Foto_lng_Codigo,
                           Foto_vch_Caption,
                           Foto_vch_Url
                           FROM Foto
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oFoto = new Foto;
      
        if (is_array($mArrDados))
        { 
           $oFoto->iCodigo  = $mArrDados['Foto_lng_Codigo'];
           $oFoto->sCaption = $mArrDados['Foto_vch_Caption'];
           $oFoto->sUrl     = $mArrDados['Foto_vch_Url'];
        }
        
        return $oFoto;     
    }
    
    public static final function salvaFoto( $oFoto )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Foto (
                    Album_lng_Codigo,
                    Foto_vch_Url,
                    Foto_dat_Data,
                    Foto_lng_Posicao
                    ) VALUES ( ?,?,?,? )"); 
        
        $sSql->bindParam(1, ($oFoto->iAlbumCodigo));
        $sSql->bindParam(2, ($oFoto->sUrl));
        $sSql->bindParam(3, ($oFoto->sData));
        $sSql->bindParam(4, ($oFoto->iPosicao));
        
        $sSql->execute();
        
        return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function alteraFoto( $oFoto )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Foto SET
                    Foto_vch_Caption      = :caption
                    WHERE Foto_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oFoto->iCodigo));
        $sSql->bindParam(':caption',   ($oFoto->sCaption));
        //$sSql->bindParam(':url',($oFoto->sUrl));
        
        $sSql->execute();
    }
    
    public static final function alteraPosicao( $oFoto )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Foto SET
                    Foto_lng_Posicao      = :posicao
                    WHERE Foto_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oFoto->iCodigo));
        $sSql->bindParam(':posicao',   ($oFoto->iPosicao));
        
        $sSql->execute();
    }
    
    public static final function excluiFoto( $iCodigo  )
    {
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Foto 
                    WHERE Foto_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
    
    public static final function contaFoto( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sJoin    = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            //self::$mArrCampos['CONDICAO'] = '';
        }
        
        // Define os Joins
        if (isset(self::$mArrJoin['JOIN']))
        {
            if ( self::$mArrJoin['JOIN'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrJoin['JOIN']); $a < $iCount; ++$a)
                {
                    $sJoin .= (self::$mArrJoin['JOIN'][$a]);
                }
            }
            //Limpa Filtros
            //self::$mArrJoin['JOIN'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           count(*)
                           FROM Foto
                           ".$sJoin."
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        //echo $mResultado->queryString;
        $mResultado->execute();
        
        $iTotal = $mResultado->fetchColumn();
        
        return $iTotal;     
    }
    
 
}

?>