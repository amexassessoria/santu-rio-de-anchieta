<?php

class Foto
{
    
    /* Atributos */
    private $iCodigo;
    private $sUrl;
    private $sCaption;
    private $sData;
    private $iPosicao;
    private $sInformacao;
    
    private $iAlbumCodigo;
    
    /* M�todos m�gicos GET e SET */
    public function __get($property) 
    {
        if (property_exists($this, $property)) 
        {
          return $this->$property;
        }
    }
    
    public function __set($property, $value) 
    {
        if (property_exists($this, $property)) 
        {
          $this->$property = $value;
        }
        return $this;
    }

}

?>