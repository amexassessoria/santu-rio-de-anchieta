<?php

class VelaDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem     = 'Vela_lng_Codigo DESC';
    private static $iLimite    = '';
    private static $iInicio    = '';
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaLimite($iArgLimite, $iArgInicio = 0)
    {
        self::$iLimite = $iArgLimite;
        self::$iInicio = $iArgInicio;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function pesquisaVelaLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
        
        if ( self::$iLimite <> '')
        {
            $sLimite = " LIMIT ".self::$iInicio." , ".self::$iLimite;

            //Limpa Filtro
            self::$iInicio = '';
            self::$iLimite = '';
        }
        
        $mResultado = $oConexao->prepare("SELECT
                           Vela_lng_Codigo,
                           Vela_vch_Nome,
                           Vela_vch_Email,
                           Vela_vch_Cidade,
                           Vela_chr_Estado,
                           Vela_txt_Intencao,
                           Vela_chr_ExibirIntencao,
                           Vela_dat_Data
                           FROM Vela
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                           ".$sLimite."
                         ");  
        
        //echo $mResultado->queryString;
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjArtigo = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oVela  = new Vela;
                
                $oVela->iCodigo         = $mArrDados[$a]['Vela_lng_Codigo'];
                $oVela->sNome           = $mArrDados[$a]['Vela_vch_Nome'];
                $oVela->sEmail          = $mArrDados[$a]['Vela_vch_Email'];
                $oVela->sCidade         = $mArrDados[$a]['Vela_vch_Cidade'];
                $oVela->sEstado         = $mArrDados[$a]['Vela_chr_Estado'];
                $oVela->sIntencao       = $mArrDados[$a]['Vela_txt_Intencao'];
                $oVela->sExibirIntencao = $mArrDados[$a]['Vela_chr_ExibirIntencao'];
                $oVela->sData           = $mArrDados[$a]['Vela_dat_Data'];
                $oVela->iDias           = Util::retornaDiferencaDeDias($mArrDados[$a]['Vela_dat_Data'], date( 'Y-m-d' ));
                
                $arrObjArtigo->append($oVela);
            }
        }
        return $arrObjArtigo;
    }
    public static final function pesquisaVela( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Vela_lng_Codigo,
                           Vela_vch_Nome,
                           Vela_vch_Email,
                           Vela_chr_Estado,
                           Vela_txt_Intencao,
                           Vela_chr_ExibirIntencao,
                           Vela_dat_Data
                           FROM Vela
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oVela = new ArtigoTIpo;
        
        if (is_array($mArrDados))
        { 
           $oVela->iCodigo         = $mArrDados['Vela_lng_Codigo'];
           $oVela->sNome           = $mArrDados['Vela_vch_Nome'];
           $oVela->sEmail          = $mArrDados['Vela_vch_Email'];
           $oVela->sEstado         = $mArrDados['Vela_chr_Estado'];
           $oVela->sIntencao       = $mArrDados['Vela_txt_Intencao'];
           $oVela->sExibirIntencao = $mArrDados['Vela_chr_ExibirIntencao'];
           $oVela->sData           = $mArrDados['Vela_dat_Data'];
        }
        
        return $oVela;     
    }
    
    public static final function contaVela( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            //self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           count(*)
                           FROM Vela
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $iTotal = $mResultado->fetchColumn();
        
        return $iTotal;     
    }
    
    public static final function salvaVela( $oVela )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Vela (
                    Vela_vch_Nome,
                    Vela_vch_Email,
                    Vela_chr_Estado,
                    Vela_vch_Cidade,
                    Vela_txt_Intencao,
                    Vela_chr_ExibirIntencao,
                    Vela_dat_Data
                    ) VALUES ( ?,?,?,?,?,?,? )"); 
        
        $sSql->bindParam(1, ($oVela->sNome));
        $sSql->bindParam(2, ($oVela->sEmail));
        $sSql->bindParam(3, ($oVela->sEstado));
        $sSql->bindParam(4, ($oVela->sCidade));
        $sSql->bindParam(5, ($oVela->sIntencao));
        $sSql->bindParam(6, ($oVela->sExibirIntencao));
        $sSql->bindParam(7, ($oVela->sData));
        $sSql->execute();
        
        return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
 
}

?>