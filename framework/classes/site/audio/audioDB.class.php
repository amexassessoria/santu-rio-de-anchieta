<?php

class AudioDB
{
    
    private static $mArrCampos = '';
    private static $mArrJoin   = '';
    private static $sOrdem = 'Audio_lng_Codigo';
    private static $iLimite = 0;
    private static $iInicio = 0;
    
    public static final function setaFiltro($sArgCampo)
    {
        self::$mArrCampos['CONDICAO'][] = $sArgCampo;
    }
    
    public static final function setaJoin($sArgJoin)
    {
        self::$mArrJoin['JOIN'][] = $sArgJoin;
    }
    
    public static final function setaOrdem($sArgOrdem)
    {
        self::$sOrdem = $sArgOrdem;
    }
    
    public static final function setaLimite($iArgLimite, $iArgInicio = 0)
    {
        self::$iLimite = $iArgLimite;
        self::$iInicio = $iArgInicio;
    }
    
    public static final function pesquisaAudioLista( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sLimite  = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            if ( self::$mArrCampos['CONDICAO'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
                {
                    $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
                }
                //Limpa Filtros
                self::$mArrCampos['CONDICAO'] = '';
            }
        }
        
        /* Define o Limite */
        if (self::$iLimite > 0)
        {
            $sLimite = (' LIMIT '.self::$iInicio.",".self::$iLimite);

            //Limpa Filtro
            self::$iInicio = 0;
            self::$iLimite = 0;
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Audio_lng_Codigo,
                           Audio_vch_Descricao,
                           Audio_vch_Url
                           FROM Audio
                           WHERE 1 = 1 ".$sFiltros."                       
                           ORDER BY ".self::$sOrdem."
                           ".$sLimite."
                         ");  
        
        $mResultado->execute();

        $mArrDados = $mResultado->fetchAll(PDO::FETCH_ASSOC);
        
         /* Instancia o Objeto */
        $arrObjAudio = new ArrayObject();
        
        if (is_array($mArrDados))
        { 
            for ($a = 0, $iCount = count($mArrDados); $a < $iCount; ++$a)
            {
                 /* Instancia o Objeto */
                $oAudio  = new Audio;
                
                $oAudio->iCodigo    = $mArrDados[$a]['Audio_lng_Codigo'];
                $oAudio->sDescricao = $mArrDados[$a]['Audio_vch_Descricao'];
                $oAudio->sUrl       = $mArrDados[$a]['Audio_vch_Url'];
                
                $arrObjAudio->append($oAudio);
            }
        }
        return $arrObjAudio;
    }
    public static final function pesquisaAudio( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            self::$mArrCampos['CONDICAO'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           Audio_lng_Codigo,
                           Audio_vch_Descricao,
                           Audio_vch_Url,
                           Audio_Tipo_lng_Codigo
                           FROM Audio
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        
        $mResultado->execute();
        
        $mArrDados = $mResultado->fetch(PDO::FETCH_ASSOC);
        ;
         /* Instancia o Objeto */
        $oAudio = new Audio;
        
        if (is_array($mArrDados))
        { 
           $oAudio->iCodigo    = $mArrDados['Audio_lng_Codigo'];
           $oAudio->sDescricao = $mArrDados['Audio_vch_Descricao'];
           $oAudio->sUrl       = $mArrDados['Audio_vch_Url'];
           
           AudioTipoDB::setaFiltro(" AND Audio_Tipo_lng_Codigo = ".$mArrDados['Audio_Tipo_lng_Codigo']);
           $oAudioTipo = AudioTipoDB::pesquisaAudioTipo();
           $oAudio->oAudioTipo = $oAudioTipo;
           
        }
        
        return $oAudio;     
    }
    
    public static final function alteraAudio( $oAudio )
    {                 
        $oConexao = db::conectar();
        
        $sSql=$oConexao->prepare("UPDATE Audio SET
                    Audio_vch_Descricao    = :descricao,
                    Audio_vch_Url          = :url,
                    Audio_Tipo_lng_Codigo  = :tipo
                    WHERE Audio_lng_Codigo = :codigo " );
 
        $sSql->bindParam(':codigo',    ($oAudio->iCodigo));
        $sSql->bindParam(':descricao', ($oAudio->sDescricao));
        $sSql->bindParam(':url',       ($oAudio->sUrl));
        $sSql->bindParam(':tipo',      ($oAudio->oAudioTipo->iCodigo));
        
        $sSql->execute();
    }
    
    public static final function salvaAudio( $oAudio )
    { 
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("INSERT INTO Audio (
                    Audio_vch_Descricao,
                    Audio_vch_Url,
                    Audio_Tipo_lng_Codigo
                    ) VALUES ( ?,?,? )"); 
        
        $sSql->bindParam(1, ($oAudio->sDescricao));
        $sSql->bindParam(2, ($oAudio->sUrl));
        $sSql->bindParam(3, ($oAudio->oAudioTipo->iCodigo));
        $sSql->execute();
        
        //return $oConexao->lastInsertId(); 
        //$sSql->debugDumpParams(); 
    }
    
    public static final function excluiAudio( $iCodigo  )
    {
        
        AudioDB::setaFiltro(' AND Audio_lng_Codigo = '.$iCodigo);
        $oAudio = AudioDB::pesquisaAudio();
    
        $file = "sounds/" . $oAudio->sUrl;
    
        if ( file_exists( $file ) )
        {
            @unlink( $file );
        }
        
        $oConexao = db::conectar();
        
        $sSql = $oConexao->prepare("DELETE FROM Audio 
                    WHERE Audio_lng_Codigo = :codigo ");
       
        $sSql->bindParam(':codigo',$iCodigo, PDO::PARAM_INT);   
        $sSql->execute();
    }
   
    public static final function contaAudio( )
    {
        $oConexao = db::conectar();
        
        $sFiltros = '';
        $sJoin    = '';
        
        // Define os Filtros
        if (isset(self::$mArrCampos['CONDICAO']))
        {
            for ($a = 0, $iCount = count(self::$mArrCampos['CONDICAO']); $a < $iCount; ++$a)
            {
                $sFiltros .= (self::$mArrCampos['CONDICAO'][$a]);
            }
            //Limpa Filtros
            //self::$mArrCampos['CONDICAO'] = '';
        }
        
        // Define os Joins
        if (isset(self::$mArrJoin['JOIN']))
        {
            if ( self::$mArrJoin['JOIN'] <> '' )
            {
                for ($a = 0, $iCount = count(self::$mArrJoin['JOIN']); $a < $iCount; ++$a)
                {
                    $sJoin .= (self::$mArrJoin['JOIN'][$a]);
                }
            }
            //Limpa Filtros
            //self::$mArrJoin['JOIN'] = '';
        }
             
        $mResultado = $oConexao->prepare("SELECT
                           count(*)
                           FROM Audio
                           ".$sJoin."
                           WHERE 1 = 1 ".$sFiltros."                       
                         ");  
        //echo $mResultado->queryString;
        $mResultado->execute();
        
        $iTotal = $mResultado->fetchColumn();
        
        return $iTotal;     
    }
    
 
}

?>