<?php

class Config
{
    //Dados do Sistema
    const NOME_SITE       = "Santu�rio Nacional S�o Jos� de Anchieta";
    const EMAIL_SITE      = "contato@santuariodeanchieta.com";
	const EMAIL_CAMPANHA  = "contato@santuariodeanchieta.com";  
    //const EMAIL_CAMPANHA  = "fa.bio-santos@hotmail.com"; 
    const NUMERO_CAMPANHA = "999";
    const NUMERO_CAMPANHA_DOACAO_ANONIMA = "998";
	const SITE_CODIGO = 2;
    
	/* Recaptcha */
    const RECAPTCHA_PUBLIC_KEY  = "6LeEIBgUAAAAAJpBbKWrju0wmhcCoS2mp_tzdQMU";
    const RECAPTCHA_PRIVATE_KEY = "6LeEIBgUAAAAAHR7ckRVqB_lDjNrtxPOlc5A8ssR";
    
    //Informa��es de Conex�o - DB MySQL
    const HOST = 'mysql.santuariodeanchieta.com';
    const USER = 'santuariodeanc03';
    const PSWD = 'sda0317';
    const DB   = 'santuariodeanc03';

    //E-mails espiritualidade
    const EMAIL_TESTEMUNHO = 'contato-site@santuariodeanchieta.com';
    const SENHA_EMAIL_TESTEMUNHO = 'SnsjA2017';
    const EMAIL_PEDIDODEORACAO = 'contato-site@santuariodeanchieta.com';
    const SENHA_EMAIL_PEDIDOORACAO = 'SnsjA2017';
	
	// Configura��es de SMTP
    const SMTP_EMAIL = "contato-site@santuariodeanchieta.com";
    const SMTP_HOST  = "smtp.kinghost.net";
    const SMTP_PWD   = "SnsjA2017";
	
	/* Configura��es do ISSUU */
    const ISSUU_KEY    = "donjx7erxqnuuuzxl9jj1032bupjuc6l";
    const ISSUU_SECRET = "3uafpp1rzq87j7irj9s89f2fchvhmdhd";
    const ISSUU_NAME   = "santuarionacionalsaojosedeanchieta";

    //Outras Configura��es
    const QUANTIDADE_TESTEMUNHOS = 2;
    const NUMERO_VELAS = 9;
    const NUMERO_PAGINAS_BUSCA = 10;
    const QUANTIDADE_ULTIMAS_NOTICIAS = 5;
    const QUANTIDADE_VIDEOS = 2;
    const QUANTIDADE_NUMERO_ALBUNS_TELA_INICIAL = 2;
	const NUMERO_PAGINAS_MULTIMIDIA = 9;
    const QUANTIDADE_NOTICIAS_CARROSSEL = 5;

    private final function __construct()
    {
        
    }

    private final function __clone()
    {
        
    }

    private final function __wakeup()
    {
        
    }

    public static final function loadConfig()
    {
        /* Carrega Configura��es Padr�es se Forem Necess�rias */

    }

    public static final function loadController()
    {
        try
        {
            if (file_exists( PATH_FW .'/classes/core/controller.class.php'))
            {
                include_once (  PATH_FW .'/classes/core/controller.class.php' );
            }
            else
            {
                throw new Exception('CONFIG:Arquivo do Controller n&#227;o encontrado.', 1000);
            }
        }
        catch (Exception $e)
        {
            die('#' . $e->getCode() . ' : ' . $e->getMessage());
        }
    }

}

?>