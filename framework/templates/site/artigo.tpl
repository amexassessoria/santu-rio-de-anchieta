<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    {include file="site/_header.tpl"}
    <link href="{$WWW_CSS}ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css" />
    <script src="{$WWW_JS}jquery-ui-1.10.3.custom.min.js"></script>
    <link href="{$WWW_CSS}jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{$WWW_CSS}jquery.bxslider.css" />
    <body>
        {include file="site/_topo.tpl"}
        <div id="wrapper-interna" >
            <!-- Main Wrapper -->
            <div class="container">
                <section id="artigo">

                    <div class="divArticleHeader">
                        <h1 id="h1Artigo">
                            {if ($oArtigo->oArtigoTipo->iCodigo == 3)}Santu�rio{else}{$oArtigo->oArtigoTipo->sDescricao}{/if}
                            {if ($oArtigo->oArtigoTipo->iCodigo <> 4) &&  ($oArtigo->oArtigoTipo->iCodigo <> 5)}
                                {if ($oArtigo->oArtigoTipo->iCodigo <> 3)}
                                    > {$oArtigo->sDataCadastro|date_format:"%d"} de {Util::retornaMesExtenso($oArtigo->sDataCadastro|date_format:"%m")}, {$oArtigo->sDataCadastro|date_format:"%Y"}
                                {/if}
                            {else}
                                <span id="spanArticleData">{$oArtigo->sDia} de {$oArtigo->sMesExtenso} {if $oArtigo->sAno <> NULL}- Ano {$oArtigo->sAno}{/if}</span>
                            {/if}
                        </h1>
                        <h2>{$oArtigo->sTitulo}</h2>
                    </div>

                    <!-- Content -->
                    <article>
                        <div id="divArticleResumo">
                            {if trim($oArtigo->sResumo) <> ''}
                                {$oArtigo->sResumo}
                            {/if}
                        </div>

                        <div id="divArticleContent">
                            <div id="divConteudoInterna">
                                {if (($oArtigo->oArtigoTipo->iCodigo <> 4) && ($oArtigo->oArtigoTipo->iCodigo <> 5))}
                                    {if ($oArtigo->sUrlImagem <> '')}
                                        {if $oArtigo->sOcultarMiniatura <> 'S'}
                                            <img class="imgFloat" src="{$oArtigo->sUrlImagem}"/>
                                        {/if}
                                    {/if}
                                {/if}

                                {$oArtigo->sConteudo}

                                {if trim($oArtigo->sCredito) <> ''}
                                    <div id="divArticleCredito">
                                        <strong>Por:</strong> {$oArtigo->sCredito}
                                    </div>
                                {/if}

                                {if trim($oArtigo->sFonteDescricao) <> ''}
                                    <br/>
                                    <i>Fonte: <a href="{$oArtigo->sFonteUrl}" target="_blank">{$oArtigo->sFonteDescricao}</a></i>
                                {/if}

                                <br class="clear"/>

                                {if $oArtigo->oAlbum->iCodigo <> ''}
                                    <div id="divNomeAlbumArtigo">{$oArtigo->oAlbum->sTitulo}</div>
                                    <ul class="galeriaDeFotos">
                                        {foreach $oArtigo->oAlbum->arrObjFoto as $oFoto}
                                            <li><img src="images/fotosAlbuns/{$oFoto->sUrl}" title="{$oFoto->sCaption}" /></li>
                                        {/foreach}
                                    </ul>

                                    <div class="thumbsArtigoAlbum">
                                        {foreach $oArtigo->oAlbum->arrObjFoto as $oFoto}
                                            <a data-slide-index="{$oFoto@index}" href=""><div class="thumbAlbum" style="background-image: url('images/fotosAlbuns/{$oFoto->sUrl}');"></div></a>
                                        {/foreach}
                                    </div>

                                    <div id="divCreditos" class="text-center">
                                        {if (trim($oArtigo->oAlbum->sCredito <> ''))}<label>Fotos : <strong>{$oArtigo->oAlbum->sCredito}</strong></label>{/if}
                                    </div>
                                {/if}

                                <br />

                                {if ($oArtigo->oArtigoTipo->iCodigo == 4)}
                                    <div id="divFonteEvangelho">
                                        Fonte: <a href="http://www.paulinas.org.br/">Portal Paulinas</a>
                                    </div>
                                    <br/>
                                {/if}

                                {if ($arrObjSanto <> '') && (count($arrObjSanto) > 0)}
                                    <h3>Outros Santos do dia {$oArtigo->sDia}/{$oArtigo->sMes}</h3>
                                    {foreach $arrObjSanto as $oSanto}
                                        <a href="{$oSanto->sLink}" title="">{$oSanto->sTitulo}</a>
                                        <br />
                                    {/foreach}
                                {/if}

                                <div id="divNavArtigo">
                                    {if ($oArtigo->oArtigoTipo->iCodigo == 4)}
                                        <div class="left">
                                            <a href="{$oEvangelhoAnterior->sLink}&{$oEvangelhoAnterior->sAno}&{$oEvangelhoAnterior->sMes}&{$oEvangelhoAnterior->sDia}" title="{$oEvangelhoAnterior->sTitulo}"><< Anterior</a>
                                        </div>
                                        <div class="right">
                                            <a href="{$oEvangelhoProximo->sLink}&{$oEvangelhoProximo->sAno}&{$oEvangelhoProximo->sMes}&{$oEvangelhoProximo->sDia}" title="{$oEvangelhoProximo->sTitulo}">Pr�ximo >></a>
                                        </div>
                                    {/if}

                                    {if ($oArtigo->oArtigoTipo->iCodigo == 5)}
                                        <div class="left">
                                            <a href="{$oSantoAnterior->sLink}" title="{$oSantoAnterior->sTitulo}">Anterior</a>
                                        </div>

                                        <div class="right">
                                            <a href="{$oSantoProximo->sLink}" title="{$oSantoProximo->sTitulo}">Pr�ximo</a>
                                        </div>
                                    {/if}

                                </div>
                                <br />
                            </div>

                            <form action="{$WWW}imprimirArtigo" method="POST" style="float: right">
                                <input type="hidden" value="{$oArtigo->iCodigo}" name="codigo"/>
                                <button type="submit"><i class="fa fa-print"></i>Imprimir</button>
                            </form>

                            <div id="divSocialsFooter">
                                <span class='st_facebook_large' displayText='Facebook'></span>
                                <span class='st_twitter_large' displayText='Tweet'></span>
                                <span class='st_googleplus_large' displayText='Google +'></span>
                                <span class='st_email_large' displayText='Email'></span>
                                <span class='st_fblike_large' displayText='Facebook Like'></span>
                            </div>

                            <br class="clear"/>

                            

                            <div id="disqus_thread"></div>

                            <div id="disqus_thread"></div>
                                <script type="text/javascript">

                                    /* * * CONFIGURATION VARIABLES * * */
                                    var disqus_shortname = 'santuario-nacional-sao-jose-de-anchieta';

                                    /* * * DON'T EDIT BELOW THIS LINE * * */
                                    (function() {
                                        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                        dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                                    })();
                                </script>
                                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
                            </div>   
                    </article>
                </section>
                {include file="site/_lateral.tpl"}
            </div> 
        </div>

        {include file="site/_footer.tpl"}    

        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript" src="{$WWW_JS}jquery.tagcanvas.min.js"></script>
        <script type="text/javascript" src="{$WWW_JS}jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="{$WWW_JS}jquery.mCustomScrollbar.min.js"></script>
        <script src="{$WWW_JS}jquery.bxslider.min.js"></script>
        <script>

            stLight.options({
                publisher: "7be91bcf-7d46-4d0d-a1f1-c6deb2c354d0", doNotHash: true, doNotCopy: false, hashAddressBar: false
            });

            /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
            var disqus_shortname = 'santuario-nacional-sao-jose-de-anchieta'; // required: replace example with your forum shortname

            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function () {
                var s = document.createElement('script'); s.async = true;
                s.type = 'text/javascript';
                s.src = '//' + disqus_shortname + '.disqus.com/count.js';
                (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
            }());

            $(document).ready(function () {

               if( ! $('#myCanvas').tagcanvas({
                    textColour: '#b39630',
                    outlineColour: '#b39630',
                    textFont: 'Droid Sans'
                  })) {
                    $('#divTags').hide();
                }

                {if (($oArtigo->oArtigoTipo->iCodigo == 4) || ($oArtigo->oArtigoTipo->iCodigo == 5))}
                    
                    $('#divEvangelho').accordion({
                        header: 'h5',
                        collapsible: true,
                        active: false,
                        heightStyle: "content"
                    });

                {/if}

                {if $oArtigo->oAlbum->iCodigo <> ''}
                
                    $('.galeriaDeFotos').bxSlider({
                        mode: 'fade',
                        controls: true,
                        captions: true,
                        pagerCustom: '.thumbsArtigoAlbum'
                    });

                    $(".thumbsArtigoAlbum").mCustomScrollbar({
                        horizontalScroll:true,
                        scrollButtons:{
                                enable:false
                        },
                        theme:"dark-thin"
                    });

                {/if}

            });
        </script>
    </body>
</html>