<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    {include file="site/_header.tpl"}
    <link rel="stylesheet" type="text/css" href="css/animate.css"/>
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css"/>
    <body>
        {include file="site/_topo.tpl"}
        <section class="slider">
            <ul>
                {foreach $arrObjCarrossel as $oCarrossel}
                    <li style="background-image:url('{$oCarrossel->sImagemUrl}')">
                        <a href="{$oCarrossel->sLink}"  {if $oCarrossel->bLinkExterno}data-target="_blank"{/if}>
                            {if $oCarrossel->sTitulo !== ''}<h1>{$oCarrossel->sTitulo}</h1>{/if}
                        </a>
                    </li>
                {/foreach}
            </ul>
        </section>
        <section class="atualidades">
            <div class="titulo-linha">
                <h1>Atualidades</h1>
            </div>
            <ul class="menu">
                <li><a href="tag/1/2">Santu�rio</a></li>
                <li><a href="tag/1/7">Vaticano</a></li>
                <li><a href="tag/1/8">CNBB</a></li>
                <li><a href="artigos/1">Artigos</a></li>
                <li><a href="noticias/1">Ver todas</a></li>
            </ul>
            <div class="container">
                <ul class="noticias">
                    {foreach $arrObjUltimasNoticias as $oUltimasNoticias}
                        <li>
                            <div class="thumb" style="background-image:url('{$oUltimasNoticias->sUrlImagem}')"></div>
                            <div class="texto">
                                <a href="{$oUltimasNoticias->sLink}">
                                    {if $oUltimasNoticias@first}
                                        <h1>{$oUltimasNoticias->sTitulo|truncate:100}</h1>
                                        <img src="images/home/leia-mais.png"/>
                                    {else}
                                        {foreach $oUltimasNoticias->arrObjArtigoTag as $oArtigoTag}
                                            <h2>{$oArtigoTag->sDescricaoTag}{if !$oArtigoTag@last},{/if}</h2>
                                        {/foreach}
                                        <h1>{$oUltimasNoticias->sTitulo|truncate:100}</h1>
                                    {/if}
                                </a>
                            </div>
                        </li>
                    {/foreach}
                </ul>
            </div>
        </section>
        <section class="santuario-virtual">
            <div class="titulo-linha">
                <h1>Santu�rio virtual</h1>
            </div>
            <ul>
                <li>
                    <a href="evangelho-do-dia" class="hvr-float">
                        <div class="thumb">
                            <img src="images/home/evangelho.png"/>
                        </div>
                        <h1>Evangelho <br/> do dia</h1>
                        <h2>A Palavra do Senhor � alimento para a nossa Alma.</h2>
                    </a>
                </li>
                <li>
                    <a href="santo-do-dia" class="hvr-float">
                        <div class="thumb">
                            <img src="images/home/santo.png"/>
                        </div>
                        <h1>Santo do dia</h1>
                        <h2>Conhe�a o exemplo de santidade que hoje nos inspira.</h2>
                    </a>
                </li>
                <li>
                    <a href="intencao-de-missa"  class="hvr-float">
                        <div class="thumb">
                            <img src="images/home/intencao.png"/>
                        </div>
                        <h1>Inten��es <br/> de missa</h1>
                        <h2>Deixe aqui suas inten��es de missa</h2>
                    </a>
                </li>
                <li>
                    <a href="pedido-de-oracao" class="hvr-float">
                        <div class="thumb">
                            <img src="images/home/pedido.png"/>
                        </div>
                        <h1>Pedido <br/> de ora��o</h1>
                        <h2>Deixe seus pedidos sob a intercess�o de S�o Jos� de Anchieta.</h2>
                    </a>
                </li>
                <li>
                    <a href="artigo/novena-em-honra-a-sao-jose-de-anchieta-o-apostolo-do-brasil.html" class="hvr-float">
                        <div class="thumb">
                            <img src="images/home/novena.png"/>
                        </div>
                        <h1>Novena de <br/> S. J. de Anchieta</h1>
                        <h2>Acompanhe a novena completa.</h2>
                    </a>
                </li>
                <li>
                    <a href="velavirtual/1" class="hvr-float">
                        <div class="thumb">
                            <img src="images/home/vela.png"/>
                        </div>
                        <h1>Vela virtual</h1>
                        <h2>O Senhor acolher� o seu pedido. Acenda sua vela virtual.</h2>
                    </a>
                </li>
                <li>
                    <a href="testemunho/1" class="hvr-float">
                        <div class="thumb">
                            <img src="images/home/gracas.png"/>
                        </div>
                        <h1>Gra�as alcan�adas</h1>
                        <h2>Compartilhe conosco sua hist�ria de f�!</h2>
                    </a>
                </li>
            </ul>
        </section>
        <section class="campanha">
            <div class="titulo-linha">
                <h1>Campanha</h1>
            </div>
            <div class="container">
                <a href="campanha">
                    <div class="fundo-campanha">
                        <div class="titulo" data-0="background-position: center -300px" data-1100="background-position: center -300px" data-1400="background-position: center 0"></div>
                        <div class="arte" data-0="background-position:  300px 0" data-1100="background-position: 300px 0" data-1400="background-position: 0 0"></div>
                    </div>
                </a>
            </div>
        </section>
        <div class="titulo-linha titulo-duplo">
            <div class="container">
                <h1>Multim�dia</h1>
                <h1>Facebook</h1>
            </div>
        </div>
        <div class="container agrupa">
            <section class="multimidia">
                <ul>
                    {foreach $arrObjGaleriaDeFotos as $oGaleriaDeFotos}
                        <li class="foto">
                            <a href="fotos/{$oGaleriaDeFotos->iCodigo}">
                                <div class="thumb" style="background-image: url('{if ($oGaleriaDeFotos->iCodigo < 20)}{$oGaleriaDeFotos->sUrlCapa}{else}images/fotosAlbuns/{$oGaleriaDeFotos->sUrlCapa}{/if}')"></div>
                                <div class="icone"></div>
                                <div class="texto">{$oGaleriaDeFotos->sTitulo}</div>
                            </a>
                        </li>
                    {/foreach}
                </ul>
                <div class="text-center">
                    <a href="albuns/1" class="btn">Ver mais</a>
                </div>
            </section>
            <section class="facebook">
                <div class="facebook-timeline">
                    <div id="fb-root"></div>
                    <script type="text/javascript">
                        (function(d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) return;
                        js = d.createElement(s); js.id = id;
                        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8&appId=938121589539957";
                        fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));
                    </script>
                    <div class="fb-page" data-href="https://www.facebook.com/santuariodeanchieta/" data-tabs="timeline" data-width="500" data-height="470" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                        <blockquote cite="https://www.facebook.com/santuariodeanchieta/" class="fb-xfbml-parse-ignore">
                            <a href="https://www.facebook.com/santuariodeanchieta/">Santu�rio Nacional de S�o Jos� de Anchieta</a>
                        </blockquote>
                    </div>
                </div>
            </section>
        </div>
        <div class="titulo-linha titulo-duplo">
            <div class="container">
                <h1>Turismo</h1>
                <h1>Links</h1>
            </div>
        </div>
        <div class="container agrupa">
            <section class="turismo">
                <div class="banner-turismo hvr-grow-rotate" >
                    <a href="turismo" >
                        <div class="icones-turismo"></div>
                    </a>
                </div>
            </section>
            <section class="links">
                <ul>
                    <li>
                        <a href="artigo/biblioteca-padre-diogo-fernandes-sj.html" class="hvr-back-pulse">
                            Biblioteca
                        </a>
                    </li>
                    <li>
                        <a href="museu" class="hvr-back-pulse">
                            Museu
                        </a>
                    </li>
                    <li>
                        <a href="artigo/arquivo.html" class="hvr-back-pulse">
                            Arquivo
                        </a>
                    </li>
                </ul>
            </section>
        </div>
        <section class="loja">
            <div class="titulo-linha">
                <h1>Loja Virtual</h1>
            </div>
            <div class="container">
                <a href="artigo/loja-virtual.html">
                    <div class="fundo-loja">
                        <div class="texto">EM BREVE, VOCE PODER� ADQUIRIR<br/>
                        PRODUTOS OFICIAIS NA LOJA ONLINE.</div>
                        <div class="canecas"></div>
                    </div>
                </a>
            </div>
        </section>
        {if $oPopup}
            <div id="popup-home" >
                <a href="#" class="fechar"><img src="images/home/fechar.png"/></a>
                <a href="{$oPopup->sLink}" {if $oPopup->bLinkExterno}target="_blank"{/if} class="click">
                    <img src="images/banner/{$oPopup->sImagemUrl}" />
                </a> 
                <div><a href="#" class="nao-ver-mais">N�o desejo ver mais essa janela</a></div>
            </div>
        {/if}

        {include file="site/_footer.tpl"}   

        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script type="text/javascript" src="js/skrollr.js"></script>
        <script type="text/javascript" src="{$WWW_JS}js.cookie.js"></script>
        <script type="text/javascript">        

            if ($(window).width() >= 1024) {
                var skrollr = skrollr.init({
                    forceHeight: false
                });
            }

            /*$(window).scroll(function (event) {
                var scroll = $(window).scrollTop();
                console.log(scroll);
            });*/

            var slider = $('.slider ul');

            slider.owlCarousel({
                loop: true,
                nav: false,
                autoplay: true,
                dots: true,
                margin: 0,
                items: 1,
                smartSpeed: 500,
                autoplayTimeout: 10000
            });
        
            jQuery(document).ready(function() {

                {if $oPopup}

                    var oCookie = Cookies.get('popup-'+{$oPopup->iCodigo});

                    $("#popup-home .nao-ver-mais").click(function(e){
                        e.preventDefault();
                        $("#popup-home").hide();
                        Cookies.set('popup-'+{$oPopup->iCodigo}, true, { expires: 365 });
                    }); 

                    $("#popup-home .fechar").click(function(e){
                        e.preventDefault();
                        $("#popup-home").hide();
                    });

                    setTimeout(function(){ 
                        if (oCookie == undefined || oCookie == null) {
                            $("#popup-home").show();
                        }
                    }, 3000);

                {/if}

                setTimeout(function(){ 
                    $(".abertura").hide();
                }, 3000);
            });	//ready

        </script>
    </body>
</html>