<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    {include file="site/_header.tpl"}
    <body>
        {include file="site/_topo.tpl"}
        <div id="wrapper-interna">
            <div class="container">
                <section id="artigo" class="noPadding">
                    <div class="divArticleHeader">
                        <h2>Informativos</h2>
                    </div>
                    <article>
                        <div id="divConteudoInterna">
                            <div id="divSocialsFooter" class="right">
                                <span class='st_facebook_large' displayText='Facebook'></span>
                                <span class='st_twitter_large' displayText='Tweet'></span>
                                <span class='st_googleplus_large' displayText='Google +'></span>
                                <span class='st_email_large' displayText='Email'></span>
                                <span class='st_fblike_large' displayText='Facebook Like'></span>
                            </div>
                            <div id="divArticleContent">
                                {foreach $arrInformativo->rsp->_content->result->_content as $oInformativo}
                                    <div class="left">
                                        <a href="http://issuu.com/{CONFIG::ISSUU_NAME}/docs/{$oInformativo->document->name}" target="_blank">
                                            <div class="divInformativosAnteriores">
                                                <img src="http://image.issuu.com/{$oInformativo->document->documentId}/jpg/page_1_thumb_large.jpg" />
                                            </div>
                                            <div class="divDataInformativosAnteriores">{$oInformativo->document->title|utf8_decode}</div>
                                        </a>
                                    </div>
                                {foreachelse}
                                    Nenhuma informativo encontrado.
                                {/foreach}
                                <a class="botao-ver-mais-informativos" href="https://issuu.com/{CONFIG::ISSUU_NAME}" target="_blank"><button>Ver mais Informativos</button></a>
                            </div>

                    </article>
                </section>
                {include file="site/_lateral.tpl"}
            </div>
        </div>
        {include file="site/_footer.tpl"}
    </body>
</html>