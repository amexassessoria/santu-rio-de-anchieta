<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    {include file="site/_header.tpl"}
    <script src="{$WWW_JS}jquery.validate.min.js"></script>
    <script>
        $().ready(function() {
            $("#formPedidoOracao").validate({
                rules: {
                    inputNome:     "required",
                    inputIntencao: "required",
                    inputEmail:
                    {
                        required: true,
                        email: true
                    }
                }
            });
        });

        var RecaptchaOptions = {
            theme : 'white',
            tabindex : 2
         };
    </script>
    <body>
        {include file="site/_topo.tpl"}
        <div id="wrapper-interna">
            <div class="container">
                <section id="artigo">
                    <div class="divArticleHeader">
                        <h2>Pedido de ora��o</h2>
                    </div>
                    <article>
                        <div id="divArticleContent" class="clearfix">
                            {if $sMensagemCadastro !== ""}
                                <div class="divMensagemCadastro">
                                    {$sMensagemCadastro}
                                </div>
                            {/if}

                            <div id="divPedidoOracao">
                                <form action="pedido-de-oracao" method="post" id="formPedidoOracao" class="formPadrao">
                                    <div id="divPedidoMensagem">
                                        <strong>
                                            N�s queremos rezar pelas suas inten��es!
                                        </strong>
                                    </div>
                                    <img id="santoPedido" src="{$WWW_IMG}interna/santo-pedido.jpg" />
                                    <div id="form-pedido">
                                        <ul>
                                            <li class="col100 noHeight">
                                                <label>Fa�a seu pedido de ora��o</label>
                                                <textarea type="text" name="inputIntencao" id="inputIntencao">{$smarty.post.inputIntencao|default:""}</textarea>
                                            </li>
                                        </ul>
                                        <ul>
                                            <li class="col100">
                                                <label>Nome</label>
                                                <input type="text" name="inputNome" id="inputNome" value="{$smarty.post.inputNome|default:""}" />
                                            </li>
                                        </ul>
                                        <ul>
                                            <li class="col100">
                                                <label>E-mail</label>
                                                <input type="text" name="inputEmail" id="inputEmail" value="{$smarty.post.inputEmail|default:""}" />
                                            </li>
                                        </ul>
                                        <ul>
                                            <li class="col100 noHeight checkbox">
                                                <label> Tornar meu pedido de ora��o p�blico. </label>
                                                <input {if isset($smarty.post.inputPublico)}checked{/if} type="checkbox" name="inputPublico" />
                                            </li>
                                        </ul>

                                        <ul>
                                            <li class="col100 noHeight">
                                                {$recaptcha}
                                                {if $bCaptchaCorreto == false}
                                                    <label class="error errorCaptcha">Captcha incorreto</label>
                                                    <script>
                                                        var p = $(".errorCaptcha").position();
                                                        $(window).scrollTop(p.top);
                                                    </script>
                                                {/if}
                                            </li>
                                        </ul>
                                    </div>
                                    <ul>
                                        <li class="col100">
                                            <button type="submit" name="inputSalvar">Enviar</button>
                                        </li>
                                    </ul>        
                                </form>
                            </div>
                        </div>
                    </article>
                </section>
                {include file="site/_lateral.tpl"}
            </div>
        </div>        
        {include file="site/_footer.tpl"}                  
    </body>
</html>   