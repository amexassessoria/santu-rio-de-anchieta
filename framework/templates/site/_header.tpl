<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <head>
        <title>{$sNomeSite}</title>
        <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1" />
        <meta http-equiv="content-language" content="pt-br" />
        <meta name="keywords" content="{$sNomeSite}" />
        <meta name="description" content="{$sNomeSite}" />
        <meta name="robots" content="index, follow"/>
        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="author" content="Amex Assessoria" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="copyright" content="Amex Assessoria" />
        <meta name="geo.region" content="GO"/>
        <meta name="geo.placename" content="Niquelândia" />
        <meta name="MSSmartTagsPreventParsing" content="true" />
        <meta http-equiv="imagetoolbar" content="no" />
        <meta http-equiv="content-script-type" content="text/javascript" />
        <meta http-equiv="content-style-type" content="text/css" />
        <base href="{$WWW}" />
        <meta property="fb:app_id" content="865104380211119" />
        
        {if isset($oArtigo)}
            <meta property="og:title" content="{htmlentities($oArtigo->sTitulo)}" />
            <meta property="og:url" content="{$WWW}{$oArtigo->sLink}" />
            <meta property="og:description" content="{htmlentities($oArtigo->sResumo)}" />
            <meta property="og:image" content="{$oArtigo->sUrlImagem}" />
        {else if isset($oAlbum)}
            <meta property="og:title" content="{htmlentities($oAlbum->sTitulo)}" />
            <meta property="og:url" content="{$WWW}fotos/{$oAlbum->iCodigo}" />
            <meta property="og:description" content="{htmlentities($oAlbum->sTitulo)}" />
            <meta property="og:image" content="{$WWW}{$oAlbum->arrObjFoto[0]->sUrl}" />
        {/if}

        <link rel="shortcut icon" href="{$WWW_IMG}favicon.png" type="image/x-icon" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="last-modified" content="{$lastModified}" /> 

        <!-- CSS -->
        <link rel="stylesheet" href="{$WWW_CSS}normalize.css" />
        <link rel="stylesheet" href="{$WWW_CSS}style.css" />
        <link rel="stylesheet" href="{$WWW_CSS}slicknav.css" />
        <link rel="stylesheet" href="{$WWW_CSS}style-responsive.css"/>
        <link rel="stylesheet" href="{$WWW_CSS}hover-min.css" />

        <!-- JQUERY -->
        <script src="{$WWW_JS}jquery.min.js"></script>
        
        <!-- FONT -->
        <link rel="stylesheet" href="fonts/styles.css" />
    </head>

