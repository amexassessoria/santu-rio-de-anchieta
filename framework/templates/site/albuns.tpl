<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    {include file="site/_header.tpl"}
    <body>
        {include file="site/_topo.tpl"}
        <div id="wrapper-interna">
            <div class="container">
                <section id="artigo">
                    <div class="divArticleHeader">
                        <h2>�lbuns</h2>
                    </div>
                    <article>
                        <section id="bloco-interna">
                            <ul class="ul-center">
                                {foreach $arrObjAlbum as $oAlbum}
                                    <li class="bloco album">
                                        <a href="fotos/{$oAlbum->iCodigo}" title="{$oAlbum->sTitulo}">
                                            <div style="background-image: url('{if ($oAlbum->iCodigo < 20)}{$oAlbum->sUrlCapa}{else}images/fotosAlbuns/{$oAlbum->sUrlCapa}{/if}');" class="thumbAlbumLista hvr-curl-bottom-right"></div>
                                            <div class="text-center">{$oAlbum->sTitulo|truncate:70}</div>
                                        </a>
                                    </li>
                                {/foreach}
                            </ul>
                        </section>
                        {include file="site/_paginacao.tpl"}
                    </article>
                </section>
                {include file="site/_lateral.tpl"}
            </div>
        </div>
        {include file="site/_footer.tpl"}
    </body>
</html>