{if $iTotalPaginas > 1}
    <div class="pagination" >
        <ul>
            <li><a href="{$sPaginaNome}/1{$sComplemento}" title="Primeira"><<</a></li>
            <li><a href="{$sPaginaNome}/{$iAnterior}{$sComplemento}" title="Anterior"><</a></li>
                {section name=paginacao loop=$iLoop start=$iStart step=1}
                    <li {if $smarty.section.paginacao.index == $iPaginaAtual}class="active"{/if} ><a href="{$sPaginaNome}/{$smarty.section.paginacao.index}{$sComplemento}">{$smarty.section.paginacao.index}</a></li>
                {/section}
            <li><a href="{$sPaginaNome}/{$iProxima}{$sComplemento}" title="Pr�xima">></a></li>
            <li><a href="{$sPaginaNome}/{$iTotalPaginas}{$sComplemento}" title="�ltima">>></a></li>
        </ul>
    </div>
{/if}