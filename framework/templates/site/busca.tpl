<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    {include file="site/_header.tpl"}
    <body>
        {include file="site/_topo.tpl"}
        <div id="wrapper-interna">
            <div class="container">
                <section id="artigo">
                    <div class="divArticleHeader">
                        {if $oArtigoTipo->sDescricao == ''}
                            {if isset($smarty.get.tag)}
                                <h1 id="h1Artigo">Categoria : <strong>{$oTag->sDescricao}</strong></h1>
                            {else}
                                <h1 id="h1Artigo">Busca : <strong>{$sTextoBusca}</strong></h1>
                            {/if}
                        {else}
                            <h1 id="h1Artigo">{$oArtigoTipo->sDescricaoPlural}</h1>
                        {/if}
                    </div>
                    <article>
                        <ul id="divArticleContent" class="clearfix">
                            {foreach $arrObjArtigo as $oArtigo}
                                <li class="divBusca">
                                    <a href="{$oArtigo->sLink}">
                                        <div class="hoverEffect ">
                                            <div style="background-image: url('{$oArtigo->sUrlImagem}');" class="thumbBusca"> </div>
                                        </div>
                                        <div class="divTexto">
                                            <h1>{$oArtigo->sDataCadastro|date_format:"%d"} de {Util::retornaMesExtenso($oArtigo->sDataCadastro|date_format:"%m")}, {$oArtigo->sDataCadastro|date_format:"%Y"}</h1>
                                            <h2>{$oArtigo->sTitulo}</h2>
                                            {if trim($oArtigo->sResumo) <> ''}
                                                <h3>{$oArtigo->sResumo}</h3>
                                            {/if}
                                        </div>
                                    </a>
                                </li>
                            {foreachelse}
                                Nenhum resultado encontrado.
                            {/foreach}
                        </ul>
                        {include file="site/_paginacao.tpl"}
                    </article>
                </section>
                {include file="site/_lateral.tpl"}
            </div>
        </div>    
        {include file="site/_footer.tpl"}                               
    </body>
</html>