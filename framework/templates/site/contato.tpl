<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

    {include file="site/_header.tpl"}

    <script src="{$WWW_JS}jquery.validate.min.js"></script>

    <script>

        var RecaptchaOptions = {

            theme: 'white',

            tabindex: 2

        };



        $().ready(function() {

            $("#formContato").validate({

                rules: {

                    inputNome: "required",

                    inputMensagem: "required",

                    inputEmail:

                    {

                        required: true,

                        email: true

                    }

                }

            });

        });

    </script>

    <body>

        {include file="site/_topo.tpl"}

        <div id="wrapper-interna">

            <div class="container">

                <section id="artigo">

                    <div class="divArticleHeader">
                        <h2 id="h1Artigo">Contato</h2>
                    </div>

                    <article>

                        <div id="divArticleContent" class="clearfix">

                            {if $sMensagemCadastro !== ""}

                                <div class="divMensagemCadastro">

                                    {$sMensagemCadastro}

                                </div>

                            {/if}

                            <div class="text-center">

                                <strong>Endere�o:</strong><br/>

                                Pra�a do Santu�rio, 29230-000<br/>
                                
                                Anchieta - ES - Brasil<br/>

                            </div>

                            <br/>

                            <div class="text-center">

                                <strong>Telefone:</strong><br/>

                                +55 (28) 3536-3985<br/>

                            </div>

                            <form action="contato" id="formContato" method="post" class="formPadrao">

                                <ul>

                                    <li class="col100">

                                        <h1>Formul�rio de Contato</h1>

                                    </li>

                                </ul>

                                <ul>

                                    <li class="col100">

                                        <label>Nome</label>

                                        <input type="text" name="inputNome" id="inputNome" value="{$smarty.post.inputNome|default:""}" class="txt" />

                                    </li>

                                </ul>

                                <ul>

                                    <li class="col50">

                                        <label>E-mail</label>

                                        <input type="text" name="inputEmail" id="inputEmail" value="{$smarty.post.inputEmail|default:""}" class="txt" />

                                    </li>

                                    <li class="col50">

                                        <label>Telefone</label>

                                        <input type="text" name="inputTelefone" id="inputTelefone" value="{$smarty.post.inputTelefone|default:""}" class="txt" />

                                    </li>

                                </ul>

                                <ul>

                                    <li class="col100 noHeight">

                                        <label>Mensagem</label>

                                        <textarea  type="text" name="inputMensagem" id="inputMensagem">{$smarty.post.inputMensagem|default:""}</textarea>

                                    </li>

                                </ul>

                                <ul>

                                    <li class="col100 noHeight">

                                        {$recaptcha}

                                        {if $bCaptchaCorreto == false}

                                        <label class="error errorCaptcha">Captcha incorreto</label>

                                        <script>

                                            var p = $(".errorCaptcha").position();

                                            $(window).scrollTop(p.top);

                                        </script>

                                        {/if}

                                    </li>

                                </ul>

                                <ul>

                                    <li class="col100 noHeight">

                                        <button type="submit" name="inputEnviar">Enviar</button>

                                    </li>

                                </ul>

                            </form>

                        </div>

                    </article>

                </section>

                {include file="site/_lateral.tpl"}

            </div>

        </div>   

        {include file="site/_footer.tpl"}                                    

    </body>

</html>