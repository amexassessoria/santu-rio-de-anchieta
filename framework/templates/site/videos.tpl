<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    {include file="site/_header.tpl"}
    <link rel="stylesheet" type="text/css" href="{$WWW_CSS}jquery.fancybox.css" media="all" />
    <body>
        {include file="site/_topo.tpl"}
        <div id="wrapper-interna">
            <div class="container">
                <section id="artigo">
                    <div class="divArticleHeader">
                        <h2>V�deos</h2>
                    </div>
                    <article>
                        <div id="divArticleContent" class="ul-center">
                            {foreach $arrObjVideo as $oVideo}
                                <div class="divVideo">
                                    <a class="aVideo" href="{$oVideo->sLink}?fs=1&amp;autoplay=1" title="{$oVideo->sDescricao}">
                                        <img src="http://img.youtube.com/vi/{$oVideo->sCodigoIdentificacao}/0.jpg" />
                                        <div class="text-center">{$oVideo->sDescricao}</div>
                                    </a>
                                </div>
                            {foreachelse}
                                <div>Nenhum resultado encontrado.</div>
                            {/foreach}
                            {include file="site/_paginacao.tpl"}
                        </div>
                    </article>
                </section>
                {include file="site/_lateral.tpl"}
            </div>
        </div>
        {include file="site/_footer.tpl"}
        <script type="text/javascript" src="{$WWW_JS}jquery.fancybox.pack.js"></script>
        <script type="text/javascript">
            $(".aVideo").mouseenter(function() {
                $(this).parent().find('img').addClass("transparentHover");
            });

            $(".aVideo").mouseleave(function() {
                $(this).parent().find('img').removeClass("transparentHover");
            });

            $(".aVideo").click(function() {
                $.fancybox({
                    'padding': 0,
                    'autoScale': false,
                    'transitionIn': 'none',
                    'transitionOut': 'none',
                    'title': this.title,
                    'width': 640,
                    'height': 385,
                    'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                    'type': 'swf',
                    'swf': {
                        'wmode': 'transparent',
                        'allowfullscreen': 'true'
                    }
                });

                return false;
            });
        </script>
    </body>
</html>