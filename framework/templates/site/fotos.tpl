<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    {include file="site/_header.tpl"}
    <link rel="stylesheet" type="text/css" href="{$WWW_CSS}jquery.fancybox.css" media="all" />
    <link href="{$WWW_CSS}jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{$WWW_CSS}jquery.bxslider.css" />
    <body>
        {include file="site/_topo.tpl"}
        <div id="wrapper-interna">
            <div class="container">
                <section id="artigo">
                    <div class="divArticleHeader">
                        <h2>�lbum - {$oAlbum->sTitulo}</h2>
                    </div>
                    <article>
                        <div id="divConteudoInterna">
                            <div id="divSocialsFooter" class="right">
                                <span class='st_facebook_large' displayText='Facebook'></span>
                                <span class='st_twitter_large' displayText='Tweet'></span>
                                <span class='st_googleplus_large' displayText='Google +'></span>
                                <span class='st_email_large' displayText='Email'></span>
                                <span class='st_fblike_large' displayText='Facebook Like'></span>
                            </div>
                            {if (trim($oAlbum->sCredito <> ''))}
                                <div id="divCreditos">
                                    <label>Fotos : <strong>{$oAlbum->sCredito}</strong></label>
                                </div>
                            {/if}
                            <div id="divArticleContent">
                                <ul class="galeriaDeFotos">
                                    {foreach $arrObjFoto as $oFoto}
                                        <li><img src="{if ($oAlbum->iCodigo < 20)}{$oFoto->sUrl}{else}images/fotosAlbuns/{$oFoto->sUrl}{/if}" title="{$oFoto->sCaption}" /></li>
                                    {/foreach}
                                </ul>
                                <div class="thumbsArtigoAlbum">
                                    {foreach $arrObjFoto as $oFoto}
                                        <a data-slide-index="{$oFoto@index}" href=""><div class="thumbAlbum" style="background-image: url('{if ($oAlbum->iCodigo < 20)}{$oFoto->sUrl}{else}images/fotosAlbuns/{$oFoto->sUrl}{/if}');"></div></a>
                                    {/foreach}
                                </div>
                            </div>
                            <div class="voltar-albuns">
                                <a href="albuns/1"><b><h5><< Voltar para �lbuns</h5></b></a>
                            </div>    
                        </div>
                    </article>
                </section>
                {include file="site/_lateral.tpl"}
            </div> 
        </div>     
        {include file="site/_footer.tpl"}  
        <script type="text/javascript">var switchTo5x=true;</script>
        <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
        <script type="text/javascript" src="{$WWW_JS}jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="{$WWW_JS}jquery.mCustomScrollbar.min.js"></script>
        <script src="{$WWW_JS}jquery.bxslider.min.js"></script>
        <script type="text/javascript">
        
            stLight.options({
                publisher: "7be91bcf-7d46-4d0d-a1f1-c6deb2c354d0", doNotHash: true, doNotCopy: false, hashAddressBar: false
            });

            $(document).ready(function () {
                $('.galeriaDeFotos').bxSlider({
                    mode: 'fade',
                    controls: true,
                    captions: true,
                    pagerCustom: '.thumbsArtigoAlbum'
                });

                $(".thumbsArtigoAlbum").mCustomScrollbar({
                    horizontalScroll:true,
                    scrollButtons:{
                        enable:false
                    },
                    theme:"dark-thin"
                });
            });
        </script>
    </body>
</html>