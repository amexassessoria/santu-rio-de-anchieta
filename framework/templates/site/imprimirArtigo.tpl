<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    {include file="site/_header.tpl"}    
    <link rel="stylesheet" href="{$WWW_CSS}report.css" type="text/css" media="screen" charset="UTF-8" />
    <link rel="stylesheet" type="text/css" href="{$WWW_CSS}reportPrint.css" media="print" />
</head>

<body>
    <div class="boxFiltro">
        <br/>
        <a class="btn btn-primary" href='javascript:window.print();'>
            <i class="icon-print icon-white"></i> Imprimir
        </a>
    </div>
    <br/>
    <div class="boxBody">
        <div class="boxTop" style="float:left">
            <img src="{$WWW_IMG}topo/logo-site.png" alt="{$sNomeSite}" title="{$sNomeSite}" style="max-width:180px;max-height:60px;"
            />
        </div>
        <div class="boxTop cabecalho" style="float:left;width:550px;margin-left:20px;">
        </div>
        <div class="boxTop left" style="float:left;width:50px;height:80px">
            <span></span>
            <p class="tright">{$smarty.now|date_format:"d/m/Y"}</p>
        </div>
        <br class="clear" />
        <div style="width:100%;float:left">
            <section id="artigo">

                <div class="divArticleHeader">
                    <h1 id="h1Artigo">
                        {if ($oArtigo->oArtigoTipo->iCodigo == 3)}Santu�rio{else}{$oArtigo->oArtigoTipo->sDescricao}{/if} {if ($oArtigo->oArtigoTipo->iCodigo
                        <> 4) && ($oArtigo->oArtigoTipo->iCodigo
                            <> 5)} {if ($oArtigo->oArtigoTipo->iCodigo
                                <> 3)} > {$oArtigo->sDataCadastro|date_format:"%d"} de {Util::retornaMesExtenso($oArtigo->sDataCadastro|date_format:"%m")},
                                    {$oArtigo->sDataCadastro|date_format:"%Y"} {/if} {else}
                                    <span id="spanArticleData">{$oArtigo->sDia} de {$oArtigo->sMesExtenso} {if $oArtigo->sAno
                                        <> NULL}- Ano {$oArtigo->sAno}{/if}</span>
                                    {/if}
                    </h1>
                    <h2>{$oArtigo->sTitulo}</h2>
                </div>

                <!-- Content -->
                <article>
                    <div id="divArticleResumo">
                        {if trim($oArtigo->sResumo)
                        <> ''} {$oArtigo->sResumo} {/if}
                    </div>

                    <div id="divArticleContent">
                        <div id="divConteudoInterna">
                            {if (($oArtigo->oArtigoTipo->iCodigo
                            <> 4) && ($oArtigo->oArtigoTipo->iCodigo
                                <> 5))} {if ($oArtigo->sUrlImagem
                                    <> '')} {if $oArtigo->sOcultarMiniatura
                                        <> 'S'}
                                            <img class="imgFloat" src="{$oArtigo->sUrlImagem}" /> {/if} {/if} {/if} {$oArtigo->sConteudo} {if trim($oArtigo->sCredito)
                                            <> ''}
                                                <div id="divArticleCredito">
                                                    <strong>Por:</strong> {$oArtigo->sCredito}
                                                </div>
                                                {/if} {if trim($oArtigo->sFonteDescricao)
                                                <> ''}
                                                    <br/>
                                                    <i>Fonte:
                                                        <a href="{$oArtigo->sFonteUrl}" target="_blank">{$oArtigo->sFonteDescricao}</a>
                                                    </i>
                                                    {/if}

                                                    <br class="clear" /> {if $oArtigo->oAlbum->iCodigo
                                                    <> ''}
                                                        <div id="divNomeAlbumArtigo">{$oArtigo->oAlbum->sTitulo}</div>
                                                        <ul class="galeriaDeFotos">
                                                            {foreach $oArtigo->oAlbum->arrObjFoto as $oFoto}
                                                            <li>
                                                                <img src="images/fotosAlbuns/{$oFoto->sUrl}" title="{$oFoto->sCaption}"
                                                                />
                                                            </li>
                                                            {/foreach}
                                                        </ul>

                                                        <div class="thumbsArtigoAlbum">
                                                            {foreach $oArtigo->oAlbum->arrObjFoto as $oFoto}
                                                            <a data-slide-index="{$oFoto@index}" href="">
                                                                <div class="thumbAlbum" style="background-image: url('images/fotosAlbuns/{$oFoto->sUrl}');"></div>
                                                            </a>
                                                            {/foreach}
                                                        </div>

                                                        <div id="divCreditos" class="text-center">
                                                            {if (trim($oArtigo->oAlbum->sCredito
                                                            <> ''))}
                                                                <label>Fotos :
                                                                    <strong>{$oArtigo->oAlbum->sCredito}</strong>
                                                                </label>{/if}
                                                        </div>
                                                        {/if}

                                                        <br /> {if ($oArtigo->oArtigoTipo->iCodigo == 4)}
                                                        <div id="divFonteEvangelho">
                                                            Fonte:
                                                            <a href="http://www.paulinas.org.br/">Portal Paulinas</a>
                                                        </div>
                                                        <br/> {/if} {if ($arrObjSanto
                                                        <> '') && (count($arrObjSanto) > 0)}
                                                            <h3>Outros Santos do dia {$oArtigo->sDia}/{$oArtigo->sMes}</h3>
                                                            {foreach $arrObjSanto as $oSanto}
                                                            <a href="{$oSanto->sLink}" title="">{$oSanto->sTitulo}</a>
                                                            <br /> {/foreach} {/if}

                                                            <div id="divNavArtigo">
                                                                {if ($oArtigo->oArtigoTipo->iCodigo == 4)}
                                                                <div class="left">
                                                                    <a href="{$oEvangelhoAnterior->sLink}&{$oEvangelhoAnterior->sAno}&{$oEvangelhoAnterior->sMes}&{$oEvangelhoAnterior->sDia}"
                                                                        title="{$oEvangelhoAnterior->sTitulo}">
                                                                        << Anterior</a>
                                                                </div>
                                                                <div class="right">
                                                                    <a href="{$oEvangelhoProximo->sLink}&{$oEvangelhoProximo->sAno}&{$oEvangelhoProximo->sMes}&{$oEvangelhoProximo->sDia}" title="{$oEvangelhoProximo->sTitulo}">Pr�ximo >></a>
                                                                </div>
                                                                {/if} {if ($oArtigo->oArtigoTipo->iCodigo == 5)}
                                                                <div class="left">
                                                                    <a href="{$oSantoAnterior->sLink}" title="{$oSantoAnterior->sTitulo}">Anterior</a>
                                                                </div>

                                                                <div class="right">
                                                                    <a href="{$oSantoProximo->sLink}" title="{$oSantoProximo->sTitulo}">Pr�ximo</a>
                                                                </div>
                                                                {/if}

                                                            </div>
                                                            <br />
                        </div>

                        <form action="{$WWW}imprimirArtigo" method="POST" style="float: right">
                            <input type="hidden" value="{$oArtigo->iCodigo}" name="codigo" />
                            <button type="submit">
                                <i class="fa fa-print"></i>Imprimir</button>
                        </form>

                        <div id="divSocialsFooter">
                            <span class='st_facebook_large' displayText='Facebook'></span>
                            <span class='st_twitter_large' displayText='Tweet'></span>
                            <span class='st_googleplus_large' displayText='Google +'></span>
                            <span class='st_email_large' displayText='Email'></span>
                            <span class='st_fblike_large' displayText='Facebook Like'></span>
                        </div>

                        <br class="clear" />



                        <div id="disqus_thread"></div>

                        <div id="disqus_thread"></div>
                        <script type="text/javascript">

                            /* * * CONFIGURATION VARIABLES * * */
                            var disqus_shortname = 'santuario-nacional-sao-jose-de-anchieta';

                            /* * * DON'T EDIT BELOW THIS LINE * * */
                            (function () {
                                var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                            })();
                        </script>
                        <noscript>Please enable JavaScript to view the
                            <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a>
                        </noscript>
                    </div>
                </article>
            </section>
        </div>
        <br class="clear" />
    </div>

    <script type="text/javascript" src="{$WWW_JS}jquery.maskedinput.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $("#inputDataInicial").mask("99/99/9999");
                $("#inputDataFinal").mask("99/99/9999");
            });
        });
            //$('#inputDataInicial').datepicker();
            //$('#inputDataFinal').datepicker();
    </script>
</body>

</html>