<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    {include file="site/_header.tpl"}
    <script src="{$WWW_JS}jquery.validate.min.js"></script>
    <script>
        var RecaptchaOptions = {
            theme: 'white',
            tabindex: 2
        };

        $().ready(function() {
            $("#formTestemunho").validate({
                rules: {
                    inputNome: "required",
                    inputCidade: "required",
                    inputTestemunho: "required",
                    inputEmail:
                    {
                        required: true,
                        email: true
                    }
                }
            });
        });
    </script>
    <body>
        {include file="site/_topo.tpl"}
        <div id="wrapper-interna">
            <div class="container">
                <section id="artigo">
                    <div class="divArticleHeader">
                        <h2>Testemunho de F�</h2>
                    </div>
                    <article>
                        <div id="divArticleContent">
                            {if $sMensagemCadastro !== ""}
                                <div class="divMensagemCadastro">
                                    {$sMensagemCadastro}
                                </div>
                            {/if}
                            <div id="divEncontrarTestemunho" class="clearfix">
                                <div class="deixar-testemunho">
                                    <img src="{$WWW_IMG}interna/testemunho.png" alt="Deixar meu testemunho!" />
                                    <button id="modalNovoTestemunho" >Deixar meu testemunho!</button>
                                </div>
                                <div class="informacoes-testemunho">
                                    <div class="total">
                                        <h1>Total de testemunhos : </h1>
                                        <h2>{$iTotalTestemunho}</h2>
                                    </div>
                                    <div class="form-buscar-testemunho">
                                        <h1>Encontrar meu testemunho (entre com seu e-mail)</h1>
                                        <form action="testemunho/{$iPaginaAtual}" method="post">
                                            <input type="text" name="inputEmailBusca" value="{$smarty.post.inputEmailBusca|default:""}" />
                                            <input type="submit" class="button" name="inputBuscarTestemunho" value="Buscar" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div id="novoTestemunho" class="hide" {if $bCaptchaCorreto == false}style ="display:block" {/if} />
                            <form action="testemunho/{$iPaginaAtual}" id="formTestemunho" method="post" class="formPadrao" enctype="multipart/form-data">
                                <ul>
                                    <li class="col100">
                                        <h1>Seu testemunho aparecer� na nossa p�gina ap�s ser aprovado! Preencha seus dados abaixo:</h1>
                                    </li>
                                </ul>
                                <ul>
                                    <li class="col50">
                                        <label>Nome</label>
                                        <input type="text" name="inputNome" id="inputNome" value="{$smarty.post.inputNome|default:""}" />
                                    </li>
                                    <li class="col50">
                                        <label>E-mail</label>
                                        <input type="text" name="inputEmail" id="inputEmail" value="{$smarty.post.inputEmail|default:""}" />
                                    </li>
                                </ul>
                                <ul>
                                    <li class="col75">
                                        <label>Cidade</label>
                                        <input type="text" name="inputCidade" id="inputCidade" value="{$smarty.post.inputCidade|default:""}" />
                                    </li>
                                    <li class="col25">
                                        {include file="site/_estados.tpl"}
                                    </li>
                                </ul>
                                <ul>
                                    <li class="col100 noHeight">
                                        <label>Testemunho</label>
                                        <textarea type="text" name="inputTestemunho" id="inputTestemunho">{$smarty.post.inputTestemunho|default:""}</textarea>
                                    </li>

                                </ul>
                                <ul>
                                    <li  class="col100 noHeight">
                                        <label>Sua foto</label>
                                        <input type="file" name="inputFotoTestemunho" accept="image/x-png, image/gif, image/jpeg" />
                                    </li>
                                </ul>
                                <ul>
                                    <li class="col100 checkbox noHeight">
                                        <label> Desejo tornar meu testemunho p�blico. </label>
                                        <input type="checkbox" {if isset($smarty.post.inputPublico)}checked{/if} name="inputPublico" />
                                    </li>
                                </ul>
                                <ul>
                                    <li class="col100 noHeight">
                                        {$recaptcha}
                                        {if $bCaptchaCorreto == false}
                                        <label class="error errorCaptcha">Captcha incorreto</label>
                                        <script>
                                            var p = $(".errorCaptcha").position();
                                            $(window).scrollTop(p.top);
                                        </script>
                                        {/if}
                                    </li>
                                </ul>
                                <ul>
                                    <li class="col50 noHeight">
                                        <button type="submit" name="inputSalvar">Salvar</button>
                                    </li>
                                    <li class="col50 noHeight">
                                        <button type=button id="btnCancelarTestemunho">Cancelar</button>
                                    </li>
                                </ul>
                            </form>
                        </div>
                    </article>
                    <div id="lista-testemunho">
                        <ul>
                            {foreach $arrObjTestemunho as $oPedido}
                                <li class="bloco-testemunho">
                                    <div class="header-testemunho">
                                        {if ($oPedido->sImagemUrl <> '') && ($oPedido->sImagemUrl <> NULL)}
                                            <img src="{$WWW_IMG}testemunho/{$oPedido->sImagemUrl}" alt="{$oPedido->sNome}" />
                                        {else}
                                            <img src="{$WWW_IMG}interna/thumb-testemunho.jpg" alt="{$oPedido->sNome}" />
                                        {/if}
                                        <h1>{$oPedido->sNome}</h1>
                                        <h2>{$oPedido->sCidade}-{$oPedido->sEstado}</h2>
                                    </div>
                                    <div class="content-testemunho">
                                        {$oPedido->sIntencao}
                                    </div>
                                </li>
                            {foreachelse}
                                <li>Nenhum testemunho encontrado.</li>
                            {/foreach}
                        </ul>
                        {include file="site/_paginacao.tpl"}
                    </div>
                </section>
                {include file="site/_lateral.tpl"}
            </div>
        </div>
        {include file="site/_footer.tpl"}       
        <script type="text/javascript">

            $('#modalNovoTestemunho').click(function() {
                $('#divEncontrarTestemunho').hide();
                $('#novoTestemunho').show("slow");
            });

            $('#btnCancelarTestemunho').click(function() {
                $('#novoTestemunho').hide();
                $('#divEncontrarTestemunho').show("slow");
            });

        </script>

    </body>

</html>