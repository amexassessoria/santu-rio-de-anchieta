<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    {include file="site/_header.tpl"}
    <script src="{$WWW_JS}jquery.validate.min.js"></script>
    <body>
        {include file="site/_topo.tpl"}
        <div id="wrapper-interna">
            <div class="container">
                <section id="artigo">
                    <div class="divArticleHeader">
                        <h2 id="h1Artigo">Liturgia di�ria</h2>
                    </div>
                    <article>
                        <div id="divArticleContent" class="clearfix">
                            {utf8_decode($sConteudo)}
                            <div id="divFonteEvangelho">
                                Fonte: <a href="http://liturgiadiaria.cnbb.org.br/">CNBB</a>
                            </div>
                        </div>
                    </article>
                </section>
                {include file="site/_lateral.tpl"}
            </div>
        </div>   
        {include file="site/_footer.tpl"}                                    
    </body>
</html>