<aside>
    <ul>
        {foreach $arrObjUltimasNoticias as $oUltimaNoticia}
            <li class="noticia">
                <a href="{$oUltimaNoticia->sLink}">
                    <div class="thumb" style="background-image: url('{$oUltimaNoticia->sUrlImagem}')"></div>
                    <div class="texto">{$oUltimaNoticia->sTitulo}</div>
                </a>
            </li>
        {/foreach}
    </ul>
    <div class="banner-campanha">
        <a href="/campanha">
            <img src="images/interna/banner-campanha.jpg" />
        </a>
    </div>
</aside>

