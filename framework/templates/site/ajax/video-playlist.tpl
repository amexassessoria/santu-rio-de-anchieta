<iframe id="video-iframe" src="http://www.youtube.com/embed/{$arrObjVideos->items[0]->snippet->resourceId->videoId}?rel=0&autoplay=0" ></iframe>
<ul>
    {foreach $arrObjVideos->items as $oVideo}
        <li  class="video-link {if $oVideo@index == 0}active{/if}" data-link="http://www.youtube.com/embed/{$oVideo->snippet->resourceId->videoId}?rel=0&autoplay=1">
            <div class="thumb" style="background-image: url('{$oVideo->snippet->thumbnails->default->url}')"></div>
        </li>
    {/foreach}
</ul>
<script type="text/javascript">
    $(".video-link").click(function () { 
        $(".video-link").removeClass("active");
        $(this).addClass("active");
        $("#video-iframe").attr("src", $(this).data("link"));
    });
</script>