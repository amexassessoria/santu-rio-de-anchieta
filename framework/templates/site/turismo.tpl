<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    {include file="site/_header.tpl"}
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.css"/>
    <body>
        {include file="site/_topo.tpl"}
        <div id="wrapper-interna" class="wrapper-turismo">
            <div class="container">
                <ul class="circuito active">
                    <li>
                        <a href="#" class="hvr-grow-rotate o-que-fazer-click">
                            <img src="images/turismo/o-que-fazer.png"/>
                            <h1>O que fazer?</h1>
                        </a>
                    </li>
                    <li>
                        <a href="artigo/historia-e-perfil-de-anchieta.html" target="_blank" class="hvr-grow-rotate">
                            <img src="images/turismo/historia.png"/>
                            <h1>Hist�ria</h1>
                        </a>
                    </li>
                    <li>
                        <a href="artigo/institucional.html" target="_blank" class="hvr-grow-rotate">
                            <img src="images/turismo/cidade.png"/>
                            <h1>Cidade</h1>
                        </a>
                    </li>
                </ul>
                <div class="o-que-fazer">
                    <h1>Turismo</h1>
                    <div class="circuitos">
                        <div class="voltar">
                            <img src="images/turismo/o-que-fazer-grande.png"/>
                            <h2>O que fazer?</h2>
                            <a href="#" class="o-que-fazer-voltar">Voltar</a>
                        </div>
                        <ul>
                            <li>
                                <div class="thumb" style="background-image:url('images/turismo/agroturismo.jpg')">
                                    <a href="artigo/circuitos-do-agroturismo.html">+ Leia Mais</a>
                                </div>
                                <div class="texto">CIRCUITOS DO AGROTURISMO</div>
                            </li>
                            <li>
                                <div class="thumb" style="background-image:url('images/turismo/cultura.jpg')">
                                    <a href="artigo/circuito-cultura-e-fe.html">+ Leia Mais</a>
                                </div>
                                <div class="texto">CIRCUITO CULTURA E F�</div>
                            </li>
                            <li>
                                <div class="thumb" style="background-image:url('images/turismo/ruinas.jpg')">
                                    <a href="artigo/circuito-rio-benevente-e-ruinas.html">+ Leia Mais</a>
                                </div>
                                <div class="texto">CIRCUITO RIO BENEVENTE E RU�NAS</div>
                            </li>
                            <li>
                                <div class="thumb" style="background-image:url('images/turismo/praias.jpg')">
                                    <a href="artigo/circuito-praias-nautico-recreativo-e-cientifico.html">+ Leia Mais</a>
                                </div>
                                <div class="texto">CIRCUITO PRAIAS, N�UTICO RECREATIVO E CIENT�FICO</div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>  
        {include file="site/_footer.tpl"} 
        <script type="text/javascript" src="js/owl.carousel.min.js"></script>
        <script type="text/javascript">

            var slider = $('.circuitos ul');

            slider.owlCarousel({
                loop: true,
                nav: true,
                autoplay: true,
                dots: false,
                margin: 0,
                items: 1,
                smartSpeed: 500
            });

            $(".o-que-fazer-click").click(function(e) {
                e.preventDefault();
                $(".circuito").removeClass("active");
                $(".o-que-fazer").addClass("active");
            });

            $(".o-que-fazer-voltar").click(function(e) {
                e.preventDefault();
                $(".o-que-fazer").removeClass("active");
                $(".circuito").addClass("active");
            });

        </script>
    </body>
</html>