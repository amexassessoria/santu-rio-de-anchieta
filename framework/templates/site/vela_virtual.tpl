<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    {include file="site/_header.tpl"}
    <script src="{$WWW_JS}jquery.validate.min.js"></script>
    <script>
        var RecaptchaOptions = {
            theme: 'white',
            tabindex: 2
        };

        $().ready(function() {
            $("#formVelaVirtual").validate({
                rules: {
                    inputNome: "required",
                    inputCidade: "required",
                    inputIntencao: "required",
                    inputEmail:
                    {
                        required: true,
                        email: true
                    }
                }
            });
        });
    </script>
    <link rel="stylesheet" type="text/css" href="{$WWW_CSS}jquery.fancybox.css" media="all" />
    <body>
        {include file="site/_topo.tpl"}
        <div id="wrapper-interna">
            <div class="container">
                <section id="artigo">
                    <div class="divArticleHeader">
                        <h2>Vela virtual</h2>
                    </div>
                    <article>
                        <div id="divArticleContent">
                            {if $sMensagemCadastro !== ""}
                                <div class="divMensagemCadastro">
                                    {$sMensagemCadastro}
                                </div>
                            {/if}
                            <div id="divTopoVela">
                                <img class="imgVelaApagada" src="{$WWW_IMG}velas/vela.gif" alt="Acenda uma vela!!!" />
                                <div class="conteudo">
                                    <div id="acender-vela">
                                        <ul>
                                            <li>
                                                <button id="modalNovaVela">Acender minha vela!</button>
                                                <div>Sua vela ficar� acesa por 7 dias e ir� aparecer ap�s ser aprovada!</div>
                                            </li>
                                            <li>
                                                <div>Total de velas acesas:</div>
                                                <strong>{$iTotalVela}</strong>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="encontrar-vela">
                                        <ul>
                                            <li>
                                                Encontrar minha vela (entre com seu e-mail)
                                            </li>
                                            <li>
                                                <form action="velavirtual/{$iPaginaAtual}" method="post">
                                                    <input type="text" name="inputEmailBusca" value="{$smarty.post.inputEmailBusca|default:""}" />
                                                    <input type="submit" name="inputBuscarVela" value="Buscar" />
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div id="novaVela" class="hide" {if $bCaptchaCorreto == false}style ="display:block" {/if}>
                                <form action="velavirtual/{$iPaginaAtual}" id="formVelaVirtual" method="post" class="formPadrao">
                                    <ul>
                                        <li class="col100">
                                            <h1>Preencha seus dados abaixo:</h1>
                                        </li>
                                    </ul>
                                    <ul>
                                        <li class="col50">
                                            <label for="inputNome">Nome</label>
                                            <input type="text" maxlength="25" name="inputNome" id="inputNome" value="{$smarty.post.inputNome|default:""}" />
                                        </li>
                                        <li class="col50">
                                            <label for="inputEmail">E-mail</label>
                                            <input type="text" name="inputEmail" id="inputEmail" value="{$smarty.post.inputEmail|default:""}" />
                                        </li>
                                    </ul>
                                    <ul>
                                        <li class="col75">
                                            <label for="inputCidade">Cidade</label>
                                            <input type="text" maxlength="27" name="inputCidade" id="inputCidade" value="{$smarty.post.inputCidade|default:""}" />
                                        </li>
                                        <li class="col25">
                                            {include file="site/_estados.tpl"}
                                        </li>
                                    </ul>
                                    <ul>
                                        <li class="col100 noHeight">
                                            <label for="inputIntencao">Inten��o</label>
                                            <textarea type="text" maxlength="350" name="inputIntencao" id="inputIntencao">{$smarty.post.inputIntencao|default:""}</textarea>
                                        </li>
                                    </ul>
                                    <ul>    
                                        <li class="col100 noHeight checkbox">
                                            <label for="inputExibirIntencao"> Exibir inten��o </label>
                                            <input {if isset($smarty.post.inputExibirIntencao)}checked{/if} type="checkbox" name="inputExibirIntencao" id="inputExibirIntencao"/>
                                        </li>
                                    </ul>
                                    <ul>
                                        <li class="col100 noHeight">
                                            {$recaptcha}
                                            {if $bCaptchaCorreto == false}
                                            <label class="error errorCaptcha">Captcha incorreto</label>
                                            <script>
                                                var p = $(".errorCaptcha").position();
                                                $(window).scrollTop(p.top);
                                            </script>
                                            {/if}
                                        </li>
                                    </ul>
                                    <ul>
                                        <li class="col50 noHeight">
                                            <button type="submit" name="inputAcender">Acender</button>
                                        </li>
                                        <li class="col50 noHeight">
                                            <button type=button id="btnCancelarAcenderVela">Cancelar</button>
                                        </li>
                                    </ul>
                                </form>
                            </div>
                        </div>
                    </article>
                    <div id="velas-acesas">
                        <ul>
                            {foreach $arrObjVela as $oPedido}
                                <li class="bloco-vela">
                                    <div class="intencao" id="{$oPedido->iCodigo}">
                                        <h1>Inten��o</h1>
                                        <div>{$oPedido->sIntencao}</div>
                                    </div>
                                    <ul class="vela-acesa">
                                        <li>
                                            <img src="{$WWW_IMG}velas/vela-dia{$oPedido->iDias}.gif" />
                                        </li>
                                        <li>
                                            <strong>{$oPedido->sNome}</strong>
                                            <div>{$oPedido->sCidade}-{$oPedido->sEstado}</div>
                                            {if ($oPedido->sExibirIntencao == 'S') && ($oPedido->sExibirIntencao <> "")}<button class="botao-show-intencao" data-id-botao="{$oPedido->iCodigo}">Leia a inten��o{*{$oPedido->sIntencao}*}</button>{/if}
                                        </li>
                                    </ul>
                                </li>
                            {foreachelse}
                                <li>Nenhuma vela acesa.</li>
                            {/foreach}
                        </ul>
                        {include file="site/_paginacao.tpl"}
                    </div>
                </section>
                {include file="site/_lateral.tpl"}
            </div>
        </div>

        {include file="site/_footer.tpl"}                                     

        <script type="text/javascript">
            $(function(){
                $('#modalNovaVela').click(function() {
                    $('#divTopoVela').hide();
                    $('#novaVela').show("slow");
                });

                $('#btnCancelarAcenderVela').click(function() {
                    $('#novaVela').hide();
                    $('#divTopoVela').show("slow");
                    $('html, body').animate({
                        scrollTop: $("#h1Artigo").offset().top
                    }, 500);
                });

                $('.botao-show-intencao').mouseenter(function() {
                    var idIntencao = $(this).data("id-botao");
                    $(".intencao#"+idIntencao).addClass("active");
                });

                $('.intencao').mouseleave(function() { 
                    $(".intencao.active").removeClass("active");
                });
            });
        </script>
    </body>
</html>