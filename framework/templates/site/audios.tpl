<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    {include file="site/_header.tpl"}
    <body>
        {include file="site/_topo.tpl"}
        <div id="wrapper-interna">
            <div class="container">
                <section id="artigo">
                    <article>
                        <h1 id="h1Artigo">Podcasts</h1>
                        <div id="divArticleContent">
                            {foreach $arrObjAudio as $oAudio}
                                <div class="divAudio">
                                    <div>{$oAudio->sDescricao}</div>
                                    <audio controls=controls preload="none">
                                        <source src="sounds/{$oAudio->sUrl|rawurlencode}" type="audio/mpeg" />
                                        Seu navegador n�o suporta o reprodutor de �udio.
                                    </audio>
                                </div>
                            {foreachelse}
                                <div>Nenhum resultado encontrado.</div>
                            {/foreach}
                            {include file="site/_paginacao.tpl"}
                        </div>
                    </article>
                </section>
                {include file="site/_lateral.tpl"}
            </div>
        </div>
        {include file="site/_footer.tpl"}
    </body>
</html>