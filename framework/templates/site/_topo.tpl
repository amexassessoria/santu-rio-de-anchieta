<header>
    <div class="jesuitas">
        <div class="container">
            <a href="http://jesuitasbrasil.com" target="_blank">
                <img src="images/topo/logo-jesuitas.png"/>
            </a>
            <span>"QUEM QUISER REFORMAR O MUNDO COMECE POR SI MESMO"</span>
        </div>
    </div>
    <div class="topo">
        <div class="container">
            <div class="bloco">
                <a href="#">
                    <img class="logo-site" src="images/topo/logo-site.png"/>
                    <img class="nome-site" src="images/topo/nome-site.png"/>
                </a>
            </div>
            <div class="bloco">
                {*<ul class="social">
                    <li>
                        <a href="#" class="hvr-pop">
                            <img src="images/topo/instagram.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="hvr-pop">
                            <img src="images/topo/google.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/santuariodeanchieta" class="hvr-pop" target="_blank">
                            <img src="images/topo/facebook.png"/>
                        </a>
                    </li>
                </ul>*}  
                <div class="idiomas">
                    <ul>
                        <li><a href="#" onclick="doGTranslate('pt|pt');return false;"><img src="images/topo/brasil.png"/></a></li>
                        <li><a href="#" onclick="doGTranslate('pt|en');return false;"><img src="images/topo/estados-unidos.png"/></a></li>
                        <li><a href="#" onclick="doGTranslate('pt|it');return false;"><img src="images/topo/italia.png"/></a></li>
                        <li><a href="#" onclick="doGTranslate('pt|es');return false;"><img src="images/topo/espanha.png"/></a></li>
                    </ul>
                    {literal}
                        <!-- Tradutor -->
                        <style type="text/css">
                            #goog-gt-tt {display:none !important;}
                            .goog-te-banner-frame {display:none !important;}
                            .goog-te-menu-value:hover {text-decoration:none !important;}
                            body {top:0 !important;}
                            #google_translate_element2 {display:none!important;}
                        </style>
                        <div id="google_translate_element2"><div class="skiptranslate goog-te-gadget" dir="ltr"><div id=":0.targetLanguage"><select class="goog-te-combo"><option value="af">Afric�ner</option><option value="sq">Alban�s</option><option value="de">Alem�o</option><option value="am">Am�rico</option><option value="ar">�rabe</option><option value="hy">Arm�nio</option><option value="az">Azerbaijano</option><option value="eu">Basco</option><option value="bn">Bengali</option><option value="be">Bielo-russo</option><option value="my">Birman�s</option><option value="bs">B�snio</option><option value="bg">B�lgaro</option><option value="kn">Canar�s</option><option value="ca">Catal�o</option><option value="kk">Cazaque</option><option value="ceb">Cebuano</option><option value="ny">Chicheua</option><option value="zh-CN">Chin�s (simplificado)</option><option value="zh-TW">Chin�s (tradicional)</option><option value="sn">Chona</option><option value="si">Cingal�s</option><option value="ko">Coreano</option><option value="co">Corso</option><option value="ht">Crioulo haitiano</option><option value="hr">Croata</option><option value="ku">Curdo</option><option value="da">Dinamarqu�s</option><option value="sk">Eslovaco</option><option value="sl">Esloveno</option><option value="es">Espanhol</option><option value="eo">Esperanto</option><option value="et">Estoniano</option><option value="tl">Filipino</option><option value="fi">Finland�s</option><option value="fr">Franc�s</option><option value="fy">Fr�sio</option><option value="gd">Ga�lico escoc�s</option><option value="gl">Galego</option><option value="cy">Gal�s</option><option value="ka">Georgiano</option><option value="el">Grego</option><option value="gu">Guzerate</option><option value="ha">Hau��</option><option value="haw">Havaiano</option><option value="iw">Hebraico</option><option value="hi">Hindi</option><option value="hmn">Hmong</option><option value="nl">Holand�s</option><option value="hu">H�ngaro</option><option value="ig">Igbo</option><option value="yi">I�diche</option><option value="id">Indon�sio</option><option value="en">Ingl�s</option><option value="yo">Ioruba</option><option value="ga">Irland�s</option><option value="is">Island�s</option><option value="it">Italiano</option><option value="ja">Japon�s</option><option value="jw">Javan�s</option><option value="km">Khmer</option><option value="lo">Laosiano</option><option value="la">Latim</option><option value="lv">Let�o</option><option value="lt">Lituano</option><option value="lb">Luxemburgu�s</option><option value="mk">Maced�nio</option><option value="ml">Malaiala</option><option value="ms">Malaio</option><option value="mg">Malgaxe</option><option value="mt">Malt�s</option><option value="mi">Maori</option><option value="mr">Marata</option><option value="mn">Mongol</option><option value="ne">Nepal�s</option><option value="no">Noruegu�s</option><option value="ps">Pachto</option><option value="fa">Persa</option><option value="pl">Polon�s</option><option value="pt">Portugu�s</option><option value="pa">Punjabi</option><option value="ky">Quirguiz</option><option value="ro">Romeno</option><option value="ru">Russo</option><option value="sm">Samoano</option><option value="sr">S�rvio</option><option value="st">Sessoto</option><option value="sd">Sindi</option><option value="so">Somali</option><option value="sw">Sua�le</option><option value="sv">Sueco</option><option value="su">Sundan�s</option><option value="tg">Tadjique</option><option value="th">Tailand�s</option><option value="ta">T�mil</option><option value="cs">Tcheco</option><option value="te">Telugo</option><option value="tr">Turco</option><option value="uk">Ucraniano</option><option value="ur">Urdu</option><option value="uz">Uzbeque</option><option value="vi">Vietnamita</option><option value="xh">Xhosa</option><option value="zu">Zulu</option></select></div>Powered by <span style="white-space:nowrap"><a class="goog-logo-link" href="https://translate.google.com" target="_blank"><img src="https://www.gstatic.com/images/branding/googlelogo/1x/googlelogo_color_42x16dp.png" width="37px" height="14px" style="padding-right: 3px" alt="Google Tradutor">Tradutor</a></span></div></div>
                        <script type="text/javascript">
                        function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'pt',autoDisplay: false}, 'google_translate_element2');}
                        </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>
                        <script type="text/javascript">
                            function GTranslateFireEvent(element,event){try{if(document.createEventObject){var evt=document.createEventObject();element.fireEvent('on'+event,evt)}else{var evt=document.createEvent('HTMLEvents');evt.initEvent(event,true,true);element.dispatchEvent(evt)}}catch(e){}}function doGTranslate(lang_pair){if(lang_pair.value)lang_pair=lang_pair.value;if(lang_pair=='')return;var lang=lang_pair.split('|')[1];var teCombo;var sel=document.getElementsByTagName('select');for(var i=0;i<sel.length;i++)if(sel[i].className=='goog-te-combo')teCombo=sel[i];if(document.getElementById('google_translate_element2')==null||document.getElementById('google_translate_element2').innerHTML.length==0||teCombo.length==0||teCombo.innerHTML.length==0){setTimeout(function(){doGTranslate(lang_pair)},500)}else{teCombo.value=lang;GTranslateFireEvent(teCombo,'change');GTranslateFireEvent(teCombo,'change')}}
                        </script>
                    {/literal}
                </div>
                <div class="busca" id="abre-busca">
                    <a href="#">
                        <img src="images/topo/lupa-grande.png"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <nav id="nav">
            <ul id="ulNav" >
                {foreach $arrObjMenu as $oMenu1}
                    {if ($oMenu1->arrObjMenu->count()) > 0}
                    <li>
                        <span>{$oMenu1->sDescricao}</span>
                        <ul>
                            {foreach $oMenu1->arrObjMenu as $oMenu2}
                                {if ($oMenu2->arrObjMenu->count()) > 0}
                                    <li>
                                        <span>{$oMenu2->sDescricao}</span>
                                        <ul>
                                            {foreach $oMenu2->arrObjMenu as $oMenu3}
                                                <li>
                                                    <a href="{$oMenu3->sLink}">{$oMenu3->sDescricao}</a>
                                                </li>
                                            {/foreach}
                                        </ul>
                                    </li>
                                {else}
                                    <li>
                                        <a href="{$oMenu2->sLink}">{$oMenu2->sDescricao}</a>
                                    </li>
                                {/if}
                            {/foreach}
                        </ul>
                    </li>
                    {else}
                        <li>
                            <a href="{$oMenu1->sLink}">{$oMenu1->sDescricao}</a>
                        </li>
                    {/if}
                {/foreach} 
            </ul>
        </nav>
    </div>
</header>
<div id="divBusca">          
    <div class="fechar" id="fecha-busca">
        <a href="#">
            <img src="images/topo/fechar.png"/>
        </a>
    </div>                                                   
    <div class="caixa-busca">
        <input id="inputBusca" value="{if (isset($smarty.get.s))}{utf8_decode($smarty.get.s)}{/if}" type="search" placeholder="Buscar..." name="inputBuscar"/>
        <div class="lupa">
            <a href="#">
                <img src="images/topo/lupa-grande.png"/>
            </a>
        </div>
    </div>
</div>