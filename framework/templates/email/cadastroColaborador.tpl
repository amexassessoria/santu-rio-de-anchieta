<table border="0" width="600" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff">
	<tbody>
		<tr style="height: 13px;">
			<td style="height: 13px;" colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td style="height: 51px;" width="30">&nbsp;</td>
			<td style="font-family: Arial; color: #000000; font-size: 15px; text-align: center; height: 51px;" width="540">
				<p><b>Novo Colaborador Cadastrado! Segue os dados:</b></p>
				<p>
					<ul>
						<li>
							<b>Nome: </b> {$oColaborador->sNome}
						</li>
						<li>
							<b>Endere�o: </b> {$oColaborador->oLogradouro->sDescricao} {$oColaborador->sEndereco} N� {$oColaborador->iNumero}
							{$oColaborador->sComplemento} - {$oColaborador->sBairro} - {$oColaborador->sCidade} - {$oColaborador->sEstado}
						</li>
						<li>
							<b>CEP: </b> {$oColaborador->sCEP}
						</li>
						<li>
							<b>Data de nascimento: </b> {$oColaborador->sDataNascimento|date_format:"%d/%m/%Y"}
						</li>
						<li>
							<b>Telefone: </b> {$oColaborador->sTelefone}
						</li>
						<li>
							<b>E-mail: </b> {$oColaborador->sEmail}
						</li>
						<li>
							<b>CPF: </b> {$oColaborador->sCPF}
						</li>
						<li>
							<b>Sexo: </b> {$oColaborador->sSexo}
						</li>
					</ul>
				</p>
			</td>
			<td style="height: 51px;" width="30">&nbsp;</td>
		</tr>
		<tr style="height: 13px;">
			<td style="height: 13px;" colspan="3">&nbsp;</td>
		</tr>
		<tr style="height: 34px;">
			<td style="height: 34px;" width="30">&nbsp;</td>
			<td style="font-family: Arial; color: #000000; font-size: 15px; text-align: center; height: 34px;" width="540">
				{$sNomeSite}
			</td>
			<td style="height: 34px;" width="30">&nbsp;</td>
		</tr>
	</tbody>
</table>