<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        {include file="centraldecontrole/_header.tpl"}
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.4&appId=865104380211119";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    </head>
    <body>
        {include file="centraldecontrole/_menu.tpl"}
        
        <br/><br/>
        <div class="container">
            {if $smarty.session.PERMISSOES.ADICIONAR_ARTIGO == 'S'}
                <div class="page-header" style="text-align:center">
                    {foreach $arrObjArtigoTipo as $oArtigoTipo}
                        {if ($oArtigoTipo->iCodigo <> 4) && ($oArtigoTipo->iCodigo <> 5)}
                            <a class="btn btn-primary" href="?pagina=centraldecontrole/publicar_artigo&tipo={$oArtigoTipo->iCodigo}">
                                <i class="icon-plus-sign icon-white"></i> {$oArtigoTipo->sDescricao}
                            </a>
                        {/if}
                    {/foreach}
                </div>
            {else}
                <br/>
            {/if}
            <form class="form-inline" action="?pagina=centraldecontrole/inicial" method="post">
                <div class="form-group">
                    <label class="sr-only">Descri��o</label>
                    <input type="text" style="width:250px;" name="inputDescricao" id="inputDescricao" value="{$sDescricao}" /> 
                    <label class="sr-only" for="inputTipo">Tipo</label>
                    <select name="inputTipo">
                        <option value="-" >Todos</option>
                        {foreach $arrObjArtigoTipo as $oArtigoTipo}
                            {if ($oArtigoTipo->iCodigo <> 4) && ($oArtigoTipo->iCodigo <> 5)}
                                <option value="{$oArtigoTipo->iCodigo}" {if $iTipo == $oArtigoTipo->iCodigo} selected {/if} >{$oArtigoTipo->sDescricao}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <label class="sr-only">Data</label>
                    <input type="text" style="width:80px;" name="inputDataInicial" id="inputDataInicial" value="{$sDataInicial}" /> �
                    <input type="text" style="width:80px;" name="inputDataFinal" id="inputDataFinal" value="{$sDataFinal}"/> 
                    <button type="submit" class="btn btn-default">Filtrar</button>
                </div>
                
            </form>
            
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>T�tulo</th>
                        <th>Autor</th>
                        <th>Data</th>
                        <th>Agendado para:</th>
                        <th>Tipo</th>
                        <th class="text-center">A��es</th>
                    </tr>
                </thead>
                <tbody>
                    {foreach $arrObjArtigo as $oArtigo}
                        <tr>
                            <td>
                                <a href="?pagina=centraldecontrole/publicar_artigo&codigo={$oArtigo->iCodigo}" title="Editar artigo">{$oArtigo->sTitulo}</a>
                            </td>
                            <td>
                                {$oArtigo->oUsuario->sNome}
                            </td>
                            <td>
                                {$oArtigo->sDataCadastro|date_format:"%d/%m/%Y"}
                            </td>
                            <td>
                                {if ($oArtigo->sAgenda !== null) && ($oArtigo->sAgenda !== "")}{$oArtigo->sAgenda|date_format:"%d/%m/%Y"}{else}-{/if}
                            </td>
                            <td>
                                {$oArtigo->oArtigoTipo->sDescricao}
                            </td>
                            <td class="text-center">
								<span class="fb-share-button" data-href="{$oArtigo->sLink}" data-layout="icon"  ></span>
                                {if $smarty.session.PERMISSOES.EXCLUIR_ARTIGO == 'S'}
                                    <a href="?pagina=centraldecontrole/excluir_artigo&codigo={$oArtigo->iCodigo}" title="Excluir"onclick="return confirmaExcluir();" >
                                        <img src="{$WWW_IMG}galeria/cross-circle.png" title="Excluir" style="width:19px;height:19px"/>
                                    </a>
                                {else}
                                    -
                                {/if}
                            </td>
                        </tr>
                    {foreachelse}
                        <tr>
                            <td colspan="5">
                                Nenhum resultado encontrado.
                            </td>
                        </tr>
                    {/foreach}
                </tbody>
            </table>
        </div>
        <script type="text/javascript" src="{$WWW_JS}jquery.maskedinput.js"></script>
        <script type="text/javascript" >
            $(document).ready(function(){  
                $(function(){  
                    $("#inputDataInicial").mask("99/99/9999");   
                    $("#inputDataFinal").mask("99/99/9999"); 
                });  
            });  
            
            function confirmaExcluir()
            {
                var btnConfirm = confirm ( 'Deseja realmente excluir esta Postagem?' );
                
                if ( btnConfirm )
                {
                    return true;
                }
                
                return false;
            }
            //$('#inputDataInicial').datepicker();
            //$('#inputDataFinal').datepicker();
        </script>
    </body>
</html>