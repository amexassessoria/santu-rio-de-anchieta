<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        {include file="centraldecontrole/_header.tpl"}
    </head>
    <body>
        {include file="centraldecontrole/_menu.tpl"}
        <br/><br/>
        <div class="container">
            <div class="page-header">
                <a class="btn btn-primary" href="?pagina=centraldecontrole/anexo&acao=editar">
                    <i class="icon-plus-sign icon-white"></i> Novo Anexo
                </a>
            </div>
            {if $sAcao == 'lista'}
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Anexo</th>
                            <th>Link</th>
                            <th class="text-center">Excluir</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $arrObjAnexo as $oAnexo}
                            <tr>
                                <td>
                                    {$oAnexo->sTitulo}
                                </td>
                                <td>
                                    <a href="{$WWW}{$oAnexo->sCaminho}" target="_blank">{$WWW}{$oAnexo->sCaminho}</a>
                                </td>
                                <td class="text-center">
                                    <a href="?pagina=centraldecontrole/anexo&excluir&codigo={$oAnexo->iCodigo}" onclick="return confirmaExcluir()" title="Excluir" >
                                        <img src="{$WWW_IMG}galeria/cross-circle.png" title="Excluir" style="width:19px;height:19px"/>
                                    </a>
                                </td>
                            </tr>
                        {foreachelse}
                            <tr>
                                <td colspan="4">
                                    Nenhum Anexo
                                </td>
                            </tr>

                        {/foreach}
                    </tbody>
                </table>
             {elseif $sAcao == 'editar'}
                <form action="?pagina=centraldecontrole/anexo&acao=lista" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="sr-only" for="inputTitulo">T�tulo</label>
                        <input type="text" name="inputTitulo" style="width:300px;"  value="{$oAnexo->sTitulo}" />
                        <br/><br/>
                        
                        <label class="sr-only" for="inputAnexo">Anexo</label>
                        <input type="file" name="inputAnexo"  style="width:300px;"  />
                        <br/><br/>
                        
                        <button type="submit" name="inputSalvar" class="btn btn-default">Salvar</button>
                        <a href="?pagina=centraldecontrole/anexo&acao=lista"  class="btn btn-danger">Cancelar</a>
                    </div>
                </form>
             {/if}
        </div>
        <script type="text/javascript">
            
            function confirmaExcluir()
            {
                    var btnConfirm = confirm ( 'Deseja realmente excluir este Anexo?' );
                
                if ( btnConfirm )
                {
                    return true;
                }
                
                return false;
            }
        </script>
    </body>
</html>