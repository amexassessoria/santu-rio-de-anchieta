{strip}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        {include file="centraldecontrole/_header.tpl"}
    </head>
    <body id="bodyCentraldeControle">
        <div class="container">
            {if ( $bRecuperarSenha == false )}
                <form class="form-signin" id="frmLogin" action="?pagina=centraldecontrole/login" method="post">
                    <img src="{$WWW_IMG}logo_amex.png" alt="Amex Assessoria" title="Amex Assessoria" />
                    <br/>
                    <label for="inputLogin">Usu�rio:</label>
                    <input type="text" id="inputLogin" name="inputLogin" style="width:280px" maxlength="90" title="Entre com o seu Usu�rio" value="{$smarty.post.inputLogin|default:""}"/>
                    <br class="clear"/>
                    <label for="inputSenha">Senha:</label>
                    <input type="password" id="inputSenha" name="inputSenha" style="width:280px" maxlength="12" title="Entre com sua senha" />
                    <br/>
                    <input type="submit" id="btnSend" name="btnSend" class="btn btn-large btn-primary" style="float:right" value="Acessar" />
                    <br/>
                    {if ($msgErro <> null)}
                        <br/><br/><br/>
                        <div class="alert alert-error">
                            {$msgErro}
                        </div>
                    {else}
                        <br class="clear"/>
                    {/if}
                </form>
                {* <br/>
                <div class="divCenter">
                    <a href="?pagina=centraldecontrole/login&recuperarSenha" title="Recuperar Senha">Esqueceu sua senha?</a>   
                </div> *}
            {else} 
                <form class="form-signin" id="frmRecuperarSenha" action="?pagina=centraldecontrole/login" method="post">
                    <br/>
                    <br/>
                    <span class="" style="font-size: 11px;" >Para recuperar a sua senha, digite seu e-mail cadastrado. A senha atual ser� enviada.</span>
                    <br/><br/>
                    <label for="inputEmail">E-mail:</label>
                    <input type="text" id="inputEmail" name="inputEmail"  title="Entre com o seu e-mail cadastrado." value="{$smarty.post.inputLogin|default:""}"/>
                    <br/>
                    <input type="submit" id="btnSend" name="btnSend" class="btn btn-large btn-primary" value="Enviar" />
                </form>
            {/if}
        </div>
        
    </body>
    <script type="text/javascript">
            document.getElementById('inputLogin').focus();
    </script>
</html>
{/strip}