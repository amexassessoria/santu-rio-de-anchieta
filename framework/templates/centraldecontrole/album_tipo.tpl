<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        {include file="centraldecontrole/_header.tpl"}
    </head>
    <body>
        {include file="centraldecontrole/_menu.tpl"}
        <br/><br/>
        <div class="container">
            <div class="page-header">
                <a class="btn btn-primary" href="?pagina=centraldecontrole/album_tipo&acao=editar">
                    <i class="icon-plus-sign icon-white"></i> Novo Tipo
                </a>
            </div>
            {if $sAcao == 'lista'}
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Tipo</th>
                            <th class="text-center">A��es</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $arrObjAlbumTipo as $oAlbumTipo}
                            <tr>
                                <td>
                                    <a href="?pagina=centraldecontrole/album_tipo&acao=editar&codigo={$oAlbumTipo->iCodigo}" title="Editar tipo">{$oAlbumTipo->sDescricao}</a>
                                </td>
                                <td class="text-center">
                                    <a href="?pagina=centraldecontrole/album_tipo&excluir&codigo={$oAlbumTipo->iCodigo}" onclick="return confirmaExcluir()" title="Excluir" >
                                        <img src="{$WWW_IMG}galeria/cross-circle.png" title="Excluir" style="width:19px;height:19px"/>
                                    </a>
                                </td>
                            </tr>
                        {foreachelse}
                            <tr>
                                <td colspan="2">
                                    Nenhum Tipo
                                </td>
                            </tr>

                        {/foreach}
                    </tbody>
                </table>
             {elseif $sAcao == 'editar'}
                <form action="?pagina=centraldecontrole/album_tipo&acao=lista" method="post" >
                    <div class="form-group">
                        <input name="inputCodigo" type="hidden" value="{$oAlbumTipo->iCodigo}" />
                        <label class="sr-only" for="inputDescricao">Descri��o</label>
                        <input name="inputDescricao"  type="text" value="{$oAlbumTipo->sDescricao}" />
                        
                        <br/><br/>
                        <button type="submit" name="inputSalvar" class="btn btn-default">Salvar</button>
                        <a href="?pagina=centraldecontrole/tag&acao=lista"  class="btn btn-danger">Cancelar</a>
                    </div>
                </form>
             {/if}
        </div>
        <script type="text/javascript">
            function confirmaExcluir()
            {
                var btnConfirm = confirm ( 'Deseja realmente excluir esta Tipo?' );
                
                if ( btnConfirm )
                {
                    return true;
                }
                
                return false;
            }
        </script>
    </body>
</html>