<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        {include file="centraldecontrole/_header.tpl"}
    </head>
    <body>
        {include file="centraldecontrole/_menu.tpl"}
        <br/><br/>
        <div class="container">
            <div class="page-header">
                <a class="btn btn-primary" href="?pagina=centraldecontrole/carrossel&acao=editar">
                    <i class="icon-plus-sign icon-white"></i> Novo Carrossel
                </a>
            </div>
            {if $sAcao == 'lista'}
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>T�tulo</th>
                            <th class="text-center">A��es</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $arrObjCarrossel as $oCarrossel}
                            <tr>
                                <td>
                                    <a href="?pagina=centraldecontrole/carrossel&acao=editar&codigo={$oCarrossel->iCodigo}" title="Editar carrossel">{$oCarrossel->sDescricao}</a>
                                </td>
                                <td class="text-center">
                                    <a href="?pagina=centraldecontrole/carrossel&excluir&codigo={$oCarrossel->iCodigo}" onclick="return confirmaExcluir()" title="Excluir" >
                                        <img src="{$WWW_IMG}galeria/cross-circle.png" title="Excluir" style="width:19px;height:19px"/>
                                    </a>
                                </td>
                            </tr>
                        {foreachelse}
                            <tr>
                                <td colspan="2">
                                    Nenhum Carrossel
                                </td>
                            </tr>

                        {/foreach}
                    </tbody>
                </table>
             {elseif $sAcao == 'editar'}
                <form action="?pagina=centraldecontrole/carrossel&acao=lista" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        
                        <input name="inputCodigo" type="hidden" value="{$oCarrossel->iCodigo}" />
                        <label class="sr-only" for="inputDescricao">Descri��o</label>
                        <input type="text" name="inputDescricao" style="width:300px;" maxlength="250"  value="{$oCarrossel->sDescricao}" />
                        <br/><br/>
                        
                        <label class="sr-only" for="inputImagemUrl">Imagem do Carrossel</label>
                        {if $oCarrossel->sImagemUrl == ''}
                            <input type="file" name="inputCarrossel"  accept="image/x-png, image/gif, image/jpeg" style="width:300px;" />
                        {else}
                             <input type="hidden" name="inputCarrosselHidden"  value="{$oCarrossel->sImagemUrl}" />
                             <img src="images/banner/{$oCarrossel->sImagemUrl}" style="width:300px;"/>
                             
                             <br/><br/>
                             <label class="sr-only" for="inputImagemUrl">Nova Imagem</label>
                             <input type="file" name="inputCarrossel"  accept="image/x-png, image/gif, image/jpeg" style="width:300px;" />
                        {/if}
                        <br/><br/>
                        
                        <label class="sr-only" for="inputLink">Link</label>
                        <input type="text" name="inputLink"  style="width:300px;" value="{$oCarrossel->sLink}" />          
                        <br/><br/>

                        <label class="sr-only" for="inputLinkExterno">Abrir link externamente?</label>
                        <select class="inputLinkExterno" name="inputLinkExterno" style="width:80px;">
                            <option value=1 {if $oCarrossel->bLinkExterno == 1}selected{/if}>Sim</option>
                            <option value=0 {if $oCarrossel->bLinkExterno == 0}selected{/if}>N�o</option>
                        </select>
                        <br/><br/>

                        <label>Data e hora de entrada</label>
                        <input type="text" class="data" name="inputEntradaData" id="inputEntradaData" maxlength="20" style="width:100px" value="{if ($oCarrossel->sEntrada !== null) && ($oCarrossel->sEntrada !== "")}{$oCarrossel->sEntrada|date_format:"%d/%m/%Y"}{/if}"/>
                        <input type="text" class="hora" name="inputEntradaHora" id="inputEntradaHora" maxlength="20" style="width:50px" value="{if ($oCarrossel->sEntrada !== null) && ($oCarrossel->sEntrada !== "")}{$oCarrossel->sEntrada|date_format:"H:i"}{/if}"/>
                        <br/><br/>

                        <label>Data e hora de sa�da</label>
                        <input type="text" class="data" name="inputExpiracaoData" id="inputExpiracaoData" maxlength="20" style="width:100px" value="{if ($oCarrossel->sExpiracao !== null) && ($oCarrossel->sExpiracao !== "")}{$oCarrossel->sExpiracao|date_format:"%d/%m/%Y"}{/if}"/>
                        <input type="text" class="hora" name="inputExpiracaoHora" id="inputExpiracaoHora" maxlength="20" style="width:50px" value="{if ($oCarrossel->sExpiracao !== null) && ($oCarrossel->sExpiracao !== "")}{$oCarrossel->sExpiracao|date_format:"H:i"}{/if}"/>
                        <br/><br/>

                        <button type="submit" name="inputSalvar" class="btn btn-default">Salvar</button>
                        <a href="?pagina=centraldecontrole/carrossel&acao=lista"  class="btn btn-danger">Cancelar</a>
                    </div>
                </form>
             {/if}
        </div>
        <script type="text/javascript" src="{$WWW_JS}jquery.maskedinput.js"></script>
        <script type="text/javascript">

            $(function(){  
                $(".data").mask("99/99/9999");   
                $(".hora").mask("99:99");  
            });  

            function confirmaExcluir()
            {
                var btnConfirm = confirm ( 'Deseja realmente excluir este Carrossel?' );
                
                if ( btnConfirm )
                {
                    return true;
                }
                
                return false;
            }
        </script>
    </body>
</html>