<!DOCTYPE html>
<html>
    <head>  
        {include file="centraldecontrole/_header.tpl"}
        <link href="{$WWW_CSS}galeria/960_12.css" rel="stylesheet" type="text/css">
        <link href="{$WWW_CSS}galeria/simple-lists.css" rel="stylesheet" type="text/css">
        <!-- <link href="{$WWW_CSS}galeria/reset.css" rel="stylesheet" type="text/css"> -->
        <link href="{$WWW_CSS}galeria/common.css" rel="stylesheet" type="text/css">
        <link href="{$WWW_CSS}galeria/standard.css" rel="stylesheet" type="text/css"> 
        <link href="{$WWW_CSS}galeria/form.css" rel="stylesheet" type="text/css" />
        <link href="{$WWW_CSS}galeria/simple-lists.css" rel="stylesheet" type="text/css" />
        <link href="{$WWW_CSS}galeria/block-lists.css" rel="stylesheet" type="text/css" />
        <!-- <link href="{$WWW_CSS}galeria/table.css" rel="stylesheet" type="text/css" /> -->
        <link href="{$WWW_CSS}galeria/admin.css" rel="stylesheet" type="text/css" />
        <link href="{$WWW_CSS}ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css" />

        <!-- Generic libs -->
        <script type="text/javascript" src="{$WWW_JS}galeria/html5.js"></script>
        <!-- <script type="text/javascript" src="{$WWW_JS}galeria/old-browsers.js"></script> -->

        <!-- Template core functions -->
        <script type="text/javascript" src="{$WWW_JS}galeria/common.js"></script>
        <script type="text/javascript" src="{$WWW_JS}galeria/jquery.tip.js"></script>
        <script type="text/javascript" src="{$WWW_JS}galeria/standard.js"></script> 
        <script src="{$WWW_JS}jquery-ui-1.10.3.custom.min.js"></script>

        <!-- UPLOAD -->
        <link href="{$WWW_CSS}galeria/uploadify.css" type="text/css" rel="stylesheet" />

<!-- <script type="text/javascript" src="{$WWW_JS}galeria/swfobject.js"></script>
<script type="text/javascript" src="{$WWW_JS}galeria/jquery.uploadify.v2.1.4.min.js"></script>
<script src="{$WWW_JS}galeria/jquery.scrollto.js" type="text/javascript"></script> -->
        <script type="text/javascript" src="{$WWW_JS}galeria/album.js"></script>

    </head>
    <body>	
        {include file="centraldecontrole/_menu.tpl"}
        <br/><br/>
        <div class="container">
            <p>&nbsp;</p>
            <div id="home" style="overflow-y:auto; ">
                {if $oAlbum->iCodigo <> ''}
                    <div class="box-album"> 
                        <a href="?pagina=centraldecontrole/album" class="btn btn-primary"> Voltar para �lbuns </a>
                        <br/><br/>
                        <ul class="box-album-head">
                            <p class="one-line-input grey-bg with-padding">
                                <span class="relative">
                                    <label for="{$oAlbum->iCodigo}">Nome do �lbum</label>
                                    <input type="text" name="album_name" style="float:left;margin-left:10px;width: 220px;" id="{$oAlbum->iCodigo}" class="album_name with-tip" title="Nome do �lbum" value="{$oAlbum->sTitulo}" />
                                    <label for="inputCredito" style="margin-left:15px;">Cr�ditos</label>
                                    <input type="text" name="inputCredito" style="float:left;margin-left:10px;width: 200px;" class="album_credito" title="Cr�ditos do �lbum" value="{$oAlbum->sCredito}" />      
                                    <label style="margin-left:15px;">Vis�vel</label>
                                    <select class="inputVisivel" name="inputVisivel" style="width:80px;float:left;margin-left:10px;">
                                        <option value="S" {if $oAlbum->sVisivel == 'S'}selected{/if}>Sim</option>
                                        <option value="N" {if $oAlbum->sVisivel == 'N'}selected{/if}>N�o</option>
                                    </select>
                                    <label style="margin-left:15px;">Tipo</label>
                                    <select class="album_tipo" name="inputTipo" style="width:200px;float:left;margin-left:10px;">
                                        <option value="-">-</option>
                                        {foreach $arrObjAlbumTipo as $oAlbumTipo}
                                            <option value="{$oAlbumTipo->iCodigo}" {if $oAlbum->oAlbumTipo->iCodigo == $oAlbumTipo->iCodigo}selected{/if}>{$oAlbumTipo->sDescricao}</option>
                                        {/foreach}
                                    </select>
                                    <button class="btn updateAlbum">Atualizar</button>
                                </span>					
                            </p>
                        </ul>
                        <span class="align-right btn-upload">
                            <form action="?pagina=centraldecontrole/upload_foto" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="inputAlbumCodigo" value="{$oAlbum->iCodigo}" />
                                <input name="uploads[]" type="file" multiple accept="image/x-png, image/gif, image/jpeg"  />
                                <input type="submit" name="submit" class="btn">
                            </form>
                        </span> 
                        <ul class="sortable" style="list-style-type: none; margin: 0; padding: 0;">
                            {foreach $oAlbum->arrObjFoto as $oFoto}
                                <li class="lisort" id="item_{$oFoto->iCodigo}" class="div_{$oFoto->iCodigo}">
                                    <ul class="box-foto-edit extended-list div_{$oFoto->iCodigo}">
                                        <li class="div_{$oFoto->iCodigo}">
                                            <ul class="mini-menu with-children-tip">
                                                <li><a href="javascript:void(0)" title="Atualizar"    id="{$oFoto->iCodigo}" album="{$oAlbum->iCodigo}" class="refresh"><img src="{$WWW_IMG}galeria/icons/refresh.png" width="16" height="16"></a></li>
                                                <li><a href="javascript:void(0)" title="Definir Capa" id="{$oFoto->iCodigo}" album="{$oAlbum->iCodigo}" class="cover"><img src="{$WWW_IMG}galeria/icons/photo.png" width="16" height="16"></a></li>
                                                <li><a href="javascript:void(0)" title="Remover"      id="{$oFoto->iCodigo}" class="delete"><img src="{$WWW_IMG}galeria/cross-circle.png" width="16" height="16"></a></li>
                                            </ul>   
                                            <img class="pic with-tip tip-bottom" title="Mover posi��o" src="{$WWW_IMG}fotosAlbuns/{$oFoto->sUrl}" style="width:174px;height:136px" />
                                            <input type="text" class="with-tip foto_caption" id="f_{$oFoto->iCodigo}"  value="{$oFoto->sCaption}" maxlength="74" title="T�tulo" style="margin-top:5px;" />
                                            <label>Link:</label><input type="text" value="{$WWW_IMG}fotosAlbuns/{$oFoto->sUrl}" title="Link" style="margin-top:-4px;" />
                                        </li>
                                    </ul>
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                {else}
                    <div class="box-album"> 
                        <form name="f" action="?pagina=centraldecontrole/album&create=true" method="post">
                            <ul class="box-album-head" style="margin-bottom: 20px;">
                                <p class="one-line-input grey-bg with-padding">
                                    <span class="relative">
                                        <label for="new">Nome do �lbum</label>
                                        <input type="text" name="inputAlbum" style="float:left;margin-left:10px;width: 250px"  id="new" class="album_name with-tip" title="Nome do �lbum" />
                                        <label for="inputCredito" style="margin-left:15px;">Cr�ditos</label>
                                        <input type="text" name="inputCredito" style="float:left;margin-left:10px;width: 200px;" title="Cr�ditos do �lbum" value="{$oAlbum->sCredito}" />   
                                        <label style="margin-left:15px;">Vis�vel</label>
                                        <select class="inputVisivel" name="inputVisivel" style="width:80px;float:left;margin-left:10px;">
                                            <option value="S" {if $oAlbum->sVisivel == 'S'}selected{/if}>Sim</option>
                                            <option value="N" {if $oAlbum->sVisivel == 'N'}selected{/if}>N�o</option>
                                        </select>
                                        <label style="margin-left:15px;">Tipo</label>
                                        <select class="album_tipo" name="inputTipo" style="width:200px;float:left;margin-left:10px;">
                                            <option value="-">-</option>
                                            {foreach $arrObjAlbumTipo as $oAlbumTipo}
                                                <option value="{$oAlbumTipo->iCodigo}" >{$oAlbumTipo->sDescricao}</option>
                                            {/foreach}
                                        </select>
                                        <button class="btn">Criar</button>
                                    </span>					
                                </p>
                            </ul>
                        </form>
                    </div>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>�lbum</th>
                                <th class="text-center">Vis�vel</th>
                                <th class="text-center">A��es</th>
                            </tr>
                        </thead>
                        <tbody>                  
                            {foreach $arrObjAlbum as $oAlbum}
                                <tr>
                                    <td> {$oAlbum->iCodigo} </td>
                                    <td> 
                                        <a class="" title="Editar �lbum" href="?pagina=centraldecontrole/album&edit={$oAlbum->iCodigo}">
                                            {$oAlbum->sTitulo} 
                                        </a> 
                                    </td>
                                    <td class="text-center"> {if ($oAlbum->sVisivel == 'S')} Sim {else} N�o {/if} </td>
                                    <td class="text-center"> 
                                        <a class="deleteAlbum" title="Remover �lbum"  id="{$oAlbum->iCodigo}" href="javascript:void(0)">
                                            <img src="{$WWW_IMG}galeria/cross-circle.png" style="width:19px;height:19px">
                                        </a> 
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>                                        
                    </table>
                {/if}
            </div>

        </div>                   
    </body>
</html>