<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        {include file="centraldecontrole/_header.tpl"}
    </head>
    <body>
        {include file="centraldecontrole/_menu.tpl"}
        <br/><br/>
        <div class="container">
            <form action="?pagina=centraldecontrole/aprovarPedidos" method="post" >
                <br/><br/>
                <div class="checkbox">
                    <label>
                        <input name="inputSelecionarTodos" id="inputSelecionarTodos" type="checkbox"/> Selecionar todos
                        <input type="submit" class="btn btn-primary" name="inputAprovar" id="inputAprovar" value="Aprovar selecionados"/>
                        <input type="submit" class="btn btn-danger" name="inputExcluir" id="inputExcluir" value="Excluir selecionados"/> 
                    </label>
                   
                </div>
                <br/>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>
                                
                            </th>
                            <th>
                                Nome(E-mail)
                            </th>
                            <th>
                                Inten��o
                            </th>
                            <th>
                                Foto
                            </th>
                            <th>
                                Cidade
                            </th>
                            <th>
                                Data
                            </th>
                            <th>
                                Tipo
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $arrObjPedido as $oPedido}
                            <tr>
                                <td><input type="checkbox" name="inputPedido[]" value="{$oPedido->iCodigo}"/></td>
                                <td>{$oPedido->sNome}({$oPedido->sEmail})</td>
                                <td>{$oPedido->sIntencao}<br/></td>
                                <td>
                                    {if ($oPedido->sImagemUrl <> '') && ($oPedido->sImagemUrl <> NULL)}
                                        <img src="{$WWW_IMG}testemunho/{$oPedido->sImagemUrl}" alt="{$oPedido->sNome}" style="max-width: 150px;" />
                                    {else}
                                        Sem foto
                                    {/if}
                                </td>
                                <td>{$oPedido->sCidade}-{$oPedido->sEstado}<br/></td> 
                                <td>{$oPedido->sData|date_format:"d/m/Y"}<br/></td>
                                <td>{if ($oPedido->sTipo == 'T')}Testemunho{elseif ($oPedido->sTipo == 'V')}Vela virtual{/if}<br/></td>
                            </tr>
                        {foreachelse}
                             <tr>
                                 <td colspan="7">Nenhum resultado encontrado.</td>
                            </tr>
                        {/foreach}
                    </tbody> 
                </table>
            </form>   
   
           
        </div>
        <script type="text/javascript">	       
            $("#inputSelecionarTodos").click(function () {
                if ($("#inputSelecionarTodos").is(':checked')) {
                    $(".table input[type=checkbox]").each(function () {
                        $(this).prop("checked", true);
                    });

                } else {
                    $(".table input[type=checkbox]").each(function () {
                        $(this).prop("checked", false);
                    });
                }
            });
            
            function confirmaExcluir()
            {
                var btnConfirm = confirm ( 'Deseja realmente excluir?' );
                
                if ( btnConfirm )
                {
                    return true;
                }
                
                return false;
            }
        </script>
    </body>
</html>