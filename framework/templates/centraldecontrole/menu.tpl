<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        {include file="centraldecontrole/_header.tpl"}
        <link href="{$WWW_CSS}ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css" />
        <script src="{$WWW_JS}jquery-ui-1.10.3.custom.min.js"></script>
        <style>
            #sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
            #sortable li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.0em; height: 18px; float:left;cursor:move }
            #sortable li span { position: absolute; margin-left: -1.3em; }
        </style>
    </head>
    <body>
        {include file="centraldecontrole/_menu.tpl"}
        <br/><br/>
        <div class="container">
            <div class="page-header">
                {if isset( $smarty.get.codigoMenuPai )}
                   {if $oMenuAnterior->iCodigo <> ''} 
                       Menu anterior : <a href="?pagina=centraldecontrole/menu&codigoMenuPai={$oMenuAnterior->iCodigo}">{$oMenuAnterior->sDescricao}</a>
                   {/if}
                   <h4>Menu pai : {$oMenuPai->sDescricao}</h4>
                {/if}
                <a class="btn btn-primary" href="?pagina=centraldecontrole/menu&acao=editar{if isset( $smarty.get.codigoMenuPai )}&codigoMenuPai={$smarty.get.codigoMenuPai}{/if}">
                    <i class="icon-plus-sign icon-white"></i> Novo Menu
                </a>
            </div>
            {if $sAcao == 'lista'}
                <ul id="sortable">
                {foreach $arrObjMenu as $oMenu}
                    <li class="ui-state-default" id="{$oMenu->iCodigo}"><span class="ui-icon ui-icon-arrowthick-2-e-w"></span>{$oMenu->sDescricao}</li>
                {foreachelse}
                    <li>Nenhum Menu</li>
                {/foreach}
                </ul>
                <br class="clear"/><br/>
                <a class="btn" id="btnSalvarOrdemMenu">
                    <i class=""></i> Salvar ordem do menu
                </a>
                <br/><br/>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Menu</th>
                            <th>Ativo</th>
                            <th class="text-center">A��es</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $arrObjMenu as $oMenu}
                            <tr>
                                <td>
                                    <a href="?pagina=centraldecontrole/menu&acao=editar&codigo={$oMenu->iCodigo}{if isset( $smarty.get.codigoMenuPai )}&codigoMenuPai={$smarty.get.codigoMenuPai}{/if}" title="Editar menu">{$oMenu->sDescricao}</a>
                                </td>
                                <td>
                                    {if ($oMenu->sAtivo) == 'S'}Sim{else}N�o{/if}
                                </td>
                                <td class="text-center">
                                    <a href="?pagina=centraldecontrole/menu&excluir&codigo={$oMenu->iCodigo}{if isset( $smarty.get.codigoMenuPai )}&codigoMenuPai={$smarty.get.codigoMenuPai}{/if}" onclick="return confirmaExcluir()" title="Excluir" >
                                        <img src="{$WWW_IMG}galeria/cross-circle.png" title="Excluir" style="width:19px;height:19px"/>
                                    </a>
                                </td>
                                <td>
                                    <a href="?pagina=centraldecontrole/menu&acao=lista&codigoMenuPai={$oMenu->iCodigo}" title="Submenus">Submenus</a>
                                </td>
                            </tr>
                        {foreachelse}
                            <tr>
                                <td colspan="2">
                                    Nenhum Menu
                                </td>
                            </tr>
                        {/foreach}
                    </tbody>
                </table>
             {elseif $sAcao == 'editar'}
                <form action="?pagina=centraldecontrole/menu&acao=lista{if isset( $smarty.get.codigoMenuPai )}&codigoMenuPai={$smarty.get.codigoMenuPai}{/if}" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <input name="inputCodigo" type="hidden" value="{$oMenu->iCodigo}" />
                        <label class="sr-only" for="inputDescricao">Descri��o</label>
                        <input type="text" name="inputDescricao" style="width:300px;"  value="{$oMenu->sDescricao}" />

                        <br/><br/>
                        <label class="sr-only" for="inputLink">Link</label>
                        <!-- <input type="file" name="inputMenu"  style="width:300px;"  /> -->
                        <input type="text" name="inputLink"  style="width:300px;" value="{$oMenu->sLink}" />
                        
                        <br/><br/>
                        <label class="sr-only" for="inputLink">Ativo</label>
                        <select class="inputAtivo" name="inputAtivo" style="width:80px;">
                            <option value="S" {if $oMenu->sAtivo == 'S'}selected{/if}>Sim</option>
                            <option value="N" {if $oMenu->sAtivo == 'N'}selected{/if}>N�o</option>
                        </select>
                        
                        <br/><br/>
                        <button type="submit" name="inputSalvar" class="btn btn-default">Salvar</button>
                        <a href="?pagina=centraldecontrole/menu&acao=lista{if isset( $smarty.get.codigoMenuPai )}&codigoMenuPai={$smarty.get.codigoMenuPai}{/if}"  class="btn btn-danger">Cancelar</a>
                    </div>
                </form>
             {/if}
        </div>
        <script type="text/javascript">
            function confirmaExcluir()
            {
                
                var btnConfirm = confirm ( 'Deseja realmente excluir este Menu?' );
                
                if ( btnConfirm )
                {
                    return true;
                }
                
                return false;
            }
            
            $('#btnSalvarOrdemMenu').click(function(){
            
                var arrPosicoesMenu = [];
                
                $( "#sortable li" ).each(function( index ) {
                    arrPosicoesMenu[index] = $(this).attr('id');
                });
                
                $.ajax({
                    type: "POST",
                    url: "?pagina=centraldecontrole/ajax",
                    data: { arrPosicoesMenu:arrPosicoesMenu, action:'salvarOrdemMenu' }
                })
                    .done(function( msg ) {
                    alert( "Salvo com sucesso!");
                });
            });
            
            $(function() {
                $( "#sortable" ).sortable();
                $( "#sortable" ).disableSelection();
            });
            
        </script>
    </body>
</html>