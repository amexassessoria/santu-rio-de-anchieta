<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        {include file="centraldecontrole/_header.tpl"}
    </head>
    <body>
        {include file="centraldecontrole/_menu.tpl"}
        <br/><br/>
        <div class="container">
            <div class="page-header">
                <a class="btn btn-primary" href="?pagina=centraldecontrole/tag&acao=editar">
                    <i class="icon-plus-sign icon-white"></i> Nova Tag
                </a>
            </div>
            {if $sAcao == 'lista'}
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Tag</th>
                            <th class="text-center">A��es</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $arrObjTag as $oTag}
                            <tr>
                                <td>
                                    <a href="?pagina=centraldecontrole/tag&acao=editar&codigo={$oTag->iCodigo}" title="Editar tag">{$oTag->sDescricao}</a>
                                </td>
                                <td class="text-center">
                                    <a href="?pagina=centraldecontrole/tag&excluir&codigo={$oTag->iCodigo}" onclick="return confirmaExcluir()" title="Excluir" >
                                        <img src="{$WWW_IMG}galeria/cross-circle.png" title="Excluir" style="width:19px;height:19px"/>
                                    </a>
                                </td>
                            </tr>
                        {foreachelse}
                            <tr>
                                <td colspan="2">
                                    Nenhuma Tag
                                </td>
                            </tr>

                        {/foreach}
                    </tbody>
                </table>
             {elseif $sAcao == 'editar'}
                <form action="?pagina=centraldecontrole/tag&acao=lista" method="post" >
                    <div class="form-group">
                        <input name="inputCodigo" type="hidden" value="{$oTag->iCodigo}" />
                        <label class="sr-only" for="inputDescricao">Descri��o</label>
                        <input name="inputDescricao"  type="text" value="{$oTag->sDescricao}" />
                        
                        <br/><br/>
                        <button type="submit" name="inputSalvar" class="btn btn-default">Salvar</button>
                        <a href="?pagina=centraldecontrole/tag&acao=lista"  class="btn btn-danger">Cancelar</a>
                    </div>
                </form>
             {/if}
        </div>
        <script type="text/javascript">
            function confirmaExcluir()
            {
                var btnConfirm = confirm ( 'Deseja realmente excluir esta Tag?' );
                
                if ( btnConfirm )
                {
                    return true;
                }
                
                return false;
            }
        </script>
    </body>
</html>