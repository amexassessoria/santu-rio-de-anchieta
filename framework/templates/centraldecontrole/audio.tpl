<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        {include file="centraldecontrole/_header.tpl"}
    </head>
    <body>
        {include file="centraldecontrole/_menu.tpl"}
        <br/><br/>
        <div class="container">
            <div class="page-header">
                <a class="btn btn-primary" href="?pagina=centraldecontrole/audio&acao=editar">
                    <i class="icon-plus-sign icon-white"></i> Novo �udio
                </a>
            </div>
            {if $sAcao == 'lista'}
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Audio</th>
                            <th class="text-center">A��es</th>
                        </tr>
                    </thead>
                    <tbody>
                        {foreach $arrObjAudio as $oAudio}
                            <tr>
                                <td>
                                    <a href="?pagina=centraldecontrole/audio&acao=editar&codigo={$oAudio->iCodigo}" title="Editar audio">{$oAudio->sDescricao}</a>
                                </td>
                                <td class="text-center">
                                    <a href="?pagina=centraldecontrole/audio&excluir&codigo={$oAudio->iCodigo}" onclick="return confirmaExcluir()" title="Excluir" >
                                        <img src="{$WWW_IMG}galeria/cross-circle.png" title="Excluir" style="width:19px;height:19px"/>
                                    </a>
                                </td>
                            </tr>
                        {foreachelse}
                            <tr>
                                <td colspan="2">
                                    Nenhum �udio
                                </td>
                            </tr>

                        {/foreach}
                    </tbody>
                </table>
             {elseif $sAcao == 'editar'}
                <form action="?pagina=centraldecontrole/audio&acao=lista" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <input name="inputCodigo" type="hidden" value="{$oAudio->iCodigo}" />
                        <label class="sr-only" for="inputDescricao">Descri��o</label>
                        <input type="text" name="inputDescricao" style="width:300px;"  value="{$oAudio->sDescricao}" />

                        <br/><br/>
                        <label class="sr-only" for="inputTipo">Tipo</label>
                        <select name="inputTipo">
                            <option value="-">-</option>
                            {foreach $arrObjAudioTipo as $oAudioTipo}
                                <option value="{$oAudioTipo->iCodigo}" {if ($oAudio->oAudioTipo !== null) && ($oAudio->oAudioTipo->iCodigo == $oAudioTipo->iCodigo)}selected{/if}>{$oAudioTipo->sDescricao}</option>
                            {/foreach}
                        </select>
                        
                        <br/><br/>
                        <label class="sr-only" for="inputUrl">�udio</label>
                        {if $oAudio->sUrl <> ''}
                            <input type="text" name="inputAudioText" readonly accept="audio/x-mpeg" value="{$oAudio->sUrl}"/>
                            <br/><br/>
                            <label class="sr-only" for="inputUrl">Alterar �udio</label>
                        {/if}
                        <input type="file" name="inputAudio" accept="audio/x-mpeg" style="width:300px;"  />
                        
                        <br/><br/>
                        <button type="submit" name="inputSalvar" class="btn btn-default">Salvar</button>
                        <a href="?pagina=centraldecontrole/audio&acao=lista"  class="btn btn-danger">Cancelar</a>
                    </div>
                </form>
             {/if}
        </div>
        <script type="text/javascript">
            function confirmaExcluir()
            {
                var btnConfirm = confirm ( 'Deseja realmente excluir este �udio?' );
                
                if ( btnConfirm )
                {
                    return true;
                }
                
                return false;
            }
        </script>
    </body>
</html>