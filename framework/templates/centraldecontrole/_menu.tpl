<!-- Menu fixo -->
<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a  href="?pagina=centraldecontrole/inicial" ><img style="max-height: 38px;float: left;margin-right: 10px;" src="{$WWW_IMG}topo/logo-site.png"></a>
            <div class="nav-collapse collapse">
                <p class="navbar-text pull-right">
                    Voc� est� logado como : <b>{$smarty.session.NOME}</b>
                </p>
                <ul class="nav">
                    <li class="dropdown {if ( $sMenu == 'layout' )} active {/if}">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Conte�do/Layout <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li {if $sPagina == 'postagens'}class="active"{/if}>
                                <a href="?pagina=centraldecontrole/inicial">Postagens</a>
                            </li>
                            {if $smarty.session.PERMISSOES.CADASTRAR_MENU == 'S'}
                                <li {if $sPagina == 'menu'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/menu&acao=lista">Menu</a>
                                </li>
                            {/if}
                            {if $smarty.session.PERMISSOES.CADASTRAR_CARROSSEL == 'S'}
                                <li {if $sPagina == 'carrossel'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/carrossel&acao=lista">Carrossel</a>
                                </li>
                            {/if}
                            {if $smarty.session.PERMISSOES.CADASTRAR_CARROSSEL == 'S'}
                                <li {if $sPagina == 'popup'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/popup&acao=lista">Pop-up</a>
                                </li>
                            {/if}
                            {if $smarty.session.PERMISSOES.CADASTRAR_TAG == 'S'}
                                <li {if $sPagina == 'tags'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/tag&acao=lista">Tags</a>
                                </li>
                            {/if} 
                        </ul>
                    </li>
                    <li class="dropdown {if ( $sMenu == 'multimidia' )} active {/if}">
                        <a href="#" class="dropdown-toggle " data-toggle="dropdown">Multim�dia <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            {if $smarty.session.PERMISSOES.CADASTRAR_FOTO == 'S'}
                                <li {if $sPagina == 'album_tipo'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/album_tipo">Tipos de �lbum</a>
                                </li>
                            {/if}
                            {if $smarty.session.PERMISSOES.CADASTRAR_FOTO == 'S'}
                                <li {if $sPagina == 'fotos'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/album">Fotos</a>
                                </li>
                            {/if}
                            {if $smarty.session.PERMISSOES.CADASTRAR_AUDIO == 'S'}
                                <li {if $sPagina == 'audio_tipo'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/audio_tipo">Tipos de �udio</a>
                                </li>
                            {/if}
                            {if $smarty.session.PERMISSOES.CADASTRAR_AUDIO == 'S'}
                                <li {if $sPagina == 'audios'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/audio">�udios</a>
                                </li>
                            {/if}
                            {if $smarty.session.PERMISSOES.CADASTRAR_VIDEO == 'S'}
                                <li {if $sPagina == 'videos'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/video">V�deos</a>
                                </li>
                            {/if}
                            <li {if $sPagina == 'anexos'}class="active"{/if}>
                                <a href="?pagina=centraldecontrole/anexo">Anexos</a>
                            </li>
                        </ul>
                    </li>
                    {if $smarty.session.PERMISSOES.ADMINISTRAR_ESPIRITUALIDADE == 'S'}
                        <li class="dropdown {if ( $sMenu == 'espiritualidade' )} active {/if}">
                            <a href="#" class="dropdown-toggle " data-toggle="dropdown">Espiritualidade<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li {if $sPagina == 'aprovarPedidos'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/aprovarPedidos">Aprovar Testemunhos e Vela Virtual</a>
                                </li>
                                {*<li {if $sPagina == 'responderPedidos'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/responderPedidos&acao=lista" >Responder pedidos de ora��o e testemunhos</a>
                                </li>*}
                                <li>
                                    <a href="?pagina=centraldecontrole/imprimirIntencoes" target="_blank">Imprimir pedidos de ora��o</a>
                                </li>
                                <li {if $sPagina == 'relatoriosPublicos'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/relatorioPedidoPublico" >Relat�rios de pedidos p�blicos</a>
                                </li>
                            </ul>
                        </li>
                    {/if}
                    {if $smarty.session.PERMISSOES.VISUALIZAR_RELATORIOS == 'S'}
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle " data-toggle="dropdown">Relat�rios<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="?pagina=centraldecontrole/relatorioColaborador" target="_blank">Colaboradores cadastrados</a>
                                </li>
                                <li>
                                    <a href="?pagina=centraldecontrole/imprimirNoticias" target="_blank">Not�cias</a>
                                </li>
                             </ul>
                        </li>
                    {/if}
                    <li class="dropdown {if ( $sMenu == 'configuracoes' )} active {/if}">
                        <a href="#" class="dropdown-toggle " data-toggle="dropdown">Configura��es<b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            {if $smarty.session.PERMISSOES.CADASTRAR_USUARIO == 'S'}
                                <li {if $sMenu == 'usuarios'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/usuario&acao=lista">Usu�rios</a>
                                </li>
                            {/if}
                            {if $smarty.session.CODIGO <> 1}
                                <li {if $sPagina == 'alterarSenha'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/alterarSenha">Alterar Senha</a>
                                </li>
                            {/if}
                            {if $smarty.session.PERMISSOES.CADASTRAR_CONFIGURACOES == 'S'}
                                <li {if $sPagina == 'boleto'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/boleto">Boleto</a>
                                </li>
                                <li {if $sPagina == 'parametros'}class="active"{/if}>
                                    <a href="?pagina=centraldecontrole/parametros">Par�metros</a>
                                </li>
                            {/if}
                        </ul>
                    </li>            
                    <li>
                        <a href="?pagina=centraldecontrole/logoff">Sair</a>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div>

{*<script src="{$WWW_JS}bootstrap/bootstrap-transition.js"></script> 
<script src="{$WWW_JS}bootstrap/bootstrap-alert.js"></script>
<script src="{$WWW_JS}bootstrap/bootstrap-modal.js"></script>
<script src="{$WWW_JS}bootstrap/bootstrap-dropdown.js"></script>
<script src="{$WWW_JS}bootstrap/bootstrap-scrollspy.js"></script>
<script src="{$WWW_JS}bootstrap/bootstrap-tab.js"></script>
<script src="{$WWW_JS}bootstrap/bootstrap-tooltip.js"></script> 
<script src="{$WWW_JS}bootstrap/bootstrap-popover.js"></script>
<script src="{$WWW_JS}bootstrap/bootstrap-button.js"></script>
<script src="{$WWW_JS}bootstrap/bootstrap-collapse.js"></script>
<script src="{$WWW_JS}bootstrap/bootstrap-carousel.js"></script>
<script src="{$WWW_JS}bootstrap/bootstrap-typeahead.js"></script>*}