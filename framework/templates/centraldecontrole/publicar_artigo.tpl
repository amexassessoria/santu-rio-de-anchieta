<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        {include file="centraldecontrole/_header.tpl"}
        <script src="{$WWW_JS}tinymce/tinymce.min.js"></script>
        <script src="{$WWW_JS}tinymce/jquery.tinymce.min.js"></script>
        <script src="{$WWW_JS}tinymce/langs/pt_BR.js"></script>
    </head>
    <body>
        {include file="centraldecontrole/_menu.tpl"}
        <br/><br/>
        <div class="container">
            <form action="?pagina=centraldecontrole/publicar_artigo{if isset($smarty.get.tipo)}&tipo={$smarty.get.tipo}{/if}" method="post" >
                <fieldset>
                    <legend>{$oArtigo->oArtigoTipo->sDescricao}</legend>
                    <input type="hidden" name="inputCodigo" value="{$oArtigo->iCodigo}" />
                    <input type="hidden" name="inputTipo"   value="{$oArtigo->oArtigoTipo->iCodigo}" />
                    <label>T�tulo</label>
                    <input type="text" name="inputTitulo" id="inputTitulo" maxlength="100" style="width:800px" value="{$oArtigo->sTitulo}"/>
                    <div id="divMensagemTitulo"></div>
                    {if $oArtigo->sLink <> ''}
                        <label>Link: {$oArtigo->sLink}</label>
                        <input type="hidden" value="{$oArtigo->sLink}" name="inputLink"/>
                    {/if}
                    <br/>
                    <label>Resumo</label>
                    <textarea name="inputResumo" maxlength="250" style="width:800px;height:50px;">{$oArtigo->sResumo}</textarea>
                    <br/>
                    <div class="form-inline">
                        {if ($oArtigo->oArtigoTipo->iCodigo == 4) || ($oArtigo->oArtigoTipo->iCodigo == 5)}
                            <label>Data referente</label>
                            <input type="text" id="dataDiaMes" name="inputData" maxlength="100" style="width:60px" value="{$oArtigo->sDia}/{$oArtigo->sMes}"/>
                        {/if}

                        {if ($oArtigo->oArtigoTipo->iCodigo == 4)}
                            <label style="margin-left:15px;" >Ano</label>
                            <select name="inputAno" style="width:60px;">
                                <option value="A" {if $oArtigo->sAno == 'A'}selected{/if} >A</option>
                                <option value="B" {if $oArtigo->sAno == 'B'}selected{/if}>B</option>
                                <option value="C" {if $oArtigo->sAno == 'C'}selected{/if}>C</option>
                            </select>
                        {/if}
                    </div>
                    <br/>
                    <textarea style="height:400px;width:800px" class="inputConteudo" name="inputConteudo">{$oArtigo->sConteudo}</textarea>
                    <br/>
                    
                    {if ($oArtigo->oArtigoTipo->iCodigo <> 4) && ($oArtigo->oArtigoTipo->iCodigo <> 5)}
                        <label>Miniatura</label> 
                        <div id="divMiniatura">
                            <img alt="Miniatura" style="max-height: 180px; max-width: 280px;" id="imgMiniatura" title="Minatura" src="{$oArtigo->sUrlImagem}" />
                        </div>
                        <br/>
                        <label>Url (link) da miniatura</label> 
                        <input type="text" id="inputMiniatura" name="inputMiniatura" style="width:800px" value="{$oArtigo->sUrlImagem}"/>
                    {/if}
                    <br/><br/>
                    
                    <label class="sr-only" for="inputAtivo">Ativo</label>
                    <select class="inputAtivo" name="inputAtivo" style="width:80px;">
                        <option value="S" {if $oArtigo->sAtivo == 'S'}selected{/if}>Sim</option>
                        <option value="N" {if $oArtigo->sAtivo == 'N'}selected{/if}>N�o</option>
                    </select>
                    <br/><br/>
                    
                    <input style="margin-left:10px;margin-bottom:5px" type="checkbox" name="inputOcultarMiniatura" {if $oArtigo->sOcultarMiniatura == 'S'}checked{/if}/> Ocultar miniatura da p�gina
                    <br/><br/>
                    
                    <input style="margin-left:10px;margin-bottom:5px" type="checkbox" name="inputDestaque" {if $oArtigo->sDestaque == 'S'}checked{/if}/> <b>Publicar no Portal Santu�rios do Brasil</b>
                    <br/><br/>
                    
                    <input style="margin-left:10px;margin-bottom:5px" type="checkbox" name="inputCarrossel" {if $oArtigo->sCarrossel == 'S'}checked{/if}/> Exibir no Carrossel
                    <br/><br/>
                    <br/>
                    
                    <label>Agendar postagem para</label>
                    <input type="text" name="inputAgenda" id="inputAgenda" maxlength="20" style="width:100px" value="{if ($oArtigo->sAgenda !== null) && ($oArtigo->sAgenda !== "")}{$oArtigo->sAgenda|date_format:"%d/%m/%Y"}{/if}"/>
                    <br/><br/>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#divTags">
                                    Tags
                                </a>
                            </h4>
                        </div>
                        <div id="divTags" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="form-group">
                                    {foreach $arrObjTag as $oTag}
                                <input type="checkbox" name="inputTag[]" value="{$oTag->iCodigo}" {foreach $oArtigo->arrObjArtigoTag as $oArtigoTag}{if $oArtigoTag->iCodigoTag == $oTag->iCodigo}checked{/if}{/foreach} style="margin-left:10px;margin-top:-3px;" /> {$oTag->sDescricao}
                                <br/>
                            {/foreach}
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/><br/>
                   
                    <label>�lbum</label>
                    <div class="form-inline">
                        <select name="inputAlbum" style="width:300px;">
                            <option value="-" >Nenhum</option>
                            {foreach $arrObjAlbum as $oAlbum}
                                <option {if $oArtigo->oAlbum->iCodigo == $oAlbum->iCodigo}selected{/if} value="{$oAlbum->iCodigo}" >{$oAlbum->sTitulo}</option>
                            {/foreach}
                        </select>
                    </div>
                    
                    <br/><br/>
                    <label>Cr�dito</label>
                    <input type="text" name="inputCredito" id="inputCredito" maxlength="250" style="width:500px" value="{$oArtigo->sCredito}"/>
                    
                    <br/><br/>
                    <label>Nome da Fonte</label>
                    <input type="text" name="inputFonteDescricao" id="inputFonteDescricao"  style="width:300px" value="{$oArtigo->sFonteDescricao}"/>
                    
                    <br/><br/>
                    <label>Link da Fonte</label>
                    <input type="text" name="inputFonteUrl" id="inputFonteUrl"  style="width:500px" value="{$oArtigo->sFonteUrl}"/>
                                  
                    <br/><br/>
                    {if $smarty.session.PERMISSOES.EDITAR_ARTIGO == 'S'}
                        
                        <div id="divMensagemTitulo" ></div>
                        
                        <input type="submit"  name="inputSalvarArtigo" class="btn btn-primary btnSalvar" value="Salvar"/>
                        {if !isset($smarty.get.codigo)}
                            <input type="submit" name="inputSalvarArtigo" class="btn btn-success btnSalvar" value="Salvar e Novo"/>
                        {/if}
                        <a href="?pagina=centraldecontrole/inicial"  class="btn btn-danger">Cancelar</a>
                    {/if}

                </fieldset>
            </form>
        </div>
        <script type="text/javascript" src="{$WWW_JS}jquery.maskedinput.js"></script>
        <script type="text/javascript">
            
            $(document).ready(function(){  

                tinymce.init({
                    selector: "textarea.inputConteudo",
                    plugins: [
                        "advlist autolink lists link image charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table contextmenu paste"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
                });

                $(function(){  
                    $("#dataDiaMes").mask("99/99");   
                    $("#inputAgenda").mask("99/99/9999");  
                });  
                
                {if !isset($smarty.get.codigo)}
                    $("#inputTitulo").keyup(function() {
                        return verificaLinkIgual();
                    });       
                {/if}
            });  

            $("#inputMiniatura").bind('paste',function( event ) {
                var $this = $(this);  
                setTimeout(function(){ 
                    $("#imgMiniatura").attr("src",$this.val());
                  },0); 
            });
            
            function verificaLinkIgual ()
            {
                
                $.post("?pagina=centraldecontrole/ajax", {
                     action:'verificaLinkIgual',
                     sTitulo:$("#inputTitulo").val()
                 }, function(data) {
                    if (data == true)
                    {
                        $('.btnSalvar').prop('disabled', false);   
                        $("#divMensagemTitulo").html('');   
                    }
                    else
                    {
                        $("#divMensagemTitulo").html('<strong style="color:red;">O t�tulo da not�cia j� existe, troque o t�tulo para salvar</strong><br/><br/>');   
                        $('.btnSalvar').prop('disabled', true);    
                    }
                    //$('#divMensagemTitulo').html(data);
                 }); 
            }
        </script>
    </body>
</html>