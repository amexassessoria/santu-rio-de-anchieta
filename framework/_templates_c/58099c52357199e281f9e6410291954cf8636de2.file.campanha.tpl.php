<?php /* Smarty version Smarty-3.0.8, created on 2017-08-04 09:07:03
         compiled from "framework/templates/site/campanha.tpl" */ ?>
<?php /*%%SmartyHeaderCode:54959846367031751-38999185%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '58099c52357199e281f9e6410291954cf8636de2' => 
    array (
      0 => 'framework/templates/site/campanha.tpl',
      1 => 1501782229,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '54959846367031751-38999185',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <?php $_template = new Smarty_Internal_Template("site/_header.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
    <script src="<?php echo $_smarty_tpl->getVariable('WWW_JS')->value;?>
jquery.validate.min.js"></script>
    <script src="<?php echo $_smarty_tpl->getVariable('WWW_JS')->value;?>
extensoes_validacao/cpf.js"></script>
    <script src="<?php echo $_smarty_tpl->getVariable('WWW_JS')->value;?>
extensoes_validacao/dateBR.js"></script>
    <script>
        
        var RecaptchaOptions = {
            theme : 'white',
            tabindex : 2
         };

        $().ready(function() {
            $("#formCadastroCampanha").validate({
                rules: {
                    inputNome:     "required",
                    inputCEP:      "required",
                    inputEndereco: "required",
                    inputNumero:   "required",
                    inputBairro:   "required",
                    inputCidade:   "required",
                    inputTelefone: "required",
                    inputEmail:
                    {
                        required: true,
                        email: true
                    },
                    inputCPF:
                    {
                        required: true,
                        cpf: true
                    },
                    inputDataNascimento:
                    {
                        required: true,
                        dateBR: true
                    }
                }
            });
        });
    </script>
    <link href="<?php echo $_smarty_tpl->getVariable('WWW_CSS')->value;?>
ui-lightness/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $_smarty_tpl->getVariable('WWW_CSS')->value;?>
jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo $_smarty_tpl->getVariable('WWW_JS')->value;?>
jquery-ui-1.10.3.custom.min.js"></script> 
    <body>
        <?php $_template = new Smarty_Internal_Template("site/_topo.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
        <div id="wrapper-interna">
            <section class="campanha">
                <div class="titulo-linha">
                    <h1>Campanha</h1>
                </div>
                <div class="container">
                    <div class="fundo-campanha">
                        <div class="titulo"></div>
                        <div class="arte"></div>
                    </div>
                </div>
                <div class="container">
                    <section id="artigo" class="contentCampanha">
                        <nav>
                            <ul id="menuCampanha">
                                <li class="hvr-pop active" id="liInicio" >
                                    <a>Campanha</a>
                                </li>
                                <li class="hvr-pop <?php if ($_smarty_tpl->getVariable('bErroCadastro')->value==true){?>active <?php }?>"  id="liCadastro" >
                                    <a>Cadastre-se</a>
                                </li>
                                <li class="hvr-pop  <?php if (isset($_GET['doar'])||($_smarty_tpl->getVariable('bCadastradoSucesso')->value==true)){?>active<?php }?>" id="liDoacao">
                                    <a>Fa�a sua doa��o</a>
                                </li>
                                <li class="hvr-pop  <?php if (isset($_GET['doar'])||($_smarty_tpl->getVariable('bCadastradoSucesso')->value==true)){?>active<?php }?>" id="liDoacaoComRegistro">
                                    <a>Doa��o Online</a>
                                </li>
                            </ul>
                        </nav>
                        <div id="divCampanha" class="aba active">
                            <article>
                                <div id="divArticleContent" class="clearfix texto-justificado">
                                    <p>A obra mission�ria confiada por Jesus a toda a Igreja pede dos crist�os uma postura ativa, com ora��es e atitudes que ajudem a propagar o Evangelho. Nesse sentido, a Campanha Amigos de Anchieta convida os admiradores e fi�is que frequentam o Santu�rio Nacional a se tornarem um associado, que ajude na condu��o de iniciativas e projetos do Templo.</p>
                                    <p>O interessado em se associar deve preencher a ficha de cadastro que se encontra na secretaria, tamb�m pelo n�mero (28) 3536-3985 ou pelo e-mail contato@santuariodeanchieta.com. Periodicamente, ser�o enviadas not�cias e uma carta do reitor. Pessoas de outros estados e pa�ses tamb�m podem contribuir.</p>
                                    <p>"Temos v�rios projetos, como por exemplo o restauro e a reforma do museu, o aparelhamento do centro de pesquisa, adapta��o da casa de retiros, constru��o de banheiros e de um local de refei��o para o peregrino. Outro desejo nosso � exibir uma missa televisionada ao menos uma vez por m�s, disponibilizando-a tamb�m na internet para uma visita virtual", explica o reitor do Santu�rio, padre C�sar Augusto dos Santos.</p>
                                    <p>O sacerdote garante ainda que quem n�o puder doar, pode contribuir com ora��es, s�plicas e participar de outros eventos. Atualmente, o Santu�rio Nacional de Anchieta oferece missas di�rias, al�m de uma terceira missa dominical, sacramento da confiss�o, exibi��o de cinema e outras atividades art�stico-culturais, como o coral das crian�as.</p>
                                    <p>Nas Escrituras Sagradas Jesus nos comunica essa responsabilidade com a Igreja quando diz: "Eis que vos envio para pregar o Evangelho". "Assim, convidamos as pessoas a se tornarem correspons�veis pelo Santu�rio. Precisamos de uma contribui��o mensal, pois temos encargos e contas todos os meses, mas a quantia � de acordo com o cora��o de quem contribui", esclarece padre C�sar.</p>
                                    <p>Alguns colaboradores se confundem ao imaginar que a colabora��o com o Santu�rio substitui o d�zimo. A contribui��o do dizimista ajuda no sustento e na manuten��o da igreja local. Como o Santu�rio n�o faz parte da par�quia Nossa Senhora da Assun��o, mas existe para servir a todo o Brasil na condi��o de Templo Nacional, sobrevive das ofertas de doa��es e de campanhas</p>
                                    <p>Em caso de d�vidas, o interessado pode ligar para a secretaria do Santu�rio pelo n�mero: (28) 3536-3985.</p>
                                    <p>&nbsp;</p>
                                    <p>&nbsp;</p>
                                    <h2>Sua empresa pode ajudar o santu�rio</h2>
                                    <p>&nbsp;</p>
                                    <p>Em caso de d�vidas, o interessado pode ligar para a secretaria do Santu�rio pelo n�mero: (28) 3536-3985. A sua empresa tamb�m pode contribuir com a miss�o do Santu�rio Nacional de Anchieta. Entre em contato e saiba como ajudar.</p>
                                    <p>(28) 3536-3985 | contato@santuariodeanchieta.com</p>
                                    <p>Este Santu�rio foi constru�do pelas m�os do primeiro Ap�stolo e agora Padroeiro do Brasil, ardoroso mission�rio em terras brasileiras. O Santu�rio � uma bela refer�ncia na Cidade de Anchieta, no Estado do Esp�rito Santo, e para todos que visitam e rezam sua vida e caminhada neste lugar sagrado, tendo como padroeiro S�o Jos� de Anchieta</p>
                                    <p>Desde j� agrade�o a sua aten��o e invoco as b�n��os de Deus sobre todos os seus empreendimentos para que o Senhor, por intercess�o de S�o Jos� de Anchieta, multiplique o que hoje voc� entrega de cora��o para divulga��o do Reino de Jesus Cristo entre n�s</p>
                                    <p>&nbsp;</p>
                                    <p><i>Pe. Cesar Augusto dos Santos, SJ <br/> Reitor do Santu�rio Nacional de Anchieta</i></p>
                                    <p>&nbsp;</p>
                                    <div class="text-center">
                                        <p><b>Empresas parceiras:</b></p>
                                        <p><a href="http://www.pontaldeubu.com.br/br" target="_blank"><img src="http://santuariodeanchieta.com/wp-content/uploads/2016/04/img-pontal.png"/></a></p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div id="divCadastro" class="aba">
                            <article>
                                <div id="divArticleContent" class="clearfix">
                                    <p>
                                        <b>Quer contribuir para a manuten��o e conserva��o do Santu�rio Nacional de S�o Jos� de Anchieta?</b>
                                        <br/>
                                        � simples, basta se cadastrar e voc� receber� em casa nosso informativo com um boleto para doa��o espont�nea ou atrav�s do Deposito em conta.
                                        Saiba, que sua colabora��o � sempre muito importante para n�s, e todos que dela se realizam na f�.
                                    </p>
                                    <?php if ($_smarty_tpl->getVariable('bErroCadastro')->value){?>
                                    <div class="divMensagemCadastro">
                                        <?php echo $_smarty_tpl->getVariable('sMensagemCadastro')->value;?>

                                    </div>
                                    <?php }?>
                                    <?php if (!isset($_GET[1])){?>
                                        <form action="campanha" method="post" id="formCadastroCampanha" class="formPadrao formCampanha">
                                            <ul>
                                                <li class="col75">
                                                    <label>Nome*</label>
                                                    <input type="text" name="inputNome" id="inputNome" class="txt" value="<?php echo (($tmp = @$_POST['inputNome'])===null||$tmp==='' ? '' : $tmp);?>
" />
                                                </li>
                                                <li class="col25">
                                                    <label>CPF*</label>
                                                    <input type="text" name="inputCPF" id="inputCPF" class="inputCPF" class="txt" value="<?php echo (($tmp = @$_POST['inputCPF'])===null||$tmp==='' ? '' : $tmp);?>
" />
                                                </li>
                                            </ul>
                                            <ul>
                                                <li class="col33">
                                                    <label>Data de Nascimento*</label>
                                                    <input type="text" name="inputDataNascimento" id="inputDataNascimento" class="txt" value="<?php echo (($tmp = @$_POST['inputDataNascimento'])===null||$tmp==='' ? '' : $tmp);?>
" />
                                                </li>
                                                <li class="col33">
                                                    <label>Sexo*</label>
                                                    <select name="inputSexo" id="inputSexo">
                                                        <option <?php if (isset($_POST['inputsexo'])&&($_POST['inputSexo']=='M')){?> checked <?php }?> value="M">Masculino</option>
                                                        <option <?php if (isset($_POST['inputsexo'])&&($_POST['inputSexo']=='F')){?> checked <?php }?> value="F">Feminino</option>
                                                    </select>
                                                </li>
                                                <li class="col33">
                                                    <label>CEP*</label>
                                                    <input type="text" name="inputCEP" id="inputCEP" class="txt" value="<?php echo (($tmp = @$_POST['inputCEP'])===null||$tmp==='' ? '' : $tmp);?>
" />
                                                </li>
                                            </ul>
                                            <ul>
                                                <li class="col33">
                                                    <label>Logradouro*</label>
                                                    <select name="inputLogradouro" id="inputLogradouro">
                                                        <?php  $_smarty_tpl->tpl_vars['oLogradouro'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('arrObjLogradouro')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['oLogradouro']->key => $_smarty_tpl->tpl_vars['oLogradouro']->value){
?>
                                                        <option <?php if (isset($_POST['inputlogradouro'])&&($_POST['inputlogradouro']==$_smarty_tpl->getVariable('oLogradouro')->value->iCodigo)){?>selected<?php }?> value="<?php echo $_smarty_tpl->getVariable('oLogradouro')->value->iCodigo;?>
"><?php echo $_smarty_tpl->getVariable('oLogradouro')->value->sDescricao;?>
</option>
                                                        <?php }} ?>
                                                    </select>
                                                </li>
                                                <li class="col66">
                                                    <label>Endere�o*</label>
                                                    <input type="text" name="inputEndereco" id="inputEndereco" class="txt" value="<?php echo (($tmp = @$_POST['inputEndereco'])===null||$tmp==='' ? '' : $tmp);?>
" />
                                                </li>
                                            </ul>
                                            <ul>
                                                <li class="col33">
                                                    <label>N�mero*</label>
                                                    <input type="text" name="inputNumero" id="inputNumero" class="txt" value="<?php echo (($tmp = @$_POST['inputNumero'])===null||$tmp==='' ? '' : $tmp);?>
" />
                                                </li>
                                                <li class="col66">
                                                    <label>Complemento</label>
                                                    <input type="text" name="inputComplemento" id="inputComplemento" class="txt" value="<?php echo (($tmp = @$_POST['inputComplemento'])===null||$tmp==='' ? '' : $tmp);?>
" />
                                                </li>
                                            </ul>
                                            <ul>
                                                <li class="col100">
                                                    <label>Bairro*</label>
                                                    <input type="text" name="inputBairro" id="inputBairro" class="txt" value="<?php echo (($tmp = @$_POST['inputBairro'])===null||$tmp==='' ? '' : $tmp);?>
" />
                                                </li>
                                            </ul>
                                            <ul>
                                                <li class="col33">
                                                    <?php $_template = new Smarty_Internal_Template("site/_estados.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
                                                </li>
                                                <li class="col66">
                                                    <label>Cidade*</label>
                                                    <input type="text" name="inputCidade" id="inputCidade" class="txt" value="<?php echo (($tmp = @$_POST['inputCidade'])===null||$tmp==='' ? '' : $tmp);?>
" />
                                                </li>
                                            </ul>
                                            <ul>
                                                <li class="col50">
                                                    <label>Telefone*</label>
                                                    <input type="text" name="inputTelefone" id="inputTelefone" class="txt" value="<?php echo (($tmp = @$_POST['inputTelefone'])===null||$tmp==='' ? '' : $tmp);?>
" />
                                                </li>
                                                <li class="col50">
                                                    <label>E-mail*</label>
                                                    <input type="text" name="inputEmail" id="inputEmail" class="txt" value="<?php echo (($tmp = @$_POST['inputEmail'])===null||$tmp==='' ? '' : $tmp);?>
" />
                                                </li>
                                            </ul>
                                            <ul>
                                                <li class="col100 noHeight">
                                                    <?php echo $_smarty_tpl->getVariable('recaptcha')->value;?>

                                                    <?php if ($_smarty_tpl->getVariable('bCaptchaCorreto')->value==false){?>
                                                        <label class="error errorCaptcha">Captcha incorreto</label>
                                                        <script>
                                                            var p = $(".errorCaptcha").position();
                                                            $(window).scrollTop(p.top);
                                                        </script>
                                                    <?php }?>
                                                </li>
                                            </ul>
                                            <ul>
                                                <li class="col100 noHeight">
                                                    <button type="submit" name="inputSalvar">Cadastrar</button>
                                                </li>
                                            </ul>
                                        </form>
                                    <?php }else{ ?>
                                        Seu cadastro foi realizado com sucesso. Em breve voc� receber� em casa a nossa correspond�ncia.
                                    <?php }?>
                                </div>
                            </article>
                        </div>
                        <div id="divDoacao"  class="aba">
                            <article>
                            <div id="divArticleContent" class="clearfix">
                                <h2>D�bito autom�tico</h2>
                                <p>&nbsp;</p>
                                <p>O d�bito autom�tico � uma forma pr�tica de contribuir com o Santu�rio Nacional de S�o Jos� de Anchieta. Optando por essa forma de contribui��o, o Devoto Amigo de Anchieta deve <b>escolher uma data e um valor fixo para o d�bito acontecer todos os meses.</b></p>
                                <p>A contribui��o pelo d�bito autom�tico tamb�m n�o possui um valor pr�-estipulado, dando ao s�cio, liberdade para escolher um valor que lhe � acess�vel. No entanto, como existem custos operacionais e banc�rios, o Santu�rio Nacional de Anchieta sugere que o valor m�nimo para contribui��o seja de R$ 30,00.</p>
                                <p><b>Voc� tamb�m pode optar pelo d�bito autom�tico e ajudar o Santu�rio Nacional de Anchieta com conforto e sem preocupa��o, basta ligar para (28) 3536-3985 e cadastrar-se junto a secretaria de atendimento.</b></p>
                                <p>A cada m�s o Santu�rio Nacional de Anchieta enviar� ao banco um pedido para que o d�bito seja feito na data escolhida, sem taxas, tarifas ou multas para o titular da conta. <b>No entanto, � importante lembrar que, como o banco far� uma consulta para efetuar o d�bito, se n�o tiver saldo para debitar o valor de contribui��o em sua conta, � o Santu�rio Nacional de Anchieta quem ir� arcar com as tarifas desta consulta.</b> Por isso � importante ter o valor da doa��o na conta para que aconte�a o d�bito autom�tico na data determinada.</p>
                                <p>Se desejar ser um s�cio evangelizador por d�bito autom�tico, ligue para (28) 3536-3985.</p>
                                <p>&nbsp;</p>
                                <p>&nbsp;</p>
                                <h2>Dep�sito Banc�rio</h2>
                                <p>&nbsp;</p>
                                <p>O dep�sito banc�rio � uma op��o para os s�cios e n�o s�cios. Esse dep�sito tamb�m pode ser feito online, o que chamamos de transfer�ncia online. Abaixo, voc� encontra os dados das contas para dep�sito banc�rio e transfer�ncia online.</b>
                                <p>Fa�a sua doa��o tamb�m na secretaria do Santu�rio de 8 �s 12 e 13h30 �s 18h.</b>
                                <p>Deus lhe pague!</b>
                                <div class="text-center">
                                    <p>
                                        <img src="http://www.santuariodeanchieta.com/wp-content/uploads/2016/03/banestes.png"/><br/>
                                        <b>Banco do Estado do Esp�rito Santo</b><br/>
                                        Agencia: 156<br/>
                                        Conta Corrente: 2614809-8<br/>
                                        Favorecido: Associa��o Internacional Anchieta
                                                                        
                                    </p>
                                    <p>&nbsp;</p>
                                    <p>
                                        <img src="http://www.santuariodeanchieta.com/wp-content/uploads/2016/03/BB-logo1-300x167-2.jpg"/><br/>       
                                        <b>Banco do Brasil</b><br/>
                                        Ag�ncia: 1438-9<br/>
                                        Conta Corrente: 26623-X<br/>
                                        Favorecido: Associa��o Internacional Anchieta                                                            
                                    </p>
                                    <p>&nbsp;</p>
                                    <b>Em breve voc� poder� fazer doa��es online para o Santu�rio!</b>
                                </div>
                                </div>
                            </article>
                        </div>
                        <div id="divDoacaoComRegistro"  class="aba">
                            <article>
                                <div class="divTopoCampanha">
                                    <div class="divTopoCampanhaTitulo">Fa�a sua doa��o</div>
                                    <a href="campanha"><div class="divTopoCampanhaLogo"></div></a>
                                </div>
                                
                                <div id="divArticleContent" class="clearfix">
                                <?php if ($_smarty_tpl->getVariable('bCadastradoSucesso')->value){?>
                                    <div class="divMensagemCadastro">
                                        <?php echo $_smarty_tpl->getVariable('sMensagemCadastro')->value;?>

                                    </div>
                                <?php }?>
                                    <strong class="linkCadastro cursorPointer">Se voc� ainda nao � cadastrado, cadastre-se aqui</strong>
                                    <div id="radioCadastro">
                                        <input type="radio" id="radioLocalizarCadastro" name="radioCadastro" checked="checked" /><label>J� possuo Cadastro</label>
                                        <input type="radio" id="radioDoarAnonimamente" name="radioCadastro" /><label>Doar Anonimamente</label>
                                    </div>
                                    <div id="divLocalizarCadastro">
                                        <label>CPF</label>
                                        <input type="text" id="inputLocalizarCPF" class="inputCPF" class="txt" />
                                        <button id="inputLocalizarCadastro">Localizar Cadastro </button>
                                        <span id="spanCarregandoCadastro"></span>
                                        <div id="divResultadoLocalizarCadastro"></div>
                                    </div>
                                    <div id="divValorDoacao">
                                        <form id="formDoarAnonimo" class="hide" action="campanha" method="post" target="_blank">
                                            <div id="radioDoacao">
                                                <input type="radio" id="radio10" name="radioValor" /><label for="radio10">R$ 10,00</label>
                                                <input type="radio" id="radio20" name="radioValor" checked="checked" /><label for="radio20">R$ 20,00</label>
                                                <input type="radio" id="radio40" name="radioValor" /><label for="radio40">R$ 40,00</label>
                                            </div>
                                            <label>Alterar valor</label>
                                            R$ <input type="text" name="inputValor" id="inputValorAnonimo" class="inputValor" class="moeda txt" value="20,00" />
                                            <?php if ($_smarty_tpl->getVariable('bCaptchaCorreto')->value==false){?>
                                            <label class="error errorCaptcha"></label>
                                            <?php }?>
                                            <input type="hidden" name="inputEmitirBoleto" />
                                            <input type="submit" name="inputEmitirBoletoAnonimo" id="inputEmitirBoletoAnonimo" value="Emitir Boleto" />
                                        </form>
                                    </div>
                                </div>
                            </article>
                    </section>
                </div>
            </section>
        </div>
        <?php $_template = new Smarty_Internal_Template("site/_footer.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>                   
        <script src="<?php echo $_smarty_tpl->getVariable('WWW_JS')->value;?>
jquery.maskedinput.js"></script>
        <script src="<?php echo $_smarty_tpl->getVariable('WWW_JS')->value;?>
jquery.maskMoney.js"></script>
        <script src="<?php echo $_smarty_tpl->getVariable('WWW_JS')->value;?>
util.js"></script>
        <script type="text/javascript" >
            $(function() {

                /* Abas */
                var content = $('#artigo'),
                tab = $('#menuCampanha li');

                tab.click(function() {
                    tab.removeClass('active');
                    $(this).addClass('active');
                    $('.aba', content).hide().eq($(this).index()).show();
                });
                
                <?php if ((isset($_GET['doar']))||($_smarty_tpl->getVariable('bCadastradoSucesso')->value==true)){?>
                    $("#liDoacao").click();
                <?php }elseif((isset($_GET['cadastro']))){?>
                    $("#liCadastro").click();
                <?php }elseif($_smarty_tpl->getVariable('bErroCadastro')->value==true){?>
                    $("#liCadastro").click();
                <?php }else{ ?>
                    $("#liInicio").click();
                <?php }?>
                
                //Toogle Doa�ao
                $('#radioDoarAnonimamente').click(function() {
                    $('#divLocalizarCadastro').hide();
                    $('#divResultadoLocalizarCadastro').hide();
                    $('#formDoarAnonimo').show();
                });
                $('#radioLocalizarCadastro').click(function() {
                    $('#divLocalizarCadastro').show();
                    $('#divResultadoLocalizarCadastro').show();
                    $('#formDoarAnonimo').hide();
                });

                $("#radioDoacao").buttonset();

                //Moedas
                $(".inputCPF").mask("999.999.999-99");
                $("#inputCEP").mask("99999-999");
                $("#inputDataNascimento").mask("99/99/9999");
                $(".moeda").maskMoney({
                    allowNegative: true, thousands: '.', decimal: ',', affixesStay: false
                });

                //Radios valores
                $('#radio10').click(function() {
                    $(".inputValor").val('10,00');
                });
                $('#radio20').click(function() {
                    $(".inputValor").val('20,00');
                });
                $('#radio40').click(function() {
                    $(".inputValor").val('40,00');
                });
                
                <?php if ($_smarty_tpl->getVariable('bCaptchaCorreto')->value==false){?>
                    var p = $(".errorCaptcha").position();
                    $(window).scrollTop(p.top);
                <?php }?>

            });

            /* Links */
            $(".linkCadastro").click(function() {
                $("#liCadastro").click();
            });
            $(".linkObras").click(function() {
                $("#liObras").click();
            });
            $(".linkInformativo").click(function() {
                $("#liInformativo").click();
            });
            $(".linkDoacao").click(function() {
                $("#liDoacao").click();
            });

            $("#inputLocalizarCadastro").click(function() {
                imgLoading = "<img src='<?php echo $_smarty_tpl->getVariable('WWW_CSS')->value;?>
images/bx_loader.gif' />";
                none = '';

                $('#spanCarregandoCadastro').html(imgLoading);
                
                $.post("index.php?pagina=site/ajax/buscar_colaborador", {
                    sCPF: $('#inputLocalizarCPF').val()
                }, function(data) {
                    $('#divResultadoLocalizarCadastro').html(data);
                    $('#spanCarregandoCadastro').html(none);
                    $("#radioDoacao").buttonset();
                    //Radios valores
                    $('#radio10').click(function() {
                        $(".inputValor").val('10,00');
                    });
                    $('#radio20').click(function() {
                        alert("entrou no radio de 20 reais");
                        $(".inputValor").val('20,00');
                    });
                    $('#radio40').click(function() {
                        $(".inputValor").val('40,00');
                    });
                });
            });
            
            $("#inputCEP").blur(function() {
                verificaCep($(this).val());
            });
            
            function verificaCep(iArgCep) 
            {    
                iArgCep = iArgCep.replace('-', '');
                iArgCep = iArgCep.replace('________', '');

                if ( ( $.trim( iArgCep ) ) !== '')
                {
                    imgLoading = '<img src="css/images/bx_loader.gif"  />';
                    none = '';

                    $('#spanLoader').html(imgLoading);

                    $.post("index.php?pagina=site/webservice/cep", {
                        iCep:iArgCep
                    }, function(data) {

                        var cep = jQuery.parseJSON(data);

                        $("#inputEndereco").get(0).value     = cep.endereco;
                        $("#inputBairro").get(0).value       = cep.bairro;
                        $("#inputCidade").get(0).value       = cep.cidade;
                        $("#inputComplemento").get(0).value  = cep.complemento;

                        $('#inputEstado').find('option[value='+cep.estado+']').attr('selected',true);
                        
                        $("#inputLogradouro").find("option").filter(function(index) {
                            return cep.logradouro.toUpperCase() === $(this).text();
                        }).attr("selected", "selected");
                        
                        $('#spanLoader').html(none);

                    });
                }
            }
            
        </script>
    </body>
</html>