<?php /* Smarty version Smarty-3.0.8, created on 2017-08-04 09:07:03
         compiled from "framework/templates/site/_header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2457059846367a361e6-09963295%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4d49043bc1aa5785362677f2e8677e9784a5d553' => 
    array (
      0 => 'framework/templates/site/_header.tpl',
      1 => 1501610649,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2457059846367a361e6-09963295',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <head>
        <title><?php echo $_smarty_tpl->getVariable('sNomeSite')->value;?>
</title>
        <meta http-equiv="Content-Type" content="text/html;charset=iso-8859-1" />
        <meta http-equiv="content-language" content="pt-br" />
        <meta name="keywords" content="<?php echo $_smarty_tpl->getVariable('sNomeSite')->value;?>
" />
        <meta name="description" content="<?php echo $_smarty_tpl->getVariable('sNomeSite')->value;?>
" />
        <meta name="robots" content="index, follow"/>
        <meta http-equiv="imagetoolbar" content="no" />
        <meta name="author" content="Amex Assessoria" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="copyright" content="Amex Assessoria" />
        <meta name="geo.region" content="GO"/>
        <meta name="geo.placename" content="Niquelândia" />
        <meta name="MSSmartTagsPreventParsing" content="true" />
        <meta http-equiv="imagetoolbar" content="no" />
        <meta http-equiv="content-script-type" content="text/javascript" />
        <meta http-equiv="content-style-type" content="text/css" />
        <base href="<?php echo $_smarty_tpl->getVariable('WWW')->value;?>
" />
        <meta property="fb:app_id" content="865104380211119" />
        
        <?php if (isset($_smarty_tpl->getVariable('oArtigo',null,true,false)->value)){?>
            <meta property="og:title" content="<?php echo htmlentities($_smarty_tpl->getVariable('oArtigo')->value->sTitulo);?>
" />
            <meta property="og:url" content="<?php echo $_smarty_tpl->getVariable('WWW')->value;?>
<?php echo $_smarty_tpl->getVariable('oArtigo')->value->sLink;?>
" />
            <meta property="og:description" content="<?php echo htmlentities($_smarty_tpl->getVariable('oArtigo')->value->sResumo);?>
" />
            <meta property="og:image" content="<?php echo $_smarty_tpl->getVariable('oArtigo')->value->sUrlImagem;?>
" />
        <?php }elseif(isset($_smarty_tpl->getVariable('oAlbum',null,true,false)->value)){?>
            <meta property="og:title" content="<?php echo htmlentities($_smarty_tpl->getVariable('oAlbum')->value->sTitulo);?>
" />
            <meta property="og:url" content="<?php echo $_smarty_tpl->getVariable('WWW')->value;?>
fotos/<?php echo $_smarty_tpl->getVariable('oAlbum')->value->iCodigo;?>
" />
            <meta property="og:description" content="<?php echo htmlentities($_smarty_tpl->getVariable('oAlbum')->value->sTitulo);?>
" />
            <meta property="og:image" content="<?php echo $_smarty_tpl->getVariable('WWW')->value;?>
<?php echo $_smarty_tpl->getVariable('oAlbum')->value->arrObjFoto[0]->sUrl;?>
" />
        <?php }?>

        <link rel="shortcut icon" href="<?php echo $_smarty_tpl->getVariable('WWW_IMG')->value;?>
favicon.png" type="image/x-icon" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="last-modified" content="<?php echo $_smarty_tpl->getVariable('lastModified')->value;?>
" /> 

        <!-- CSS -->
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('WWW_CSS')->value;?>
normalize.css" />
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('WWW_CSS')->value;?>
style.css" />
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('WWW_CSS')->value;?>
slicknav.css" />
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('WWW_CSS')->value;?>
style-responsive.css"/>
        <link rel="stylesheet" href="<?php echo $_smarty_tpl->getVariable('WWW_CSS')->value;?>
hover-min.css" />

        <!-- JQUERY -->
        <script src="<?php echo $_smarty_tpl->getVariable('WWW_JS')->value;?>
jquery.min.js"></script>
        
        <!-- FONT -->
        <link rel="stylesheet" href="fonts/styles.css" />
    </head>

