<?php /* Smarty version Smarty-3.0.8, created on 2017-08-04 09:07:04
         compiled from "framework/templates/site/_footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:707359846368862d88-30902894%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'feac2635a06e746664e75d3be2b0981a02b99a1b' => 
    array (
      0 => 'framework/templates/site/_footer.tpl',
      1 => 1501610649,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '707359846368862d88-30902894',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!-- Footer Wrapper -->

<footer>
    <div class="informacoes">
        <div class="container">
            <div class="instituicoes">
                <h1>Institui��es</h1>
                <ul>
                    <li>
                        <a href="http://www.vaticano.com/" target="_blank">
                            <img src="images/rodape/vaticano.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.cnbb.org.br/" target="_blank">
                            <img src="images/rodape/cnbb.png"/>
                        </a>
                    </li>
                    <li>
                        <a href="http://www.jesuitasbrasil.com/newportal/" target="_blank">
                            <img src="images/rodape/jesuitas.png"/>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="horarios">
                <h1>Hor�rios de missa</h1>
                <ul>
                    <li>
                        Participe conosco das Missas celebradas no Santu�rio.
                    </li>
                    <li>
                        Segunda � s�bado, �s 19h
                        <br/>Domingo, �s 8h30, 10h30 e 19h
                    </li>
                    <li>
                        Importante: os hor�rios aqui informados podem sofrer altera��es sem pr�vio aviso de acordo com a necessidade do Santu�rio.
                    </li>
                </ul>
            </div>
            <div class="institucional">
                <h1>Institucional</h1>
                <ul>
                    <li><a href="contato">Contato</a></li>
                    <li><a href="campanha">Campanha</a></li>
                    <li><a href="turismo">Turismo</a></li>
                    <li><a href="artigo/biblioteca-padre-diogo-fernandes-sj.html">Biblioteca</a></li>
                    <li><a href="museu">Museu</a></li>
                    <li><a href="artigo/arquivo.html">Arquivo</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="mapa">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3729.682306045169!2d-40.65587418547985!3d-20.804136771455635!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xb902b5dff9bfe3%3A0x17addadfafc2febe!2sSantu%C3%A1rio+Nacional+de+S%C3%A3o+Jos%C3%A9+de+Anchieta!5e0!3m2!1spt-BR!2sbr!4v1488481818031" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="rodape">
        <div class="container">
            <div class="santuario">
                <img src="images/rodape/logo-santuario.png"/>
                <span>Copyright <script>document.write(new Date().getFullYear())</script>. Mantenedora: Santu�rio Nacional de <br/>
                Anchieta. Atendimento: 8 �s 18h. Pra�a do Santu�rio, <br/>
                29230-000, Anchieta - ES - Brasil. Tel +55 (28) 3536-3985. <br/>
                contato@santuariodeanchieta.com<span>
            </div>
            <div class="amex">
                <span>Desenvolvido por:</span>
                <a href="http://www.amexassessoria.com" target="_blank">
                    <img src="images/rodape/logo-amex.png"/>
                </a>
            </div>
        </div>
    </div>
</footer>

<a href="#" class="back-to-top"></a>

<script src="<?php echo $_smarty_tpl->getVariable('WWW_JS')->value;?>
util.js"></script>

<script src="<?php echo $_smarty_tpl->getVariable('WWW_JS')->value;?>
jquery.slicknav.min.js"></script> 

<script src="<?php echo $_smarty_tpl->getVariable('WWW_JS')->value;?>
jquery.dropotron.min.js"></script>

<script type="text/javascript">

    

    jQuery(document).ready(function() {



        var wi = $(window).width();

    

        if (wi <= 1024)

        {

            

            $('#nav > ul').slicknav();

        }

        else

        {       

            $('#nav > ul').dropotron({

                mode: 'fade',

                noOpenerFade: true,

                speed: 300,

                detach: false

            });

        }

	

		
        $( "#abre-busca" ).click(function(e) {
            e.preventDefault();
            $("#divBusca").addClass("active");
        });

        $( "#fecha-busca" ).click(function(e) {
            e.preventDefault();
            $("#divBusca").removeClass("active");
        });

        $('#inputBusca').keypress(function (e) {
            if (e.which == 13) {
              window.location = 'busca/1/'+$("#inputBusca").val();
            }
        });

        $( ".idiomas ul li a" ).click(function(e) {
            e.preventDefault(); 
        });

        $( "#divBusca a" ).click(function(e) {
            e.preventDefault();
            if ($("#inputBusca").val().trim() !== '')
            {
                window.location = 'busca/1/'+$("#inputBusca").val();
            }
        });



        var offset = 220;

        var duration = 500;

        jQuery(window).scroll(function() {

            if (jQuery(this).scrollTop() > offset) {

                jQuery('.back-to-top').fadeIn(duration);

            } else {

                jQuery('.back-to-top').fadeOut(duration);

            }

        });



        jQuery('.back-to-top').click(function(event) {

            event.preventDefault();

            jQuery('html, body').animate({

                scrollTop: 0

            }, duration);

            return false;

        });

    });


    (function(i,s,o,g,r,a,m){
        i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-94032470-1', 'auto');
    ga('send', 'pageview');
</script>