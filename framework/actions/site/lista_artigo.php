<?php

/* Carrega as Classes Necessárias */
Controller::loadClass('site/artigo/artigo');
Controller::loadClass('site/artigo/artigoDB');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

/* Inclui menu */
include '_topo.php';

$sFiltro = '';

if ( isset ( $_GET["p"] ) )
{
    $iPagina = $_GET["p"]; 
}
else
{
    $iPagina = 1;
}

$iInicio = $iPagina - 1; 
$iInicio = (Config::NUMERO_PAGINAS_BUSCA) * $iInicio; 

ArtigoDB::setaLimite((Config::NUMERO_PAGINAS_BUSCA),$iInicio);
ArtigoDB::setaFiltro(' AND Tipo_lng_Codigo = 2 ');

$iTotalArtigo = ArtigoDB::contaArtigo();
$arrObjArtigo = ArtigoDB::pesquisaArtigoLista();

$iMenos = $iPagina - 1; 
$iMais  = $iPagina + 1;  

$iPaginas = ceil($iTotalArtigo / Config::NUMERO_PAGINAS_BUSCA);

$oTemplate->assign('iTotalArtigo', $iTotalArtigo);
$oTemplate->assign('iMenos', $iMenos);
$oTemplate->assign('iMais', $iMais);
$oTemplate->assign('iPagina', $iPagina);
$oTemplate->assign('iPaginas', $iPaginas);

$oTemplate->assign('sTextoBusca', $sFiltro);
$oTemplate->assign('arrObjArtigo', $arrObjArtigo);

/* Define Página/Template a ser executado */
$oTemplate->display('site/lista_artigo.tpl');

?>