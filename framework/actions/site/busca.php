<?php

/* Carrega as Classes Necess�rias */
Controller::loadClass('site/artigo/artigo');
Controller::loadClass('site/artigo/artigoDB');
Controller::loadClass('site/artigoTipo/artigoTipo');
Controller::loadClass('site/artigoTipo/artigoTipoDB');
Controller::loadClass('site/tag/tag');
Controller::loadClass('site/tag/tagDB');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

include '_topo.php';
include '_lateralInterna.php';

$sFiltro = '';
$oTag = new Tag();
$oArtigoTipo = new ArtigoTipo();

if ( isset ( $_GET["p"] ) )
{
    $iPaginaAtual = $_GET["p"]; 
}
else
{
    $iPaginaAtual = 1;
}


$iInicio = $iPaginaAtual - 1; 
$iInicio = (Config::NUMERO_PAGINAS_BUSCA) * $iInicio; 

ArtigoDB::setaLimite((Config::NUMERO_PAGINAS_BUSCA),$iInicio);
ArtigoDB::setaFiltro(" AND Tipo_lng_Codigo NOT IN ( 4, 5 ) ");
ArtigoDB::setaOrdem("Artigo_dat_Cadastro DESC,Artigo.Artigo_lng_Codigo DESC");

if ( isset ( $_GET['tipo'] ) )
{
    ArtigoDB::setaFiltro(" AND Tipo_lng_Codigo = ".$_GET['tipo']);
    ArtigoTipoDB::setaFiltro(" AND Tipo_lng_Codigo = ".$_GET['tipo']);
    $oArtigoTipo = ArtigoTipoDB::pesquisaArtigoTipo();
}
else
{
    if ( isset ( $_GET['tag'] ) )
    {
        ArtigoDB::setaJoin(" INNER JOIN Art_tag ON ( Artigo.Artigo_lng_Codigo = Art_tag.Artigo_lng_Codigo ) ");
        ArtigoDB::setaFiltro(" AND ( Art_tag.Tag_lng_Codigo = ".$_GET['tag']." )");

        TagDB::setaFiltro(' AND Tag_lng_Codigo = '.$_GET['tag']);
        $oTag = TagDB::pesquisaTag();
    }
    else
    {
        if ( isset ( $_POST['s'] ) )
        {
            $sFiltro = $_POST['s'];
        }
        else if ( isset ( $_GET['s'] ) && $_GET['s'] <> '' )
        {
            $sFiltro = $_GET['s'];
        }

        $sFiltro = utf8_decode($sFiltro);

        if ( strlen(trim($sFiltro)) <= 1 )
        {
            //Seta condi��o falsa para n�o retornar resultado 
            ArtigoDB::setaFiltro(' AND 1 = 2 ');
        }

        ArtigoDB::setaJoin(" LEFT JOIN Art_tag ON (Artigo.Artigo_lng_Codigo = Art_tag.Artigo_lng_Codigo)");
        ArtigoDB::setaJoin(" LEFT JOIN Tag ON (Art_tag.Tag_lng_Codigo = Tag.Tag_lng_Codigo)");
        ArtigoDB::setaFiltro(" AND ( ( UPPER(Artigo_vch_Titulo) LIKE UPPER('%".$sFiltro."%') ) OR ( UPPER(Artigo_txt_Conteudo) LIKE UPPER('%".$sFiltro."%') ) )");
        ArtigoDB::setaFiltro(" OR ( UPPER(Tag_vch_Descricao) LIKE UPPER('%".$sFiltro."%') )");
    }
}

$iTotalArtigo = ArtigoDB::contaArtigo();
$arrObjArtigo = ArtigoDB::pesquisaArtigoLista(); 

$iTotalPaginas = ceil($iTotalArtigo / Config::NUMERO_PAGINAS_BUSCA);
$arrPaginacao = Util::calculaPaginacao($iPaginaAtual,$iTotalPaginas);

/* Pagina��o */

$sPaginaNome  = '';
$sComplemento = '';

if ( isset ( $_GET['tag'] ) )
{
    $sPaginaNome  = 'tag';
    $sComplemento = '/'.$_GET['tag'];
}
elseif ( isset ( $_GET['nome'] ) )
{
    $sPaginaNome  = $_GET['nome'];
}
else
{
    $sPaginaNome  = 'busca';
    $sComplemento = '/'.$sFiltro;
}

$oTemplate->assign('sPaginaNome',   $sPaginaNome);
$oTemplate->assign('iPaginaAtual',  $iPaginaAtual);
$oTemplate->assign('iTotalPaginas', $iTotalPaginas);
$oTemplate->assign('iLoop',     $arrPaginacao['loop']);
$oTemplate->assign('iStart',    $arrPaginacao['start']);
$oTemplate->assign('iProxima',  $arrPaginacao['proxima']);
$oTemplate->assign('iAnterior', $arrPaginacao['anterior']);
$oTemplate->assign('sComplemento', $sComplemento);

$oTemplate->assign('iTotalArtigo', $iTotalArtigo);
$oTemplate->assign('sTextoBusca',  $sFiltro);
$oTemplate->assign('arrObjArtigo', $arrObjArtigo);
$oTemplate->assign('oTag',         $oTag);
$oTemplate->assign('oArtigoTipo',  $oArtigoTipo);

/* Define P�gina/Template a ser executado */
$oTemplate->display('site/busca.tpl');

?>