<?php

/* Carrega as Classes Necess�rias */
Controller::loadClass('site/album/album'); 
Controller::loadClass('site/album/albumDB');
Controller::loadClass('site/foto/foto'); 
Controller::loadClass('site/foto/fotoDB');
Controller::loadClass('site/artigo/artigo'); 
Controller::loadClass('site/artigo/artigoDB');
Controller::loadClass('site/artigoTag/artigoTag'); 
Controller::loadClass('site/artigoTag/artigoTagDB');
Controller::loadClass('site/artigoTipo/artigoTipo'); 
Controller::loadClass('site/artigoTipo/artigoTipoDB');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

/* Inclui menu */
include '_topo.php';
include '_lateralInterna.php';

$oArtigo = new Artigo;
$oSantoAnterior = '';
$oSantoProximo = '';
$oEvangelhoAnterior = '';
$oEvangelhoProximo = '';
//$sTipo = '';
$arrObjSanto = '';

/* Retira o .html da pagina para busca no banco */
$iPosicaoHtml = strpos($_GET['link'], '.html');
$sLink = substr($_GET['link'], 0, $iPosicaoHtml); 

ArtigoDB::setaFiltro(" AND Artigo_vch_Link = '".$sLink."'");

//Verifica se � Evangelho
if (isset($_GET['a']))
{
    ArtigoDB::setaFiltro(" AND ( Artigo_chr_Dia = '".$_GET['d']."' AND Artigo_chr_Mes = '".$_GET['m']."' AND Artigo_chr_Ano = '".$_GET['a']."' ) ");
}


$oArtigo = ArtigoDB::pesquisaArtigo();

/* Caso for Santo 
if ( $oArtigo->oArtigoTipo->iCodigo == '5')
{
    ArtigoDB::setaFiltro(" AND Tipo_lng_Codigo = 5 ");
    ArtigoDB::setaFiltro(" AND ( Artigo_chr_Dia = ".$oArtigo->sDia." AND Artigo_chr_Mes = ".$oArtigo->sMes." )");
    ArtigoDB::setaFiltro(" AND Artigo_lng_Codigo <> ".$oArtigo->iCodigo);
    $arrObjSanto = ArtigoDB::pesquisaArtigoLista();
}
*/

/* Caso for Santo ou Evangelho */
if ( $oArtigo->oArtigoTipo->iCodigo == '5' || $oArtigo->oArtigoTipo->iCodigo == '4')
{
    //echo date($oArtigo->sDia."-".$oArtigo->sMes."-Y");
    //$sDataArtigo = $oArtigo->sDia."-".$oArtigo->sMes.'-'.date('Y');
    //$sDataArtigo = date('d-m-Y', strtotime($sDataArtigo));
   
    
    //$sDataAnterior = date($sDataArtigo, strtotime($sDataArtigo . ' + 1 day'));
    //$sDataProxima  = date($sDataArtigo, strtotime($sDataArtigo . ' - 1 day'));
    
    $sDataArtigo = new DateTime($oArtigo->sDia."-".$oArtigo->sMes.'-'.date('Y'));
    $sDataArtigo->modify('-1 day');
    $sDataAnterior = $sDataArtigo->format('d-m-Y');
    
    $sDataArtigo = new DateTime($oArtigo->sDia."-".$oArtigo->sMes.'-'.date('Y'));
    $sDataArtigo->modify('+1 day');
    $sDataProxima = $sDataArtigo->format('d-m-Y');
    
    //echo $sDataAnterior;
    //echo $sDataProxima;
    
    $arrDataAnterior = explode('-', $sDataAnterior);
    $arrDataProxima  = explode('-', $sDataProxima);
    
    if ( $oArtigo->oArtigoTipo->iCodigo == '4' )
    {
        $sAnoAnterior = Util::calculaAno($arrDataAnterior[2]);
        $sAnoProximo  = Util::calculaAno($arrDataProxima[2]);
        
        ArtigoDB::setaFiltro(" AND ( Artigo_chr_Dia = '".$arrDataAnterior[0]."' AND Artigo_chr_Mes = '".$arrDataAnterior[1]."' AND Artigo_chr_Ano = '".$sAnoAnterior."' ) ");
        $oEvangelhoAnterior = ArtigoDB::pesquisaArtigo();

        ArtigoDB::setaFiltro(" AND ( Artigo_chr_Dia = '".$arrDataProxima[0]."' AND Artigo_chr_Mes = '".$arrDataProxima[1]."' AND Artigo_chr_Ano = '".$sAnoProximo."' ) ");
        $oEvangelhoProximo = ArtigoDB::pesquisaArtigo();
        
    }
    
    if ( $oArtigo->oArtigoTipo->iCodigo == '5' )
    {
        $sAnoAnterior = Util::calculaAno($arrDataAnterior[2]);
        $sAnoProximo  = Util::calculaAno($arrDataProxima[2]);
        
        ArtigoDB::setaFiltro(" AND ( Artigo_chr_Dia = '".$arrDataAnterior[0]."' AND Artigo_chr_Mes = '".$arrDataAnterior[1]."' ) ");
        $oSantoAnterior = ArtigoDB::pesquisaArtigo();

        ArtigoDB::setaFiltro(" AND ( Artigo_chr_Dia = '".$arrDataProxima[0]."' AND Artigo_chr_Mes = '".$arrDataProxima[1]."' ) ");
        $oSantoProximo = ArtigoDB::pesquisaArtigo();
    }
}


/*if ( $oArtigo->iCodigo == NULL )
{
    header("Location: ".PATH_WWW);
    exit();
}*/

$bContemCookie = false;

if ( isset($_COOKIE['pages'] ) ) 
{
    foreach ( $_COOKIE['pages'] as $valor ) 
    {
        if ( $valor == $oArtigo->iCodigo )
        {
            $bContemCookie = true;
        }
    }
}

if ( $bContemCookie == false )
{
    ArtigoDB::contaVisita( $oArtigo );
    setcookie("pages[]", $oArtigo->iCodigo,time()+60);
} 



/* Define Vari�veis do Template */
$oTemplate->assign ('oArtigo', $oArtigo );
$oTemplate->assign ('arrObjSanto',           $arrObjSanto );
$oTemplate->assign ('oSantoAnterior',        $oSantoAnterior );
$oTemplate->assign ('oSantoProximo',         $oSantoProximo );
$oTemplate->assign ('oEvangelhoAnterior',    $oEvangelhoAnterior );
$oTemplate->assign ('oEvangelhoProximo',     $oEvangelhoProximo );

/* Define P�gina/Template a ser executado */
$oTemplate->display('site/artigo.tpl');

//echo '<pre>';
//var_dump ( $_SESSION );
?>