<?php

/* Carrega as Classes Necessárias */
Controller::loadClass('site/artigo/artigo');
Controller::loadClass('site/artigo/artigoDB');
Controller::loadClass('site/artigoTipo/artigoTipo');
Controller::loadClass('site/artigoTipo/artigoTipoDB');
Controller::loadClass('site/tag/tag');
Controller::loadClass('site/tag/tagDB');
        
/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

include '_topo.php';

/* Define Página/Template a ser executado */
$oTemplate->display('site/turismo.tpl');

?>