<?php

/* Carrega as Classes Necessárias */
Controller::loadClass('site/artigo/artigo'); 
Controller::loadClass('site/artigo/artigoDB');
Controller::loadClass('site/artigoTag/artigoTag'); 
Controller::loadClass('site/artigoTag/artigoTagDB');
Controller::loadClass('site/album/album'); 
Controller::loadClass('site/album/albumDB');
Controller::loadClass('site/foto/foto'); 
Controller::loadClass('site/foto/fotoDB');
Controller::loadClass('site/carrossel/carrossel'); 
Controller::loadClass('site/carrossel/carrosselDB');
Controller::loadClass('site/popup/popup'); 
Controller::loadClass('site/popup/popupDB');
Controller::loadClass('site/pedido/pedido'); 
Controller::loadClass('site/pedido/pedidoDB');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

/* Inclui menu */
include '_topo.php';

AlbumDB::setaFiltro(" AND Album_chr_Visivel = 'S'");
AlbumDB::setaOrdem(" Album_lng_Codigo DESC");
AlbumDB::setaLimite(4);
$arrObjGaleriaDeFotos = AlbumDB::pesquisaAlbumLista();

//ArtigoDB::setaFiltro('Artigo_dat_Cadastro');
ArtigoDB::setaOrdem('Artigo_dat_Cadastro DESC');
ArtigoDB::setaLimite(CONFIG::QUANTIDADE_ULTIMAS_NOTICIAS);
ArtigoDB::setaFiltro(" AND Tipo_lng_Codigo = 1 ");
$arrObjUltimasNoticias = ArtigoDB::pesquisaArtigoLista(true);

CarrosselDB::setaLimite(Config::QUANTIDADE_NOTICIAS_CARROSSEL);
$arrObjCarrossel = CarrosselDB::pesquisaCarrosselLista();

$oPopup = PopupDB::pesquisaPopupAtivo();

PedidoDB::setaLimite(4);
PedidoDB::setaFiltro(" AND Pedido_chr_Tipo = 'T' ");
PedidoDB::setaFiltro(" AND Pedido_chr_Aprovado = 'S' ");
PedidoDB::setaFiltro(" AND Pedido_chr_ExibirIntencao = 'S' ");
$arrObjTestemunho = PedidoDB::pesquisaPedidoLista();

$oTemplate->assign ('arrObjUltimasNoticias', $arrObjUltimasNoticias );
$oTemplate->assign ('arrObjGaleriaDeFotos',  $arrObjGaleriaDeFotos );
$oTemplate->assign ('arrObjCarrossel', $arrObjCarrossel );
$oTemplate->assign ('arrObjTestemunho', $arrObjTestemunho );
$oTemplate->assign ('oPopup', $oPopup );

/* Define Página/Template a ser executado */
$oTemplate->display('site/inicial.tpl');

//echo '<pre>';
//var_dump ( $_SESSION );
?>
