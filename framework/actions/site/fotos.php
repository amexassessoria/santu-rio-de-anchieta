<?php

/* Carrega as Classes Necessárias */
Controller::loadClass('site/album/album'); 
Controller::loadClass('site/album/albumDB');
Controller::loadClass('site/foto/foto'); 
Controller::loadClass('site/foto/fotoDB');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

include '_topo.php';
include '_lateralInterna.php';

AlbumDB::setaFiltro(' AND Album_lng_Codigo = '.$_GET['album']);
$oAlbum = AlbumDB::pesquisaAlbum();

if ( $oAlbum->iCodigo == NULL )
{
    header("Location: ".PATH_WWW);
    exit();
}

FotoDB::setaFiltro(' AND Album_lng_Codigo = '.$_GET['album']);
$arrObjFoto = FotoDB::pesquisaFotoLista();

$oTemplate->assign ('oAlbum',  $oAlbum );
$oTemplate->assign ('arrObjFoto',  $arrObjFoto );

/* Define Página/Template a ser executado */
$oTemplate->display('site/fotos.tpl');


//echo '<pre>';
//var_dump ( $_SESSION );
?>