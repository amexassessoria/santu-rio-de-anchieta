<?php

/* Carrega as Classes Necessárias */
Controller::loadClass('site/album/album'); 
Controller::loadClass('site/album/albumDB');
Controller::loadClass('site/foto/foto'); 
Controller::loadClass('site/foto/fotoDB');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

include '_topo.php';
include '_lateralInterna.php';

if ( isset ( $_GET["p"] ) )
{
    $iPaginaAtual = $_GET["p"]; 
}
else
{
    $iPaginaAtual = 1;
}

$iInicio = $iPaginaAtual - 1; 
$iInicio = (Config::NUMERO_PAGINAS_MULTIMIDIA) * $iInicio; 

AlbumDB::setaLimite((Config::NUMERO_PAGINAS_MULTIMIDIA),$iInicio);
AlbumDB::setaOrdem(' Album_lng_Codigo DESC ');
AlbumDB::setaFiltro(" AND Album_chr_Visivel = 'S'");

$iTotal = AlbumDB::contaAlbum();
$arrObjAlbum = AlbumDB::pesquisaAlbumLista();

$iTotalPaginas = ceil($iTotal / Config::NUMERO_PAGINAS_MULTIMIDIA);
$arrPaginacao = Util::calculaPaginacao($iPaginaAtual,$iTotalPaginas);

$sPaginaNome  = 'albuns';
$sComplemento = '';

$oTemplate->assign('sPaginaNome',   $sPaginaNome);
$oTemplate->assign('sComplemento', $sComplemento);
$oTemplate->assign('iPaginaAtual',  $iPaginaAtual);
$oTemplate->assign('iTotalPaginas', $iTotalPaginas);
$oTemplate->assign('iLoop',     $arrPaginacao['loop']);
$oTemplate->assign('iStart',    $arrPaginacao['start']);
$oTemplate->assign('iProxima',  $arrPaginacao['proxima']);
$oTemplate->assign('iAnterior', $arrPaginacao['anterior']);

$oTemplate->assign ('arrObjAlbum',  $arrObjAlbum );

/* Define Página/Template a ser executado */
$oTemplate->display('site/albuns.tpl');


//echo '<pre>';
//var_dump ( $_SESSION );
?>