<?php

/* Carrega as Classes Necessárias */
Controller::loadClass('site/artigo/artigo');
Controller::loadClass('site/artigo/artigoDB');
Controller::loadClass('site/artigoTipo/artigoTipo');
Controller::loadClass('site/artigoTipo/artigoTipoDB');
Controller::loadClass('site/tag/tag');
Controller::loadClass('site/tag/tagDB');
Controller::loadClass('core/recaptchalib');
Controller::loadClass('core/mail');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

/* Inclui menu */
include '_topo.php';
include '_lateralInterna.php';

$bCaptchaCorreto = true;   
$sMensagemCadastro   = '';

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    if (isset($_POST['inputEnviar'])) 
    {
        $resp = recaptcha_check_answer (Config::RECAPTCHA_PRIVATE_KEY, $_SERVER["REMOTE_ADDR"],$_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

        if (!$resp->is_valid) 
        {
            $bCaptchaCorreto = false;
        }
        else
        {
            
            $sContatoNome     = $_POST['inputNome'];
            $sContatoEmail    = $_POST['inputEmail'];
            $sContatoTelefone = $_POST['inputTelefone'];
            $sContatoMensagem = $_POST['inputMensagem'];
            
            $sArrDestinatario= '';
            
            $sArrDestinatario[0]['Nome']  = Config::NOME_SITE;
            $sArrDestinatario[0]['Email'] = Config::EMAIL_SITE;
            //$sArrDestinatario[0]['Email'] = 'fa.bio-santos@hotmail.com';
            
            $sContatoMensagem = "<b>Nome:</b>".$sContatoNome."<br/> <b>E-mail: </b>".$sContatoEmail."<br/> <b>Telefone: </b>".$sContatoTelefone."<br/> <b>Mensagem:</b> ".$sContatoMensagem;
            
            Mail::setaAssunto("Contato do site");
            Mail::setaConteudo($sContatoMensagem);
            Mail::setaDestinatario($sArrDestinatario);
            Mail::setaReply($sContatoEmail, $sContatoNome);
            
            if( Mail::enviaEmail() )
            {
                $sMensagemCadastro = '<label class="sucesso">Obrigado pelo contato!</label>';
            }
            else
            {
                $sMensagemCadastro = '<label class="erro">Erro ao enviar e-mail</label>';
            }
        }
    }
}

$oTemplate->assign('recaptcha',  recaptcha_get_html(Config::RECAPTCHA_PUBLIC_KEY) );
$oTemplate->assign('bCaptchaCorreto', $bCaptchaCorreto );
$oTemplate->assign('sMensagemCadastro', $sMensagemCadastro );

/* Define Página/Template a ser executado */
$oTemplate->display('site/contato.tpl');


//echo '<pre>';
//var_dump ( $_SESSION );
?>