<?php

$w = 287; //largura
$h = 190; //altura
$pic = $_GET['img'];

switch ($_GET['tipo']){
    case 'album':
      $pic = 'images/fotosAlbuns/'.$_GET['img'];
      $w = 250; //largura
      $h = 195; //altura
      break;
    case 'albumLateral':
      $pic = 'images/fotosAlbuns/'.$_GET['img'];
      $w = 205; //largura
      $h = 136; //altura
      break;
    case 'albumLista':
      $pic = 'images/fotosAlbuns/'.$_GET['img'];
      $w = 191; //largura
      $h = 143; //altura
      break;
    case 'albumThumb':
      $pic = 'images/fotosAlbuns/'.$_GET['img'];
      $w = 40; //largura
      $h = 40; //altura
      break;
    case 'destaque':
      $w = 224; 
      $h = 150; 
      break;
    case 'ultimasNoticias':
      $w = 155; 
      $h = 117; 
      break;
    case 'santoEvangelho':
      $w = 140; 
      $h = 105; 
      break;
    case 'maisVistos':
      $w = 206; 
      $h = 136; 
      break;
    case 'busca':
      $w = 153; 
      $h = 100; 
      break;
  }

Controller::loadClass('core/canvas');

if ( isset( $_GET['img'] ) )
{
    
    
    $t = new Canvas;
    $t->carrega( $pic );
    $t->redimensiona( $w, $h, 'preenchimento' );
    $t->grava();
}
?>