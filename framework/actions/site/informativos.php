<?php

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

include '_topo.php';
include '_lateralInterna.php';

$arrParametrosIssuu = array(
    'access' => 'public',
    'action' => 'issuu.documents.list',
    'apiKey' => CONFIG::ISSUU_KEY,
    'documentSortBy' => 'publishDate',
    'publishDate' => 'public',
    'format' => 'json',
    'pageSize' => '9',
    'responseParams' => 'title,description,documentId,name',
    'resultOrder' => 'desc'
);

$arrInformativo = file_get_contents(Util::geraAssinaturaIssuu($arrParametrosIssuu));

$oTemplate->assign ('arrInformativo',  json_decode($arrInformativo));

/* Define P�gina/Template a ser executado */
$oTemplate->display('site/informativos.tpl');


//echo '<pre>';
//var_dump ( $_SESSION );
?>