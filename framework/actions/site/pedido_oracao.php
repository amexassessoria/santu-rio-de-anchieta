<?php

/* Carrega as Classes Necess�rias */
Controller::loadClass('site/pedido/pedido');
Controller::loadClass('site/pedido/pedidoDB');
Controller::loadClass('core/recaptchalib');
Controller::loadClass('core/mail');
Controller::loadClass('site/parametro/parametro'); 
Controller::loadClass('site/parametro/parametroDB');
Controller::loadClass('site/bloqueio/bloqueio');
Controller::loadClass('site/bloqueio/bloqueioDB');
  
/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

/* Inclui menu */
include '_topo.php';
include '_lateralInterna.php';

$bCaptchaCorreto     = true;
$sMensagemCadastro   = '';

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    if (isset($_POST['inputNome'])) 
    {
        $resp = recaptcha_check_answer (Config::RECAPTCHA_PRIVATE_KEY, $_SERVER["REMOTE_ADDR"],$_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

        if (!$resp->is_valid) 
        {
            $bCaptchaCorreto = false;
        }
        else 
        {

            BloqueioDB::setaFiltro(" AND Bloqueio_vch_Email = '".$_POST['inputEmail']."'");
            $oBloqueio = BloqueioDB::pesquisaBloqueio();
            
            if ($oBloqueio->iCodigo == '') {

                $oPedido = new Pedido;

                $sPublico = (isset($_POST['inputPublico'])) ? 'S' : 'N';

                $oPedido->sNome           = $_POST['inputNome'];
                $oPedido->sEmail          = $_POST['inputEmail'];
                $oPedido->sIntencao       = Util::filtraPalavroes($_POST['inputIntencao']);
                $oPedido->sData           = date('Y-m-d');
                $oPedido->sExibirIntencao = $sPublico;
                $oPedido->sTipo           = 'P';
                $oPedido->sRespondido     = 'S';

                $sResultado = PedidoDB::salvaPedido($oPedido);

                if ( $sResultado )
                {
                    $oParametro = ParametroDB::pesquisaParametro();

                    /* Define Vari�veis do Template */
                    $oTemplate->assign('oPedido', $oPedido);
                    $oTemplate->assign('sNomeSite', Config::NOME_SITE);

                    $oTemplate->assign('sResposta', $oParametro->sRespostaPedidoOracao);
                    Mail::setaAssunto($oPedido->sNome.', respondemos o seu Pedido de ora��o!');

                    $sConteudo = $oTemplate->fetch('email/respostaPedido.tpl');
                    
                    $sArrDestinatario = '';
                    $sArrDestinatario[0]['Nome']  = $oPedido->sNome;
                    $sArrDestinatario[0]['Email'] = $oPedido->sEmail;

                    Mail::setaConteudo($sConteudo);
                    Mail::setaDestinatario($sArrDestinatario);
                    Mail::enviaEmail();
                    
                    $sMensagemCadastro = '<label class="sucesso">Seu pedido de ora��o foi realizado com sucesso!</label>';
                }
                else
                {
                    $sMensagemCadastro = '<label class="erro">Erro ao cadastrar seu pedido de ora��o.</label>';
                }
            } else {
                $sMensagemCadastro = '<label class="erro">Seu e-mail est� bloqueado pelo administrador do sistema</label>';
            }
        }
    }
}

$oTemplate->assign('recaptcha',  recaptcha_get_html(Config::RECAPTCHA_PUBLIC_KEY) );
$oTemplate->assign('bCaptchaCorreto', $bCaptchaCorreto );
$oTemplate->assign('sMensagemCadastro', $sMensagemCadastro );

/* Define P�gina/Template a ser executado */
$oTemplate->display('site/pedido_oracao.tpl');

?>