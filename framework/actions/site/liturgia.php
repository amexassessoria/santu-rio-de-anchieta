<?php

/* Carrega as Classes Necessárias */
Controller::loadClass('site/artigo/artigo'); 
Controller::loadClass('site/artigo/artigoDB');
Controller::loadClass('site/artigoTag/artigoTag'); 
Controller::loadClass('site/artigoTag/artigoTagDB');
Controller::loadClass('site/artigoTipo/artigoTipo'); 
Controller::loadClass('site/artigoTipo/artigoTipoDB');
Controller::loadClass('core/dom');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

/* Inclui menu */
include '_topo.php';
include '_lateralInterna.php';

$sUrl = "http://liturgiadiaria.cnbb.org.br/app/user/user/UserView.php?ano=".date("Y")."&mes=".date("n")."&dia=".date("j");
$html = file_get_html($sUrl);
$sLeituras = $html->find('#corpo_leituras',0);
$sConteudo = $sLeituras->outertext;

/* Define Variáveis do Template */
$oTemplate->assign ('sConteudo', $sConteudo );

/* Define Página/Template a ser executado */
$oTemplate->display('site/liturgia.tpl');

//echo '<pre>';
//var_dump ( $_SESSION );
?>