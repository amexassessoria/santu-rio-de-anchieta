<?php

/* Carrega as Classes Necess�rias */
Controller::loadClass('site/colaborador/colaborador');
Controller::loadClass('site/colaborador/colaboradorDB');
Controller::loadClass('site/boleto/boleto');
Controller::loadClass('site/boleto/boletoDB');

include 'boleto/autoloader.php';

use OpenBoleto\Banco\Caixa;
use OpenBoleto\Agente;

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

/* Inclui menu */
include '_topo.php';

if (isset($_POST['inputEmitirBoleto']))
{

    $oDadosBoleto = BoletoDB::pesquisaBoleto();
    
    $sDataVencimento = new DateTime('NOW');
    $sDataVencimento->add(new DateInterval('P5D'));
    
    $cont = file_get_contents("boleto/sequencia.txt");
    $cont = $cont+1;
    file_put_contents("boleto/sequencia.txt",$cont);
    
    $sequencia = str_pad($cont, 12, "0", STR_PAD_LEFT);

    $oSacado  = new Agente($_POST['inputNome'], $_POST['inputCPF'], $_POST['inputEndereco'] . ' ' . $_POST['inputNumero'] . ' ' . $_POST['inputBairro'], $_POST['inputCEP'], $_POST['inputCidade'], $_POST['inputEstado']);
    $oCedente = new Agente($oDadosBoleto->sNomeCedente, $oDadosBoleto->sCNPJCedente, $oDadosBoleto->sEnderecoCedente.' '.$oDadosBoleto->sNumeroCedente.' '.$oDadosBoleto->sBairroCedente, $oDadosBoleto->sCEPCedente, $oDadosBoleto->sCidadeCedente , $oDadosBoleto->sEstadoCedente);
    
    switch ($oDadosBoleto->sCodigoBanco)
    {
        case '104':
            $oBoleto = new Caixa(array(
                // Par�metros obrigat�rios
                'dataVencimento' => $sDataVencimento,
                'valor' => $_POST['inputValor'],
                'sequencial' => Config::NUMERO_CAMPANHA.$sequencia,
                'sacado' => $oSacado,
                'cedente' => $oCedente,
                'agencia' => $oDadosBoleto->sAgenciaNumero, // At� 4 d�gitos
                'carteira' => $oDadosBoleto->sCarteira,
                'conta' => $oDadosBoleto->sContaNumero, // At� 8 d�gitos
                'convenio' => $oDadosBoleto->sConvenio, // 4, 6 ou 7 d�gitos
                // Caso queira um n�mero sequencial de 17 d�gitos, a cobran�a dever�:
                // - Ser sem registro (Carteiras 16 ou 17)
                // - Conv�nio com 6 d�gitos
                // Para isso, defina a carteira como 21 (mesmo sabendo que ela � 16 ou 17, isso � uma regra do banco)
                // Par�metros recomend�veis
                //'logoPath' => 'http://empresa.com.br/logo.jpg', // Logo da sua empresa
                'contaDv' => $oDadosBoleto->sContaDigito,
                'descricaoDemonstrativo' => array(// At� 5
                    'Compra de materiais cosm�ticos',
                    'Compra de alicate',
                ),
                'instrucoes' => array(// At� 8
                    $oDadosBoleto->sInstrucao1,
                    $oDadosBoleto->sInstrucao2,
                    $oDadosBoleto->sInstrucao3,
                    $oDadosBoleto->sInstrucao4,
                    $oDadosBoleto->sInstrucao5,
                    $oDadosBoleto->sInstrucao6,
                    $oDadosBoleto->sInstrucao7,
                    $oDadosBoleto->sInstrucao8
                ),
                    // Par�metros opcionais
                    //'resourcePath' => '../resources',
                    'moeda' => Caixa::MOEDA_REAL,
                    //'dataDocumento' => new DateTime(),
                    //'dataProcessamento' => new DateTime(),
                    //'contraApresentacao' => true,
                    //'pagamentoMinimo' => 23.00,
                    //'aceite' => 'N',
                    'especieDoc' => 'OU',
                    'numeroDocumento' => '000002449618',
                    //'usoBanco' => 'Uso banco',
                    //'layout' => 'layout.phtml',
                    //'logoPath' => 'http://boletophp.com.br/img/opensource-55x48-t.png',
                    //'sacadorAvalista' => new Agente('Ant�nio da Silva', '02.123.123/0001-11'),
                    //'descontosAbatimentos' => 123.12,
                    //'moraMulta' => 123.12,
                    //'outrasDeducoes' => 123.12,
                    //'outrosAcrescimos' => 123.12,
                    //'valorCobrado' => 123.12,
                    //'valorUnitario' => 123.12,
                    //'quantidade' => 1,
            ));
            break;
    }


        /* $oColaborador = new Colaborador;

          $oColaborador->sNome      = $_POST['inputNome'];
          $oColaborador->sEmail     = $_POST['inputEmail'];
          $oColaborador->sCidade    = $_POST['inputCidade'];
          $oColaborador->sEstado    = $_POST['inputEstado'];
          $oColaborador->sBairro    = $_POST['inputBairro'];
          $oColaborador->sTelefone  = $_POST['inputTelefone'];
          $oColaborador->iNumero    = $_POST['inputNumero'];
          $oColaborador->sEndereco  = $_POST['inputEndereco'];

          ColaboradorDB::salvaColaborador($oColaborador);

          echo "<script>
          alert('Seu cadastro foi realizado com sucesso. Em breve voc� receber� em casa a nossa correspond�ncia.');
          window.location = 'campanha';
          </script>";
         */

        echo $oBoleto->getOutput();
    }
    else
    {
        /* Define P�gina/Template a ser executado */
        $oTemplate->display('site/doacao.tpl');
    }
?>