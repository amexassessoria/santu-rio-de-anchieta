<?php

/* Carrega as Classes Necessárias */
Controller::loadClass('site/colaborador/colaborador');
Controller::loadClass('site/colaborador/colaboradorDB');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

ColaboradorDB::setaFiltro("AND Colaborador_chr_CPF = '".$_POST['sCPF']."'");
$oColaborador = ColaboradorDB::pesquisaColaborador();

$oTemplate->assign('oColaborador', $oColaborador);
    
/* Define Página/Template a ser executado */
$oTemplate->display('site/ajax/buscar_colaborador.tpl');

?>