<?php

/* Carrega as Classes Necessárias */
Controller::loadClass('site/audio/audio'); 
Controller::loadClass('site/audio/audioDB');
Controller::loadClass('site/audioTipo/audioTipo'); 
Controller::loadClass('site/audioTipo/audioTipoDB');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

/* Inclui menu */
include '_topo.php';
include '_lateralInterna.php';

/*AudioTipoDB::setaFiltro(" AND Audio_Tipo_lng_Codigo = ".$_GET['p']);
$oAudioTipo = AudioTipoDB::pesquisaAudioTipo();*/

//AudioDB::setaFiltro(" AND Audio_Tipo_lng_Codigo = ".$_GET['p']);
AudioDB::setaOrdem(' Audio_lng_Codigo DESC ');
$arrObjAudio = AudioDB::pesquisaAudioLista();

//$oTemplate->assign ('oAudioTipo',  $oAudioTipo );
$oTemplate->assign ('arrObjAudio',  $arrObjAudio );

/* Define Página/Template a ser executado */
$oTemplate->display('site/audios.tpl');


//echo '<pre>';
//var_dump ( $_SESSION );
?>