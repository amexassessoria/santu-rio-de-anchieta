<?php

/* Carrega as Classes Necess�rias */
Controller::loadClass('site/pedido/pedido');
Controller::loadClass('site/pedido/pedidoDB');
Controller::loadClass('core/recaptchalib');
Controller::loadClass('core/upload'); 
Controller::loadClass('core/mail');
Controller::loadClass('site/parametro/parametro'); 
Controller::loadClass('site/parametro/parametroDB');
Controller::loadClass('site/bloqueio/bloqueio');
Controller::loadClass('site/bloqueio/bloqueioDB');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

/* Inclui menu */
include '_topo.php';
include '_lateralInterna.php';

$bCaptchaCorreto = true;
$sMensagemCadastro   = '';

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    if (isset($_POST['inputNome'])) 
    {
        $resp = recaptcha_check_answer (Config::RECAPTCHA_PRIVATE_KEY, $_SERVER["REMOTE_ADDR"],$_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

        if (!$resp->is_valid) 
        {
            $bCaptchaCorreto = false;
        }
        else
        {
            BloqueioDB::setaFiltro(" AND Bloqueio_vch_Email = '".$_POST['inputEmail']."'");
            $oBloqueio = BloqueioDB::pesquisaBloqueio();
            
            if ($oBloqueio->iCodigo == '') {

                $sDiretorioDestino = 'images/testemunho/';
                
                if ( !file_exists ( $sDiretorioDestino ) )
                {
                    mkdir( $sDiretorioDestino );
                }
                
                $oPedido = new Pedido;
                $oFotoUpload = new Upload( $_FILES['inputFotoTestemunho'] );
                
                if ( $oFotoUpload->uploaded )
                {
        
                    $oFotoUpload->file_overwrite = true;
                    $oFotoUpload->image_convert = 'jpg';
                    //Configuracoes de redimensionamento retrato
                    $lMax  = 700; //largura maxima permitida
                    $aMax  = 600; // altura maxima permitida
                    //Configuracoes de redimensionamento paisagem
                    $plMax = 650; //largura maxima permitida
                    $paMax = 550; // altura maxima permitida


                    if ( $oFotoUpload->image_src_x > $oFotoUpload->image_y )
                    {
                        if ( $oFotoUpload->image_src_x > $lMax || $oFotoUpload->image_y > $aMax )
                        {
                            $oFotoUpload->image_resize = true;
                            $oFotoUpload->image_ratio = true;
                            $oFotoUpload->image_x = ($lMax / 2);
                            $oFotoUpload->image_y = ($aMax / 2);
                        }
                    }
                    else
                    {
                        if ( $oFotoUpload->image_src_x > $plMax || $oFotoUpload->image_y > $paMax )
                        {
                            $oFotoUpload->image_resize = true;
                            $oFotoUpload->image_ratio = true;
                            $oFotoUpload->image_x = ($plMax / 2);
                            $oFotoUpload->image_y = ($paMax / 2);
                        }
                    }

                    $oFotoUpload->file_new_name_body = md5( uniqid( $_FILES['inputFotoTestemunho']['name'] ) );
                    $oFotoUpload->Process( $sDiretorioDestino );
                    
                    if ( $oFotoUpload->processed )
                    {
                        $oPedido->sImagemUrl     = $oFotoUpload->file_dst_name;
                    }
                }
                

                $sPublico = (isset($_POST['inputPublico'])) ? 'S' : 'N';

                $oPedido->sNome           = $_POST['inputNome'];
                $oPedido->sEmail          = $_POST['inputEmail'];
                $oPedido->sCidade         = $_POST['inputCidade'];
                $oPedido->sEstado         = $_POST['inputEstado'];
                $oPedido->sIntencao       = Util::filtraPalavroes($_POST['inputTestemunho']);
                $oPedido->sData           = date('Y-m-d');
                $oPedido->sTipo           = 'T';
                $oPedido->sAprovado       = 'N';
                $oPedido->sRespondido     = 'S';
                $oPedido->sExibirIntencao = $sPublico;

                $sResultado = PedidoDB::salvaPedido($oPedido);

                if ( $sResultado )
                {

                    $oParametro = ParametroDB::pesquisaParametro();

                    /* Define Vari�veis do Template */
                    $oTemplate->assign('oPedido', $oPedido);
                    $oTemplate->assign('sNomeSite', Config::NOME_SITE);

                    $oTemplate->assign('sResposta', $oParametro->sRespostaTestemunho);
                    Mail::setaAssunto($oPedido->sNome.', respondemos o seu Testemunho de f�!');

                    $sConteudo = $oTemplate->fetch('email/respostaPedido.tpl');
                    
                    $sArrDestinatario = '';
                    $sArrDestinatario[0]['Nome']  = $oPedido->sNome;
                    $sArrDestinatario[0]['Email'] = $oPedido->sEmail;

                    Mail::setaConteudo($sConteudo);
                    Mail::setaDestinatario($sArrDestinatario);
                    Mail::enviaEmail();

                    $sMensagemCadastro = '<label class="sucesso">Seu testemunho de f� foi realizado com sucesso! Aguarde a aprova��o.</label>';
                }
                else
                {
                    $sMensagemCadastro = '<label class="erro">Erro ao cadastrar seu testemunho de f�.</label>';
                }
            } else {
                $sMensagemCadastro = '<label class="erro">Seu e-mail est� bloqueado pelo administrador do sistema</label>';
            }
        }

    }
}

if ( isset ( $_GET["p"] ) )
{
    $iPaginaAtual = $_GET["p"]; 
}
else
{
    $iPaginaAtual = 1;
}

$iInicio = $iPaginaAtual - 1; 
$iInicio = (Config::NUMERO_VELAS) * $iInicio; 

if ( isset ( $_POST['inputBuscarTestemunho'] ) && ( trim ( $_POST['inputEmailBusca'] ) <> '' ) )
{
    PedidoDB::setaFiltro("  AND ( Pedido_vch_Email = '".$_POST['inputEmailBusca']."' )");
}
else
{
    PedidoDB::setaLimite((Config::NUMERO_VELAS),$iInicio);
}

PedidoDB::setaFiltro(" AND Pedido_chr_Tipo = 'T' ");
PedidoDB::setaFiltro(" AND Pedido_chr_Aprovado = 'S' ");
PedidoDB::setaFiltro(" AND Pedido_chr_ExibirIntencao = 'S' ");

$iTotalTestemunho = PedidoDB::contaPedido();
$arrObjTestemunho = PedidoDB::pesquisaPedidoLista();

/*if ( ( $arrObjPedido->count() ) == 0 && ( !isset ( $_POST['inputBuscarPedido'] ) ) )
{
    header("Location: ".PATH_WWW);
    exit();
}*/

$iTotalPaginas = ceil($iTotalTestemunho / Config::NUMERO_VELAS);
$arrPaginacao = Util::calculaPaginacao($iPaginaAtual,$iTotalPaginas);

/* Define Vari�veis do Template */

/* Pagina��o */
$oTemplate->assign('sPaginaNome', 'testemunho');
$oTemplate->assign('iPaginaAtual', $iPaginaAtual);
$oTemplate->assign('iTotalPaginas', $iTotalPaginas);
$oTemplate->assign('iLoop', $arrPaginacao['loop']);
$oTemplate->assign('iStart', $arrPaginacao['start']);
$oTemplate->assign('iProxima', $arrPaginacao['proxima']);
$oTemplate->assign('iAnterior', $arrPaginacao['anterior']);
$oTemplate->assign('sComplemento', '');

$oTemplate->assign('recaptcha',  recaptcha_get_html(Config::RECAPTCHA_PUBLIC_KEY) );
$oTemplate->assign('bCaptchaCorreto', $bCaptchaCorreto );
$oTemplate->assign('sMensagemCadastro', $sMensagemCadastro );
$oTemplate->assign('iTotalTestemunho', $iTotalTestemunho);
$oTemplate->assign('arrObjTestemunho', $arrObjTestemunho);

/* Define P�gina/Template a ser executado */
$oTemplate->display('site/testemunho.tpl');

?>