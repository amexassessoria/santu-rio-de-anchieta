<?php

/* Carrega as Classes Necessárias */
Controller::loadClass('site/album/album'); 
Controller::loadClass('site/album/albumDB');
Controller::loadClass('site/foto/foto'); 
Controller::loadClass('site/foto/fotoDB');
Controller::loadClass('site/artigo/artigo'); 
Controller::loadClass('site/artigo/artigoDB');
Controller::loadClass('site/artigoTag/artigoTag'); 
Controller::loadClass('site/artigoTag/artigoTagDB');
Controller::loadClass('site/artigoTipo/artigoTipo'); 
Controller::loadClass('site/artigoTipo/artigoTipoDB');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();


ArtigoDB::setaFiltro(" AND Artigo_lng_Codigo = ".$_POST['codigo']);
$oArtigo = ArtigoDB::pesquisaArtigo();


$oTemplate->assign ('oArtigo', $oArtigo );

/* Define Página/Template a ser executado */
$oTemplate->display('site/imprimirArtigo.tpl');


?>