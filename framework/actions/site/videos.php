<?php

/* Carrega as Classes Necessárias */
Controller::loadClass('site/video/video'); 
Controller::loadClass('site/video/videoDB');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

include '_topo.php';
include '_lateralInterna.php';

if ( isset ( $_GET["p"] ) )
{
    $iPaginaAtual = $_GET["p"]; 
}
else
{
    $iPaginaAtual = 1;
}

$iInicio = $iPaginaAtual - 1; 
$iInicio = (Config::NUMERO_PAGINAS_MULTIMIDIA) * $iInicio; 

VideoDB::setaLimite((Config::NUMERO_PAGINAS_MULTIMIDIA),$iInicio);
VideoDB::setaOrdem(' Video_lng_Codigo DESC ');

$iTotal = VideoDB::contaVideo();
$arrObjVideo = VideoDB::pesquisaVideoLista();

$iTotalPaginas = ceil($iTotal / Config::NUMERO_PAGINAS_MULTIMIDIA);
$arrPaginacao = Util::calculaPaginacao($iPaginaAtual,$iTotalPaginas);

$sPaginaNome  = 'videos';
$sComplemento = '';

$oTemplate->assign('sPaginaNome',   $sPaginaNome);
$oTemplate->assign('sComplemento', $sComplemento);
$oTemplate->assign('iPaginaAtual',  $iPaginaAtual);
$oTemplate->assign('iTotalPaginas', $iTotalPaginas);
$oTemplate->assign('iLoop',     $arrPaginacao['loop']);
$oTemplate->assign('iStart',    $arrPaginacao['start']);
$oTemplate->assign('iProxima',  $arrPaginacao['proxima']);
$oTemplate->assign('iAnterior', $arrPaginacao['anterior']);

$oTemplate->assign ('arrObjVideo',  $arrObjVideo );

/* Define Página/Template a ser executado */
$oTemplate->display('site/videos.tpl');


//echo '<pre>';
//var_dump ( $_SESSION );
?>