<?php

/* Carrega as Classes Necess�rias */
Controller::loadClass('site/pedido/pedido');
Controller::loadClass('site/pedido/pedidoDB');
Controller::loadClass('core/recaptchalib');
Controller::loadClass('site/bloqueio/bloqueio');
Controller::loadClass('site/bloqueio/bloqueioDB');

/* Inicializa o Template */
$oTemplate = Template::inicializaSmarty();

/* Inclui menu */
include '_topo.php';
include '_lateralInterna.php';

$bCaptchaCorreto = true;   
$sMensagemCadastro   = '';

if ($_SERVER["REQUEST_METHOD"] == "POST")
{
    if (isset($_POST['inputNome'])) 
    {
        $resp = recaptcha_check_answer (Config::RECAPTCHA_PRIVATE_KEY, $_SERVER["REMOTE_ADDR"],$_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

        if (!$resp->is_valid) 
        {
            $bCaptchaCorreto = false;
        }
        else
        {

            BloqueioDB::setaFiltro(" AND Bloqueio_vch_Email = '".$_POST['inputEmail']."'");
            $oBloqueio = BloqueioDB::pesquisaBloqueio();
            
            if ($oBloqueio->iCodigo == '') {

                $oPedido = new Pedido;

                $sExibirIntencao = (isset($_POST['inputExibirIntencao'])) ? 'S' : 'N';

                $oPedido->sNome           = $_POST['inputNome'];
                $oPedido->sEmail          = $_POST['inputEmail'];
                $oPedido->sCidade         = $_POST['inputCidade'];
                $oPedido->sEstado         = $_POST['inputEstado'];
                $oPedido->sIntencao       = Util::filtraPalavroes($_POST['inputIntencao']);
                $oPedido->sExibirIntencao = $sExibirIntencao;
                $oPedido->sData           = date('Y-m-d');
                $oPedido->sTipo           = 'V';
                $oPedido->sAprovado       = "N";

                $sResultado = PedidoDB::salvaPedido($oPedido);

                if ( $sResultado )
                {
                    $sMensagemCadastro = '<label class="sucesso">Sua vela virtual foi acendida com sucesso! Aguarde aprova��o.</label>';
                }
                else
                {
                    $sMensagemCadastro = '<label class="erro">Erro ao acender sua vela virtual.</label>';
                }
            } else {
                $sMensagemCadastro = '<label class="erro">Seu e-mail est� bloqueado pelo administrador do sistema</label>';
            }
        }
    }
}

if ( isset ( $_GET["p"] ) )
{
    $iPaginaAtual = $_GET["p"]; 
}
else
{
    $iPaginaAtual = 1;
}

$iInicio = $iPaginaAtual - 1; 
$iInicio = (Config::NUMERO_VELAS) * $iInicio; 

if ( isset ( $_POST['inputBuscarVela'] ) && ( trim ( $_POST['inputEmailBusca'] ) <> '' ) )
{

    PedidoDB::setaFiltro("  AND ( Pedido_vch_Email = '".$_POST['inputEmailBusca']."' )");
}
else
{
    PedidoDB::setaLimite((Config::NUMERO_VELAS),$iInicio);
}

PedidoDB::setaFiltro(" AND ( DATEDIFF('".date( 'Y-m-d' )."',Pedido_dat_Data) <= 7 ) ");
PedidoDB::setaFiltro(" AND Pedido_chr_Tipo = 'V' ");
PedidoDB::setaFiltro(" AND ((Pedido_chr_Aprovado = 'S') OR (Pedido_chr_Aprovado IS NULL))");

$iTotalVela = PedidoDB::contaPedido();
$arrObjVela = PedidoDB::pesquisaPedidoLista();

/*if ( ( $arrObjPedido->count() ) == 0 && ( !isset ( $_POST['inputBuscarPedido'] ) ) )
{
    header("Location: ".PATH_WWW);
    exit();
}*/ 

$iTotalPaginas = ceil($iTotalVela / Config::NUMERO_VELAS);
$arrPaginacao = Util::calculaPaginacao($iPaginaAtual,$iTotalPaginas);

/* Define Vari�veis do Template */

/* Pagina��o */
$oTemplate->assign('sPaginaNome', 'velavirtual');
$oTemplate->assign('iPaginaAtual', $iPaginaAtual);
$oTemplate->assign('iTotalPaginas', $iTotalPaginas);
$oTemplate->assign('iLoop', $arrPaginacao['loop']);
$oTemplate->assign('iStart', $arrPaginacao['start']);
$oTemplate->assign('iProxima', $arrPaginacao['proxima']);
$oTemplate->assign('iAnterior', $arrPaginacao['anterior']);
$oTemplate->assign('sComplemento', '');

$oTemplate->assign('recaptcha',  recaptcha_get_html(Config::RECAPTCHA_PUBLIC_KEY) );
$oTemplate->assign('bCaptchaCorreto', $bCaptchaCorreto );
$oTemplate->assign('sMensagemCadastro', $sMensagemCadastro );
$oTemplate->assign('iTotalVela', $iTotalVela);
$oTemplate->assign('arrObjVela', $arrObjVela);

/* Define P�gina/Template a ser executado */
$oTemplate->display('site/vela_virtual.tpl');

?>